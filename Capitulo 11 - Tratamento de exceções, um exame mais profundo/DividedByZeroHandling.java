//DividedByZeroHandling.java
//Tratando ArithmeticExceptions e InputMismatchExceptions
import java.util.InputMismatchException;
import java.util.MismatchException;
import java.util.Scanner;

public class DividedByZeroHandling{

    //Demonstra o lançamento de uma exceção quando ocorre uma divisão por zero
    public static int quotient(int numerator, int denominator)
        throws ArithmeticException{
        return numerator / denominator; //Possivel divisão por zero
    }
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        boolean continueLoop = true; //Determina se mais entrada serão necessarias
        do{
            try{ //Le dois numeros e calcula o quociente
                System.out.print("Please enter an integer numerator: ");
                int numerator = sc.nextInt();
                System.out.print("Please enter an integer denominator: ");
                int denominator = sc.nextInt();

                int result = quotient(numerator, denominator);
                System.out.printf("%nResult: %d / %d = %d%n", numerator, denominator,result);
                continueLoop=false; //Entrada bem-sucedida; fim do loop
            }
            catch(InputMismatchException inputMismatchException){
                System.err.printf("%nException: %s%n",inputMismatchException);
                sc.nextLine(); //Decarta entrada para o usuario tentar denovo
                System.out.printf("You must enter integers. Please try again.%n%n");
            }
            catch(ArithmeticException arithmeticException){
                System.err.printf("%nException: %s%n", arithmeticException);
                System.out.printf("Zero is an invalid denominator. Please try again.%n%n");
            }
        }while(continueLoop);
    }
}//Fim da classe DivideByZeroHandlig