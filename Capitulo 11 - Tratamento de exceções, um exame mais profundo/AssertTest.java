//AssertTest.java
//Verificando com assert se um valor está dentro de um intervalo
import java.util.Scanner;
public class AssertTest{
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a number between 0 and 10:");
        int number = sc.nextInt();

        //afirma que o valor entra dentro do intervalo
       assert (number>= 0 && number <=10) : "bad number: " + number;

        System.out.printf("You entered %d%n",number);
    }
}//Fim da classe AssertTest