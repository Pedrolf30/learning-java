//UsingChainedExceptions.java
//Exceções encadeadas

public class UsingChainedExceptions{
    public static void main(String[]args){
        try{
            method1();
        }
        catch(Exception exception){
            exception.printStackTrace();
        }
    }
    //Chama method2;lança exceções de volta pra main
    public static void method1()throws Exception{
        try{
            method2();
        }
        catch(Exception exception){
            throw new Exception("Exception thrown in method1", exception);
        }
    }
    //Chama method3; lança exceções de volta para method1
    public static void method2()throws Exception{
        try{
            method3();
        }
        catch(Exception exception){
            throw new Exception("Exception thrown in method2",exception);
        }
    }
    //Lança Exception de volta para method2
    public static void method3()throws Exception{
        throw new Exception("Exceptions thrown in method3");
    }
}//Fim da classe UsingChainedExceptions