import jdk.internal.org.jline.utils.ExecHelper;

//UsingExceptions.java
//Mecanismo de tratamento de exceção try...cathc...finally

public class UsingExceptions{
    public static void main(String[]args){
        try{
            throwException();
        }
        catch(Exception exception){ //Exceção lançada por throwException
            System.err.println("Exception handled in main");
        }
        doesNotThrowException();
    }
    //Demonstra try...catch...finally
    public static void throwException() throws Exception{
        try{ //Lança uma exceção e imediatamente a captura
            System.out.println("Method throwException");
            throw new Exception(); //Gera a exceção
        }
        catch(Exception exception){ //Captura a exceção lançada em try
            System.err.println("Exception handled in method throwException");
            throw exception; //Lança novamente para processamento adicional

            //o codigo aqui não seria alcançado; poderia causar erros de compilação
        }
        finally{ //Executa independentemente do que ocorre em try e catch
            System.err.println("Finally executed in throwException");
        }
        //o codigo aqui não seria alcançado; poderia causar erros de compilação
    }
    //Demonstra finally quando nenhuma exceção ocorrer
    public static void doesNotThrowException(){
        try{ //Bloco try não lança uma exceção
            System.out.println("Method doesNotThrowException");
        }
        catch(Exception exception){ //Não executa
            System.err.println(exception);
        }
        finally{ //Executa independentemente do que ocorrey em try e catch
            System.err.println("Finally executed in doesNotThrowException");
        }
        System.out.println("End of method doesNotThrowException");
    }
}//Fim da classe UsingExceptions