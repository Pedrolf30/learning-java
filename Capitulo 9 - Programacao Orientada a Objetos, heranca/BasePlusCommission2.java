//BasePlusCommission.java
//A classe BasePlusCommission herda variaveis de instancia protected
//de CommissionEmployee

public class BasePlusCommission2 extends ComissionEmployee2{
   
    private double baseSalary; //Salario base por semana

    //Construtor de seis argumentos
    public BasePlusCommission2(String firstName, String lastName, String socialSecurityNumber,
    double grossSales, double commissionRate, double baseSalary){
        super(firstName, lastName, socialSecurityNumber,grossSales, commissionRate);

        //se baseSalary for invalido, lanca uma excecao
        if(baseSalary < 0.0)
            throw new IllegalArgumentException("base salary must be >=0.0");
        
            this.baseSalary=baseSalary;
    }//Fim do construtor

    //Configura o salario-base
    public void setBaseSalary(double baseSalary){
        if(baseSalary < 0.0)
            throw new IllegalArgumentException("base salary must be >=0.0");

        this.baseSalary=baseSalary;
    }
    //Retorna salario base
    public double getBaseSalary(){
        return baseSalary;
    }
    //Calcula os lucros
    public double earning(){
        return getBaseSalary + super.earnings();
    }
    //Retorna a representacao de String de BasePlusCommission
    @Override
    public String toString(){
        return String.format("%s %s%n%s: %.2f","base-salaried",super.toString(),"base salary",getBaseSalary());
    }
}//Fim da classe BasePlusCommission
