//BPCTest.java
//Programa de teste BasePlusCommission

public class BPCTest{
    public static void main(String[]args){
        //Instancia o objeto BasePlusCommission
        BasePlusCommission employee = new BasePlusCommission("Jon", "Doe", "777-777-777", 5000, .06, 300);

        //Obtem os dados do empregado comissionado com salario base
        System.out.println("Employee information obtained by get method");
        System.out.printf("%s %s%n:","First Name",employee.getFirstName());
        System.out.printf("%s %s%n:","Last Name",employee.getLastName());
        System.out.printf("%s %s%n:","Social Security Number",employee.getSocialSecurityNumber());
        System.out.printf("%s %.2f%n","Gross Sales",employee.getGrossSales());
        System.out.printf("%s %.2f%n","Commission Rate",employee.getCommissionRate());
        System.out.printf("%s %.2f%n","Base Salary",employee.getBaseSalary());

        employee.setBaseSalary(1000);
        System.out.printf("%n%s:%n%n%s%n","Updated employee informationobtained by toString",
        employee.toString());
    }//Fim do main
}//Fim da clase BPCTest