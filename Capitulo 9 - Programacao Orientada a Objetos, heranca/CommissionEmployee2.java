//CommissionEmployee.java
//A clase CommissionEmployeeusa metodos para manipular suas variaveis
//de instancia private

public class CommissionEmployee extends Object{
    
    private final String firstName;
    private final String lastName;
    private final String socialSecurityNumber;
    private double grossSales; //Vendas brutas semanais
    private double commissionRate; //Percentagem da comissao

    //Construtor de cinco argumentos
    public CommissionEmployee(String firstName, String lastName, String socialSecurityNumber,
    double grossSales, double commissionRate){
        //A chamada implicita para o construtor padrao de Object ocorre aqui
        //se grossSales for invalido, lanca uma excecao
        if(grossSales < 0.0)
            throw new IllegalArgumentException("Gross sales must be >= 0.0");
        //se commissionRate for invalido, lanca uma excecao
        if(commissionRate <= 0.0 || commissionRate >=1.0)
            throw new IllegalArgumentException("Commission rate musb be > 0.0 abd < 1.0");
        
        this.firstName=firstName;
        this.lastName=lastName;
        this.socialSecurityNumber=socialSecurityNumber;
        this.grossSales=grossSales;
        this.commissionRate=commissionRate;
    }//Fim do construtor

    //Retorna o nome
    public String getFirstName(){
        return firstName;
    }
    //Retorna o sobrenome
    public String getLastName(){
        return lastName;
    }
    //Retorna numero de seguro social
    public String getSocialSecurityNumber(){
        return socialSecurityNumber;
    }
    //Configura a quantidade de vendas brutas
    public void setGrossSales(double grossSales){
        if(grossSales < 0.0)
            throw new IllegalArgumentException("Gross sales must be >= 0.0");
        
        this.grossSales=grossSales;
    }
    //Retorna a quantidade de vendas brutas
    public double getGrossSales(){
        return grossSales;
    }
    //Configura taxa de comissao
    public void setCommissionRate(double commissionRate){
        if(commissionRate <= 0.0 || commissionRate >=1.0)
            throw new IllegalArgumentException("Commission rate musb be > 0.0 abd < 1.0");

        this.commissionRate=commissionRate;
    }
    //Retorna taxa de comissao
    public double getCommissionRate(){
        return commissionRate;
    }
    //Calcula os lucros
    public double earnings(){
        return getCommissionRate() * getGrossSales();
    }
    //Retorna a representacao String do objeto CommissionEmployee
    @Override//Indica que esse metodo substitui o metodo da superclasse
    public String toString(){
        return String.format("%s: %s %s%n%s: %s%n%s: %.2f%n%s: %.2f","Comission Employee", getFirstName(),getLastName(),"Social security number",getSocialSecurityNumber(),"Gross Sales",getGrossSales(),"Commission Rate",getCommissionRate());
    }
}//Fim da classe CommissionEmployee
