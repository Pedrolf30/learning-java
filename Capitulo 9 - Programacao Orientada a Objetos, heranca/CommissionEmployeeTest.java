//CommissionEmployeeTest.java
//Programa de teste da classe CommissionEmployee

public class CommissionEmployeeTest{
    public static void main(String[]args){
        //Instancia o objeto CommissionEmployee
        CommissionEmployee employee = new CommissionEmployee("Sue", "Jones",
         "77-7777-777777-777", 10000, .06);

         //Obtemos dados de empregado comissionado
         System.out.println("Employee information obtained by get methods:");
         System.out.printf("%s %s%n","First name is",employee.getFirstName());
         System.out.printf("%s %s%n","Last name is",employee.getLastName());
         System.out.printf("%s %s%n","Social security numbers",employee.getSocialSecurityNumber());
         System.out.printf("%s %.2f%n","Gross sales is",employee.getGrossSales());
         System.out.printf("%s %.2f%n","Commission rate is",employee.getCommissionRate());

         employee.setGrossSales(5000);
         employee.setCommissionRate(.1);

         System.out.printf("%n%s:%n%n%s%n","Updated employee information obtained by toString", employee);

    }//Fim do main
}//Fim da classe CommissionEmployeeTest