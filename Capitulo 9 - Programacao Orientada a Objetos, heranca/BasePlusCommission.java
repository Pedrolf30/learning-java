//BasePlusCommission
//A classe BasePlusCommission representa um empregado que recebe
//um salario-base alem da comissao

public class BasePlusCommission{
    private final String firstName;
    private final String lastName;
    private final String socialSecurityNumber;
    private double grossSales; //Vendas brutas semanais
    private double commissionRate; //Porcentagem da comissao
    private double baseSalary; //Salario base por semana

    //Construtor de seis argumentos
    public BasePlusCommission(String firstName, String lastName, String socialSecurityNumber,
    double grossSales, double commissionRate, double baseSalary){
        //a chamada implicita para o construtor padrao de Object ocorre aqui
        //se grossSales for invalido, lanca uma excecao
        if(grossSales < 0.0)
            throw new IllegalArgumentException("Gross sales mus be >= 0.0");
        //se commissionSales for invalido, lanca uma excecao
        if(commissionRate <=0.0 || commissionRate >=1.0)
            throw new IllegalArgumentException("Commission Rate musb be > 0.0 and < 1.0");
        //se baseSalary for invalido, lanca uma excecao
        if(baseSalary < 0.0)
            throw new IllegalArgumentException("base salary must be >=0.0");
        
            this.firstName=firstName;
            this.lastName=lastName;
            this.socialSecurityNumber=socialSecurityNumber;
            this.grossSales=grossSales;
            this.commissionRate=commissionRate;
            this.baseSalary=baseSalary;
    }//Fim do construtor

    //Retorna o nome
    public String getFirstName(){
        return firstName;
    }
    //Retorna sobrenome
    public String getLastName(){
        return lastName;
    }
    //Retorna numero do seguro social
    public String getSocialSecurityNumber(){
        return socialSecurityNumber;
    }
    //Configura a quantidade de vendas brutas
    public void setGrossSales(double grossSales){
        if(grossSales < 0.0)
            throw new IllegalArgumentException("Gross sales mus be >= 0.0");
        
        this.grossSales=grossSales;
    }
    //Retorna quantidade de vendas brutas
    public double getGrossSales(){
        return grossSales;
    }
    //Configura taxa de comissao
    public void setCommissionRate(double commissionRate){
        if(commissionRate <=0.0 || commissionRate >=1.0)
            throw new IllegalArgumentException("Commission Rate musb be > 0.0 and < 1.0");

        this.commissionRate=commissionRate;
    }
    //Retorna valor de commissionRate
    public double getCommissionRate(){
        return commissionRate;
    }
    //Configura o salario-base
    public void setBaseSalary(double baseSalary){
        if(baseSalary < 0.0)
            throw new IllegalArgumentException("base salary must be >=0.0");

        this.baseSalary=baseSalary;
    }
    //Retorna salario base
    public double getBaseSalary(){
        return baseSalary;
    }
    //Calcula os lucros
    public double earning(){
        return baseSalary + (commissionRate*grossSales);
    }
    //Retorna a representacao de String de BasePlusCommission
    @Override
    public String toString(){
        return String.format("%s: %s %s%n%s: %s%n%s: %.2f%n%s: %.2f%n%s: %.2f",
        "base-salaried comission employee", firstName, lastName,
        "social security number", socialSecurityNumber,"gross sales",grossSales,
        "commission rate",commissionRate,"base salary",baseSalary);
    }
}//Fim da classe BasePlusCommission