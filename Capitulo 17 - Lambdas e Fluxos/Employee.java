//Employee.java
//Classe Employee
public class Employee{
    private String firstName;
    private String lastName;
    private double salary;
    private String departament;

    //Construtor
    public Employee(String firstName, String lastName,
        double salary, String departament){
            this.firstName=firstName;
            this.lastName=lastName;
            this.salary=salary;
            this.departament=departament;
        }
        //Configura o nome
        public void setFirstName(String firstName){
            this.firstName=firstName;
        }
        //Obtem firstName
        public String getFirstName(){
            return firstName;
        }
        //Configura lastName
        public void setLastName(String lastName){
            this.lastName=lastName;
        }
        //Obtem lastName
        public String getLastName(){
            return lastName;
        }
        //Configura salario
        public void setSalary(double salary){
            this.salary=salary;
        }
        //Obtem salario
        public double getSalary(){
            return salary;
        }
        //Configura departamento
        public void setDepartament(String departament){
            this.departament=departament;
        }
        //Obtem departamento
        public String getDepartament(){
            return departament;
        }
        //Retornar nome e o sobrenome do empregado combinados
        public String getName(){
            return String.format("%s %s",getFirstName(),getLastName());
        }
        //Retorna uma String contendo informações do Employee
        @Override
        public String toString(){
            return String.format("%-8s %-8s %8.2f  %s",
                getFirstName(),getLastName(),getSalary(),getDepartament());
        }//Fim do método main
}//Fim da classe Employee