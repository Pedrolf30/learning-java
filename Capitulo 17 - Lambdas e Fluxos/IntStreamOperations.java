//IntStreamOperations.java
//Demonstrando operações IntStream
import java.util.Arrays;
import java.util.stream.IntStream;

public class IntStreamOperations{
    public static void main(String[] args){
        int[] values = {3,10,6,1,4,8,2,5,9,7};

        //Exibe valores originais
        System.out.println("Original values: ");
        IntStream.of(values).forEach(value -> System.out.printf("%d ",value));
        System.out.println();

        //Count, min, max, sum, average dos valores
        System.out.printf("%nCount: %d%n", IntStream.of(values).count());
        System.out.printf("Min: %d%n", IntStream.of(values).min().getAsInt());
        System.out.printf("Max: %d%n", IntStream.of(values).max().getAsInt());
        System.out.printf("Sum: %d%n", IntStream.of(values).sum());
        System.out.printf("Average: %.2f%n", IntStream.of(values).average().getAsDouble());

        //Soma dos valores com metodo reduce
        System.out.printf("%nSum via reduce method: %d%n", IntStream.of(values)
            .reduce(0,(x,y) -> x+y));
        
        //Soma das raizes quadradas dos valores com método reduce
        System.out.printf("Sum of squares via reduce method: %d%n",
            IntStream.of(values)
                .reduce(0,(x,y) -> x+y*y));
            
        //Produto dos valores com método reduce
        System.out.printf("Product vida reduce method: %d%n",
            IntStream.of(values)
                .reduce(1,(x,y) -> x+y));
            
        //Valores pares exibidos em ordem classificada
        System.out.printf("%nEven values displayed in sorted order: ");
        IntStream.of(values).filter(value -> value %2 ==0)
            .sorted()
            .forEach(value -> System.out.printf("%d ",value));
        System.out.println();

        //Valores impares multiplicados por 10 e exibidos em ordem classificada
        System.out.printf("Odd values multiplied by 10 displayed in sorted order");
        IntStream.of(values)
            .filter(value -> value %2 !=0)
            .map(value -> value *10)
            .sorted()
            .forEach(value -> System.out.printf("%d ", value));
        System.out.println();

        //Soma o intervalo dos numeros inteiro de 1 a 10, exclusivo
        System.out.printf("%nSum of integers from 1 to 9: %d%n",
            IntStream.range(1,10).sum());
        
        //Soma o intervalo dos numeros intiro de 1 a 10, inclusivo
        System.out.printf("Sum of integer from 1 to 10: %d%n",
            IntStream.rangeClosed(1,10).sum());
    }
}//Fim da classe IntStreamOperations