//ArraysAndStreams2.java
//Demonstrando lambdas e fluxos com um array e Strings
import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;

public class ArraysAndStreams2{
    public static void main(String[]args){
        String[] strings = {"Red","orange","Yellow","green","Blue",
            "indigo", "Violet"};
        
        //Exibe strings originais
        System.out.printf("Original strings: %s%n",Arrays.asList(strings));

        //Strings em maiusculas
        System.out.printf("Strings in uppercase: %s%n",
            Arrays.stream(strings)
                .map(String::toUpperCase)
                .collect(Collectors.toList()));
            
        //Strings menores que "n" (sem distinção upper/lower) em ordem crescente
        System.out.printf("String greater than m sorted ascending: %s%n",
            Arrays.stream(strings)
            .filter(s -> s.compareToIgnoreCase("n") < 0)
            .sorted(String.CASE_INSENSITIVE_ORDER)
            .collect(Collectors.toList()));
        
        //String menores que "n"(com distinção upper/lower) em ordem decrescente
        System.out.printf("String greater than m sorted descending: %s%n",
            Arrays.stream(strings)
            .filter(s ->s.compareToIgnoreCase("n") < 0)
            .sorted(String.CASE_INSENSITIVE_ORDER.reversed())
            .collect(Collectors.toList()));
    }
}//Fim da classe ArraysAndStreams2