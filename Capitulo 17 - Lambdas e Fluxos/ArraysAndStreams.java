//ArraysAndStreams.java
//Demonstrando lambdas e fluxos com um array de integers
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ArraysAndStreams{
    public static void main(String[] args){
        Integer[] values = {2,9,5,0,3,7,1,4,8,6};

        //Exibe valores originais
        System.out.printf("Original values: %s%n",Arrays.asList(values));

        //Classifica os valores em ordem crescente com fluxos
        System.out.printf("Sorted values: %s%n",
            Arrays.stream(values)
            .sorted()
            .collect(Collectors.toList()));
        
        //Valores maiores que 4
        List<Integer> greaterThan4 =
            Arrays.stream(values)
            .filter(value -> value > 4)
            .collect(Collectors.toList());
        System.out.printf("Values greater than 4: %s%n", greaterThan4);

        //Filtra valores maiores que 4 e então classifica os resultados
        System.out.printf("Sorted values greater than 4: %s%n",
            Arrays.stream(values)
            .filter(value -> value > 4)
            .sorted()
            .collect(Collectors.toList()));
    }
}//Fim da classe ArraysAndStreams