//ProcessingEmployees.java
//Processando fluxos de objetos Employee
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ProcessingEmployees{
    public static void main(String[]args){
        //Inicializa o array Employees
        Employee[] employees = {
            new Employee("Jason","Derulo",500000,"IT"),
            new Employee("Ashley","Green",7600,"IT"),
            new Employee("Matthew","Indigo",3587.5,"Sales"),
            new Employee("James","Brown",4700.77,"Marketing")};

            //Obtem a visualiação List dos Employee
            List<Employee> list = Arrays.asList(employees);

            //Exibe todos os employees
            System.out.println("Complete Employee List: ");
            list.stream().forEach(System.out::println);

            //Predicate que retorne true para salarios no intervalo 4000-6000
            Predicate<Employee> fourToSixThousand = e -> (e.getSalary() >=4000
            && e.getSalary() <=6000);

            //Exibe Employees com salarios no intervalo 4000-6000
            //classificados em ordem crescente por salario
            System.out.printf("%nEmployees earning $4000-$6000 per month by salary:%n");
            list.stream()
                .filter(fourToSixThousand)
                .sorted(Comparator.comparing(Employee::getSalary))
                .forEach(System.out::println);
            
            //Exibe o primeiro Employee com salarios no intervalo 4000-6000
            System.out.printf("%nFirst employee who earns 4000-6000: %n%s%n",
                list.stream()
                    .filter(fourToSixThousand)
                    .findFirst()
                    .get());
                
            //Functions para obter o nome e o sobrenome de um EMployee
            Function<Employee, String> byFirstName = Employee::getFirstName;
            Function<Employee, String> byLastName = Employee::getLastName;

            //Comparator para comparar Employees pelo nome,e então pelo sobrenome
            Comparator<Employee> lastThenFirst = Comparator.comparing(byLastName).thenComparing(byFirstName);

            //Classifica funcionarios pelo sobrenome e, então, pelo nome
            System.out.printf("%nEmployees is ascending order by last name then first: %n");
            list.stream()
                .sorted(lastThenFirst)
                .forEach(System.out::println);
            
            //Classifica funcionarios em ordem decrescente pelo sobrenome,e então nome
            System.out.printf("%nEmployees in descending order by last name then first: %n");
            list.stream()
                .sorted(lastThenFirst.reversed())
                .forEach(System.out::println);

            //Exibe os sobrenomes unicos dos funcionarios classificados
            System.out.printf("%nUnique employee last names:%n");
            list.stream()
                .map(Employee::getLastName)
                .distinct()
                .sorted()
                .forEach(System.out::println);

            //Exibe apenas o nome e o sobrenome
            System.out.printf("%nEmployee names in order by last name then first name:%n");
            list.stream()
                .sorted(lastThenFirst)
                .map(Employee::getName)
                .forEach(System.out::println);

            //Agrupa employees por departamento
            System.out.printf("%nEmployees by departament:%n");
            Map<String, List<Employee>> groupedByDepartament = 
                list.stream()
                    .collect(Collectors.groupingBy(Employee::getDepartament));

            groupedByDepartament.forEach(
                (departament, employeesInDepartament) -> {
                    System.out.println(departament);
                    employeesInDepartament.forEach(
                        employee -> System.out.printf("  %s%n", employee)
                    );
                }
            );

       

            //Soma os salarios dos Employees com o método de soma DoubleStream
            System.out.printf("%nSum of Employees salaries(via sum method): %.2f%n",
                list.stream()
                    .mapToDouble(Employee::getSalary)
                    .sum());

            //Calcula soma dos salarios dos Employes com o método reduce Stream
            System.out.printf("Sum of employees salarios(via reduce method): %.2f%n",
                list.stream()
                    .mapToDouble(Employee::getSalary)
                    .reduce(0,(value1,value2) -> value1 + value2));

            //Calcula a média de salarios dos Employees com o método average Double Stream
            System.out.printf("Average of Employees salaries: %.2f%n",
                list.stream()
                    .mapToDouble(Employee::getSalary)
                    .average()
                    .getAsDouble());
    }//Fim de main
}//Fim da classe ProcessingEmployees