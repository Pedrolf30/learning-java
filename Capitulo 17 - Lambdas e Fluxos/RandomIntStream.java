//RandomIntStream.java
//Rolando um dado 6000000 vezes com fluxos
import java.security.SecureRandom;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Collectors;

public class RandomIntStream{
    public static void main(String[]args){
        SecureRandom random = new SecureRandom();

        //Rolando um dado 60000000 vezes e resumindo os resultados
        System.out.printf("%-6s%s%n","Face","Frequency");
        random.ints(6_000_000,1,7)
            .boxed()
            .collect(Collectors.groupingBy(Function.identity(),
                Collectors.counting()))
            .forEach((face, frequency) ->
                System.out.printf("%-6d%d%n",face,frequency));
    }
}//Fim da classe RandomIntStream