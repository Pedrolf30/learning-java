//ClassAverage.java, resolvendo o problema da média da classe com repetição controlada por contador
import java.util.Scanner; //Programa utiliza a classe Scanner
public class ClassAverage{
	public static void main(String[]args){
		Scanner sc = new Scanner(System.in); //Inicia Scanner para a entrada de dados do usuário
		int total = 0; //Inicializa soma das notas inseridas pelo usuário
		int gradeCounter = 1; //Inicializa numero de notas digitadas pelo usuário
		while(gradeCounter<=10){ //Faz o loop 10 vezes
			System.out.println("Enter grade"); //Prompt
			int grade = sc.nextInt(); //Insere a próxima nota
			total = total + grade; //Adiciona nota ao total
			gradeCounter ++; //Incrementa contador por 1
		}//Fim do while
		int average = total /10;
		System.out.printf("The sum of the all 10 grades is: %d%n",total);
		System.out.printf("Class average is: %d%n",average);
	}
}//Fim da classe ClassAverage
