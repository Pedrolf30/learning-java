//StudentTest.java, cria e testa objetos Student
public class StudentTest{
	public static void main(String[]args){
		Student acc1 = new Student("Pedro",93.75);
		Student acc2 = new Student("Joao",75.50);
		System.out.printf("%s's grade is: %s%n", acc1.getName(),acc1.getLetterGrade());
		System.out.printf("%s's grade is: %s%n", acc2.getName(),acc2.getLetterGrade());
	}

}//Fim da classe StudentTest
