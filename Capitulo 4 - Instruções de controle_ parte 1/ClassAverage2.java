//ClassAverage2.java, resolvendo problema de média da classe com repetição controlada por sentinela
import java.util.Scanner; //Programa utiliza classe Scanner
public class ClassAverage2{
	public static void main(String[]args){
		Scanner sc = new Scanner(System.in); //Cria Scanner para a entrada de dados do usuário
		int total = 0; //Inicializa a soma das notas
		int gradeCounter = 0; //Inicializa numero de notas digitadas pelo usuário
		System.out.println("Enter grade, or type '-1' to quit: ");
		int grade = sc.nextInt();
		while(grade!=-1){ //Faz o loop até o valor da sentinela ser digitado pelo usuário
			total = total + grade; //Adiciona grade ao total
			gradeCounter++; //Incrementa contador em 1
			System.out.println("Enter grade, or type '-1' to quit: ");
			grade = sc.nextInt();
		}//Fim do while
		if(gradeCounter!=0){
			double average = (double) total/gradeCounter;
			System.out.printf("Number of grades entered: %d%n",total);
			System.out.printf("Class Average: %.2f%n", average);
		}else{
			System.out.println("No grades were entered");
		}
	}
}//Fim da classe ClassAverage2
