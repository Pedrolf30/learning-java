//Student.java, armazena a nota média e o nome do aluno
public class Student{
	private String name;
	private double average;
	//Construtor inicializa as variaveis de instância
	public Student(String name,double average){
		this.name=name;
		//Valida que a média é maior que 0.0 e menor ou igual a 100.0
		if(average>0.0)
			if(average<=100.0)
				this.average=average; //Atribui à variavel de instância
	}
	public void setName(String name){ //Define o nome do estudante
		this.name=name;
	}
	public String getName(){ //Recupera o nome do estudante
		return name;
	}
	public void setAverage(double studentAverage){ //Define a media do estudante
		if(average>0.0)
			if(average<=100.0)
				this.average=average;
	}
	public double getAverage(){ //Recupera media do estudante
		return average;
	}
	public String getLetterGrade(){ //Determina a letra referente à nota
		String letterGrade=""; //Inicializado com string vazia
		if(average>=90)
			letterGrade="A";
		else if(average>=80)
			letterGrade="B";
		else if(average>=70)
			letterGrade="C";
		else if(average>=60)
			letterGrade="D";
		else
			letterGrade="F";
		return letterGrade;
	}
}//Fim da classe StudentTest
