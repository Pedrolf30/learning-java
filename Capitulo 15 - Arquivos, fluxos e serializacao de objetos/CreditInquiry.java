//CreditInquiry.java
//Esse programa le um arquivo sequencialmente e exibe o
//conteudo baseado no tipo de conta que o usuario solicita
//(saldo credor, saldo devedor ou saldo zero).
import java.io.IOException;
import java.lang.IllegalStateException;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class CreditInquiry{
    private final static MenuOption[] choices = MenuOption.values();

    public static void main(String[]args){
        //Obtem a solicitacao do usuario
        MenuOption accountType = getRequest();

        while(accountType != MenuOption.END){
            switch (accountType){
                case ZERO_BALANCE:
                    System.out.printf("%nAccounts with zero balances:%n");
                    break;
                case CREDIT_BALANCE:
                    System.out.printf("%nAccounts with credit balances:%n");
                    break;
                case DEBIT_BALANCE:
                    System.out.printf("%nAccounts with debit balances:%n");
                    break;
                case END:
                    System.out.printf("%nTerminating:%n");
                    break;
            }
            readRecords(accountType);
            accountType = getRequest();
        }
    }
    //Obtem a solicitacao do usuario
    private static MenuOption getRequest(){
        int request = 4;
        //Exibe opções de solicitação
        System.out.printf("%nEnter requests%n%s%n%s%n%s%n%s%n",
        "1 - List accounts with zero balances",
        "2 - List accounts with credit balances",
        "3 - List accounts with debit balances",
        "4 - Terminate program");

        try{
            Scanner sc = new Scanner(System.in);

            do{ //Insere a solicitação de usuario
                System.out.printf("$n? ");
                request = sc.nextInt();
            }while((request < 1) || (request > 4));
        }
        catch(NoSuchElementException elementException){
            System.err.println("Invalid sc.Terminating");
        }
        return choices[request -1]; //Retorna o valor enum da opção
    }
    //Le os registros de arquivo e exibe somente os registros do tipo apropriado
    private static void readRecords(MenuOption accountType){
        //Abre o arquivo e processa o conteudo
        try(Scanner sc = new Scanner(Paths.get("clients.txt"))){
            while(sc.hasNext()){ //Mais dados para ler
                int accountNumber = sc.nextInt();
                String firstName = sc.next();
                String lastName = sc.next();
                double balance = sc.nextDouble();

                //Se o tipo for a conta adequado, exibe o registro
                if(shouldDisplay(accountType, balance))
                    System.out.printf("%-10d%-12s%-12s%10.2f%n", accountNumber,
                    firstName,lastName,balance);
                else
                    sc.nextLine(); //Descarta restante do registro atual
            }
        }
        catch(NoSuchElementException | IllegalStateException | IOException e){
            System.err.println("Error processing file. Terminating");
            System.exit(1);
        }
    }//Fim do metodo readRecords

    //Utiliza o tipo de registro para determinar se registro deve ser exibido
    private static boolean shouldDisplay(MenuOption accountType, double balance){
        if((accountType == CreditMenuOption.CREDIT_BALANCE) && (balance < 0))
            return true;
        else if((accountType == CreditMenuOption.DEBIT_BALANCE) && (balance > 0))
            return true;
        else if((accountType == CreditMenuOption.ZERO_BALANCE) && (balance == 0))
            return true;
        
        return false;
    }
}//Fim da classe CreditInquiry