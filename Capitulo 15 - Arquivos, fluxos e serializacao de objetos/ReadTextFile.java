//ReadTextFile.java
//Esse programa le um arquivo de texto e exibe cada registro
import java.io.IOException;
import java.lang.IllegalStateException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ReadTextFile{
    private static Scanner sc;

    public static void main(String[]args){
        openFile();
        readRecords();
        closeFile();
    }
    //Abre o arquivo
    public static void openFile(){
        try{
            sc = new Scanner(Paths.get("clients.txt"));
        }
        catch(IOException ioException){
            System.err.println("Error opening file.Terminating");
            System.exit(1);
        }
    }
    //Le o registro no arquivo
    public static void readRecords(){
        System.out.printf("%-10s%-12s%-12s%10s%n","Account","First Name",
        "Last Name","Balance");

        try{
            while(sc.hasNext()){//Enquanto houver mais linhas para ler
                //Exibe o conteudo do registro
                System.out.printf("%-10d%-12s%-12s%10.2f%n",sc.nextInt(),
                sc.next(),sc.next(),sc.nextDouble());
            } 
        }
        catch(NoSuchElementException elementException){
            System.err.println("File improperly formed.Terminating");
        }
        catch(IllegalStateException stateException){
            System.err.println("Error reading from file.Terminating");
        }
    }//Fim do metodo readRecords

    //Fecha o arquivo e termina o aplicativo
    public static void closeFile(){
        if(sc != null)
            sc.close();
    }
}//Fim da classe ReadTextFile