//MenuOption.java
//Tipo enum para as opções do programa de consulta de crédito

public enum MenuOption{
    //Declara o conteudo de cada enum
    ZERO_BALANCE(1),
    CREDIT_BALANCE(2),
    DEBIT_BALANCE(3),
    END(4);

    private final int value;//Opção atual de menu

    //Construtor 
    private CreditMenuOption(int value){
        this.value=value;
    }
    
}//Fim do enum MenuOption