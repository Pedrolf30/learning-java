//Account.java
//Classe Account serializavel para armazenas registros como objetos
import java.io.NotSerializableException;;

public class Account implements Serializable{
    private int account;
    private String firstName;
    private String lastName;
    private double balance;

    //Inicializar um Account com valores padrão

    public Account(){
        this(0,"","",0.0); //Chama outro construtor
    }

    //Inicializa uma Account com os valores fornecidos
    public Account(int account, String firstName, String lastName, double balance){
        this.account = account;
        this.firstName = firstName;
        this.lastName = lastName;
        this.balance = balance;
    }
    //Configura o numero da conta
    public void setAccount(int acct){
        this.account = account;
    }
    //Obtem numero de conta
    public int getAccount(){
        return account;
    }
    //Configura o nome
    public void setFirstName(String firstName){
        this.firstName=firstName;
    }
    //Obtem o nome 
    public String getFirstName(){
        return firstName;
    }
    //Configura o sobrenome
    public void setLastName(String lastName){
        this.lastName=lastName;
    }
    //Obtem o sobrenome
    public String getLastName(){
        return lastName;
    }
    //Configura o saldo
    public void setBalance(double balance){
        this.balance=balance;
    }
    //Obtem o saldo
    public double getBalance(){
        return balance;
    }
}//Fim da classe Account