//JFileChooserDemo.java
//Demonstrando JFileChooser
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class JFileChooserDemo extends JFrame{
    private final JTextArea outputArea; //Exibe o conteudo do arquivo

    //Configura a GUI
    public JFileChooserDemo() throws IOException{
        super("JFileChooser Demo");
        outputArea = new JTextArea();
        add(new JScrollPane(outputArea)); //outputArea é rolavel
        analyzePath(); //Obtem o Path do usuario e exibe informações
    }
    //Exibe informações sobre o arquivo ou diretório que o usuario especifica
     public void analyzePath()throws IOException{
         //Obtem o Path para o arquivo ou diretorio selecionado pelo usuario
         Path path = getFileOrDirectoryPath();

         if(path != null && Files.exists(path)){ //Se existir, exibe as informações dele
            //Coleta as informações sobre o arquivo(ou diretorio)
            StringBuilder builder = new StringBuilder();
            builder.append(String.format("%s:%n", path.getFileName()));
            builder.append(String.format("%s a directory%n",
                Files.isDirectory(path) ? "Is":"Is not"));
            builder.append(String.format("%s an absolute path%n",
                path.isAbsolute() ? "Is":"Is not"));
            builder.append(String.format("Last modified: %s%n",
                Files.getLastModifiedTime(path)));
            builder.append(String.format("Size: %s%n", Files.size(path)));
            builder.append(String.format("Path: %s%n", path));
            builder.append(String.format("Absolute path: %s%n",
                path.toAbsolutePath()));

            if(Files.isDirectory(path)){ //Listagem de diretorio de saida
                builder.append(String.format("%nDirectory contents:%n"));

                //Objeto para iteração pelo conteudo de um diretorio
                DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path);

                for(Path p : directoryStream)
                    builder.append(String.format("%s%n", p));
            }
            outputArea.setText(builder.toString()); //Exibe o conteudo de String
        }
        else //Path não existe
        {
            JOptionPane.showMessageDialog(this, path.getFileName() +
            " does not exist.", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
     }//Fim do método analyzePath

     //Permite que o usuario especifique o nome de arquivo ou diretorio
     private Path getFileOrDirectoryPath(){
         //Configura o dialogo permitindo a seleção de um arquivo ou diretorio
         JFileChooser fileChooser = new JFileChooser();
         fileChooser.setFileSelectionMode(
             JFileChooser.FILES_AND_DIRECTORIES);
        int result = fileChooser.showOpenDialog(this);

        //Se o usuario clicou no botão no dialogo, retorna
        if(result == JFileChooser.CANCEL_OPTION)
            System.exit(1);
        
        //Retorna o path representando o arquivo selecionado
        return fileChooser.getSelectedFile().toPath();
        
     }
}//Fim da classe JFileChooserDemo