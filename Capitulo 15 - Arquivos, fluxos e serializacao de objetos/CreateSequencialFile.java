//CreateSequencialFile.java
//Gravando objteos sequencialmente em um arquivo com a classe Object
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class CreateSequencialFile{
    private static ObjectOutputStream output; //Gera saida dos dados

    public static void main(String[]args){
        openFile();
        addRecords();
        closeFile();
    }
    //Abre o arquivo clients.ser
    public static void openFile(){
        try{
            output = new ObjectOutputStream(Files.newOutputStream(Paths.get("clients.ser")));
        }
        catch(IOException ioException){
            System.err.println("Error opening file. Terminating");
            System.exit(1); //Termina o programa
        }
    }
    //Adiciona registros ao arquivo
    public static void addRecords(){
        Scanner sc = new Scanner(System.in);
        System.out.printf("%s%n%s%n? ","Enter account number, first name, last name and balance.",
            "Enter end-of-file indicator to end sc.");
        while(sc.hasNext()){ //Faz um loop ate o indicador de fim do arquivo
            try{
                //Gera saida do novo registro para o arquiv; supoe entrada valida
                Account record = new Account(sc.nextInt(),
                   sc.next(), sc.next(), sc.nextDouble());

                output.writeObject(record);
            }
            catch(IOException ioException){
                System.err.println("Error writing to file. Terminating");
                break;
            }
            catch(NoSuchElementException elementException){
                System.err.println("Invalid sc.Please try again");
                sc.nextLine(); //Descarta entrada para o usuario tentar denovo
            }
            System.out.print("? ");
        }//Fim do while
    }//Fim do metodo addRecords


    //Fecha o arquivo e termina o aplicativo
    public static void closeFile(){
        try{
            if(output != null)
                output.close();
        }
        catch(IOException ioException){
            SYstem.err.println("Error closing file.Terminating");
        }
    }
}//Fim da classe CreateSequentialFile