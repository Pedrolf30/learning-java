//CreateTextFile.java
//Gravando dados em um arquivo de texto sequencial com classe Formatter
import java.io.FileNotFoundException;
import java.lang.SecurityException;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class CreateTextFile{
    private static Formatter output; //Envia uma saida de texto para um arquivo

    public static void main(String[]args){
        openFile();
        addRecords();
        closeFile();
    }
    //Abre o arquivo clients.txt
    public static void openFile(){
        try{
            output = new Formatter("clients.txt"); //Abre o arquivo
        }
        catch(SecurityException securityException){
            System.err.println("Write permission denied.Terminating");
            System.exit(1); //Termina o programa
        }
        catch(FileNotFoundException fileNotFoundException){
            System.err.println("Error opening file.Terminating");
            System.exit(1); //Termina o programa
        }
    }
    //Adiciona registros ao arquivo
    public static void addRecords(){
        Scanner sc = new Scanner(System.in);
        System.out.printf("%s%n%s%n? ","Enter account number, first name, last name and balance.",
            "Enter end-of-file indicator to end sc.");
        while(sc.hasNext()){ //Faz um loop ate o indicador de fim do arquivo
            try{
                //Gera saida do novo registro para o arquiv; supoe entrada valida
                output.format("%d %s %s %.2f%n", sc.nextInt(),
                    sc.next(), sc.next(), sc.nextDouble());
            }
            catch(FormatterClosedException formatterClosedException){
                System.err.println("Error writing to file. Terminating");
                break;
            }
            catch(NoSuchElementException elementException){
                System.err.println("Invalid sc.Please try again");
                sc.nextLine(); //Descarta entrada para o usuario tentar denovo
            }
            System.out.print("? ");
        }//Fim do while
    }//Fim do metodo addRecords

    //Fecha o arquivo
    public static void closeFile(){
        if(output != null)
            output.close();
    }
}//Fim da classe CreateTextFile