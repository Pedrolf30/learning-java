//FileAndDirectoryInfo.java
//A classe File utilizada para obter informacoes de arquivo e diretorio
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class FileAndDirectoryInfo{
    public static void main(String[]args) throws IOException{
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter file or directory name:");

        //Cria o objeto path com base na entrada do usuario
        Path path = Paths.get(sc.nextLine());

        if(Files.exists(path)){ //Se o caminho existe gera uma saida das informacoes dele
            //Exibe informacoes sobre o arquivo ou diretorio
            System.out.printf("%n%s exists%n",path.getFileName());
            System.out.printf("%s a directory%n",Files.isDirectory(path) ? "Is":"Is not");
            System.out.printf("%s is an absolute path%n",path.isAbsolute() ? "Is":"Is not");
            System.out.printf("Last modified: %s%n",Files.getLastModifiedTime(path));
            System.out.printf("Size: %s%n",Files.size(path));
            System.out.printf("Path: %s%n",path);
            System.out.printf("Absolute path: %s%n",path.toAbsolutePath());

            if(Files.isDirectory(path)){ //Listagem de diretorio de saida
                System.out.printf("%nDirectory contents:%n");

                //Objeto para iteracao pelo conteudo de um diretorio
                DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path);

                for(Path p : directoryStream)
                    System.out.println(p);
            }
        }
        else { //Se nao for arquivo ou diretorio, gera saida de mensagem de erro
            System.out.printf("%s does not exist%n",path);
        }
    }//Fim da main
}//Fim da classe FileAndDirectoryInfo
