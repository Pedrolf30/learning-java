//AccountTest.java, cria e manipula um objeto Account
import java.util.Scanner;
public class AccountTest{
	public static void main(String[]args){
		Scanner sc = new Scanner(System.in);
		//Cria dois objetos com Account
		Account conta1 = new Account(50.00);
		Account conta2 = new Account(7.00);

		//Exibe saldo inicial e nome da conta
		System.out.printf("Initial name of conta1 is :%s and its balance is $%.2f",conta1.getName(),conta1.getBalance());
		System.out.printf("\nInitial name of conta2 is :%s and its balance is$%.2f",conta2.getName(),conta2.getBalance());

		//Solicita e lê o nome
		System.out.println("\nPlease enter the conta1 name");
		String theName = sc.nextLine();//Insere uma linha de texto
		conta1.setName(theName); //Insere theName como nome de myAccount
		System.out.println(); //Insere uma linha em branco

		System.out.println("Please enter the conta2 name");
                String theName2 = sc.nextLine();//Insere uma linha de texto
                conta2.setName(theName2); //Insere theName como nome de myAccount
                System.out.println(); //Insere uma linha em braco

		//Exibe o nome armazenado em myAccount
		System.out.printf("Name in object conta1 is :%s ",conta1.getName());
		System.out.printf("\nName in object conta2 is :%s ",conta2.getName(),"\n");

		//Cria um scanner para adicionar saldo
		System.out.println("\nEnter deposit amount for conta1: "); //Prompt
		double depositAmount = sc.nextDouble(); //Leu entrada do usuario
		System.out.printf("Adding %.2f to conta1 balance",depositAmount);
		conta1.deposit(depositAmount);
		System.out.printf("\nEnter deposit amount for conta2: "); //Prompt
                depositAmount = sc.nextDouble(); //Leu entrada do usuario
                System.out.printf("\nAdding %.2f to conta2 balance",depositAmount);
		conta2.deposit(depositAmount);

		//Exibe os saldo depois dos depositos
		System.out.printf("\nThe name of conta1 is : %s and its balance is $%.2f",conta1.getName(),conta1.getBalance());
                System.out.printf("\nThe name of conta2 is : %s and its balance is $%.2f",conta2.getName(),conta2.getBalance());
	}
}//Fim da classe AccountTest
