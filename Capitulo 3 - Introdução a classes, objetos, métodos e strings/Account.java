//Account.java
public class Account{
	private String name; //Variavel de instancia
	private double balance; //Variavel de instancia
	public Account(double balance){
		//Valida que balance eh maior que 0.0
		//Se nao for, a variavel de instancia balance mantém seu valor inicial 0.0
		if(balance>0.0){
			this.balance=balance;
		}
	}
	public void deposit(double depositAmount){ //Deposita apenas quantia valida
		if(depositAmount>0.0){ //Verifica se valor do deposito eh valido
			balance = balance + depositAmount; //Adiciona saldo
		}
	}
	public double getBalance(){ //Metodo retorna saldo da conta
		return balance;
	}
	public void setName(String name){ //Metodo para definir nome do objeto
		this.name=name; //Armazena o nome
	}
	public String getName(){ //Metodo para recuperar nome do objeto
		return name; //Retorna o valor do nome para o chamador
	}
}//Fim da classe Account
