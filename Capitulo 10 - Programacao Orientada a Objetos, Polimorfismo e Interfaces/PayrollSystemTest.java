//PayrollSystemTest.java
//Programa de teste da hierarquia Employee

public class PayrollSystemTest{
    public static void main(String[]args){
        //Cria objetos de subclasse
        SalariedEmployee salariedEmployee = new SalariedEmployee("John", "Smith", "1111-11-111", 800.00);
        HourlyEmployee hourlyEmployee = new HourlyEmployee("Karen", "Price", "2222-22-222", 16.75, 40);
        CommissionEmployee commissionEmployee = new CommissionEmployee("Melissa", "Fumero", "333-333-333", 10000, .06);
        BasePlusCommissonEmployee basePlusCommissonEmployee = new BasePlusCommissonEmployee("Bob", "Lewis", "4444-44-444", 5000, .04, 300);

        System.out.println("Employees processed individually:");

        System.out.printf("%n%s%n%s: $%,.2f%n%n", salariedEmployee, "earned", salariedEmployee.earnings());
        System.out.printf("%s%n%s: &%,.2%n%n", hourlyEmployee, "earned", hourlyEmployee.earning());
        System.out.printf("%s%n%s: $%,.2f%n%n", commissionEmployee,"earned",commissionEmployee.earning());
        System.out.printf("%s%n%s: $%,.2f%n%n",basePlusCommissonEmployee,"earned",basePlusCommissonEmployee.earning());

        //Cria um array Employee de quatro elementos
        Employee[] employees = new Employee[4];

        //Inicializa o array com Employees
        employees[0] = salariedEmployee;
        employees[1] = hourlyEmployee;
        employees[2] = commissionEmployee;
        employees[3] = basePlusCommissonEmployee;

        System.out.printf("Employees processed polymorphically: %n%n");

        //processa genericamente cada elemento no employees
        for(Employee currentEmployee : employees){
            System.out.println(currentEmployee); //Invoca toString

            //Determina se elemente é um BasePlusCommissionEmployee
            if currentEmployee instanceof BasePlusCommissionEmployee (){
                //Downcast da referencia de Employee para
                //refrencia a BasePlusCommissionEmployee
                BasePlusCommissonEmployee employee = (BasePlusCommissonEmployee) currentEmployee;
                employee.setBaseSalary(1.10*employee.getBaseSalary());

                System.out.printf("New base salary with 10%% increase is: $%,.2f",
                employee.getBaseSalary());
            }//Fim do if
            System.out.printf("Earned $%,.2f%n%n", currentEmployee.earnings());
        }//Final for
        //Obtem o nome do tipo de cada objeto array employees
        for(int j=0;j<employees.length;j++)
            System.out.printf("Employee %d is a %s%n",j,employees[j].getClass().getName());
    }//Fim do main
}//Fim da classe PayrollSystemTest