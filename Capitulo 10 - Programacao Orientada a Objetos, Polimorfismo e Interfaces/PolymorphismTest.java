//PolymorphismTest.java
//Atribuindo referencias de superclasse e subclasse a variaveis de superclasse e subclasse

public class PolymorphismTest{
    public static void main(String[]args){
        //atribui uma referencia de superclasse a variavel de superclasse
        CommissionEmployee commissionEmployee = new CommissionEmployee("Sue",
        "Jones","222-222-222",10000,.06);

        //atribui uma referencia de subclasse a variavel de subclasse
        BasePlusCommission basePlusCommission = new BasePlusCommission("Bob",
        "Lewis","333-333-333",5000,.04,300);

        //invoca toString no objeto de subclasse utilizando a variavel de subclasse
        System.out.printf("%s %s:%n%n%s%n%n", "Call BasePlusCommission's toString with subclass",
        "reference to subclass object", basePlusCommissionEmployee.toString());

        //invoce toString no objeto de superclasse utilizando a variavel de superclasse
        System.out.printf("%s %s:%n%n%s%n%n", "Call BasePlusCommission's toString with superclass",
        "reference to superclass object", commissionEmployee.toString());

        //invoca toString no objeto de subclasse utilizando a variavel de superclasse
        CommissionEmployee commissionEmployee2 = basePlusCommission;
        System.out.printf("%s %s:%n%n%s%n","Call BasePlusCommision toString with superclass",
        "Reference to subclass object",commissionEmployee2.toString());
    }//Fim da main

}//Fim da classe Polymorphism