//Invoice.java
//Classe Invoice que implementa Payable

public class Invoice implements Payable{
    private final String partNumber;
    private final String partDescription;
    private int quantity;
    private double pricePerItem;

    //Construtor
    public Invoice(String partNumber, String partDescription, int quantity,
    double pricePerItem){
        if(quantity < 0) //Valida quantidade
            throw new IllegalArgumentException("Quantity must be >= 0");
        
        if(pricePerItem < 0.0) //Valida pricePerItem
            throw new IllegalArgumentException("Price per item must be >= 0");

        this.quantity=quantity;
        this.partNumber=partNumber;
        this.partDescription=partDescription;
        this.pricePerItem=pricePerItem;
    }//Fim do construtor
    //Obtem o numero da peça
    public String getPartNumber(){
        return partNumber;
    }
    //Obtem a descrição
    public String getPartDescription(){
        return partDescription;
    }
    //Configura a quantidade
    public void setQuantity(int quantity){
        if(quantity < 0) //Valida quantidade
            throw new IllegalArgumentException("Quantity must be >= 0");
        this.quantity=quantity;
    }
    //Obtem a quantidade
    public int getQuantity(){
        return quantity;
    }
    //Configura preço por item
    public void setPricePerItem(double pricePerItem){
        if(pricePerItem < 0.0) //Valida pricePerItem
            throw new IllegalArgumentException("Price per item must be >= 0");
        this.pricePerItem=pricePerItem;
    }
    //Obtem preço por item
    public double getPricePerItem(){
        return pricePerItem;
    }
    //Retorna da representação de String do objeto Invoice
    @Override
    public String toString(){
        return String.format("%s: %n%s: %s (%s) %n%s: %d %n%s: $%,.2f",
        "invoice","partnumber",getPartNumber(),getPartDescription(),"quantity",
        getQuantity(),"price per item", getPricePerItem());
    }
    //Método requerido para executar o contrato com a interface payable
    @Override
    public double getPayementAmount(){
        return getQuantity() * getPricePerItem(); //Calcula o custo total
    }
}//Fim da classe Invoice