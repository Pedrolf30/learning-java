//Payable.java
//Declaração da interface payable

public interface Payable{
    double getPaymentAmount(); //Calcula pagamento; nenhuma implementação
}