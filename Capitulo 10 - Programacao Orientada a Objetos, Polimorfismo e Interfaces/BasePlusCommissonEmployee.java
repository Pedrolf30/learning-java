//BasePlusCommissonEmployee.java
//Classe BasePlusCommissonEmployee estende a CommissionEmployee

public class BasePlusCommissonEmployee extends CommissionEmployee{
    private double baseSalary; //Salario base por semana

    //Construtor
    public BasePlusCommissonEmployee(String fN, String lN, String sSN,
    double grossSales,double commissionRate, double baseSalary){
        super(fN,lN,sSN,grossSales,commissionRate);

        if(baseSalay < 0.0)
            throw new IllegalArgumentException("Base salary must be >= 0.0");

        this.baseSalary=baseSalary;
    }
    //Configuração de salario-base
    public void setBaseSalary(double baseSalary){
        if(baseSalay < 0.0)
            throw new IllegalArgumentException("Base salary must be >= 0.0");

        this.baseSalary=baseSalary;
    }
    //Retorna o salario-base
    public double getBaseSalary(){
        return baseSalary;
    }
    //Calcula os vencimentos, sobrescreve o método earning em CommissionEmployee
    @Override
    public double earning(){
        return getBaseSalary() + super.earning();
    }
    //Retorna a representação String do objeto BasePlusCommissionEmployee
    @Override
    public String toString(){
        return String.format("%s: %s; %s: $%,.2f", "base-salaried", super.toString(),"base salary", getBaseSalary());
    }
}//Fim da classe BasePlusCommissionEMployee