//PayableInterfaceTest.java
//Programa de teste da interface Payable que processa Invoice e 
//Employees polimorficamente

public class PayableInterfaceTest{
    public static voi main(String[]args){

        //cria array Payable de quatro elementos
        Payable[] payableObjects = new Payable[4];

        //preenche o array com objetos que implementam payable
        payableObjects[0] = new Invoice("01234","seat",2, 375.00);
        payableObjects[1] = new Invoice("5678","tire",4, 79.95);
        payableObjects[2] = new Invoice("John","Smith","111-11-1111", 800.0);
        payableObjects[3] = new Invoice("Lisa","Barnes","8888-888-888", 1200.00);

        System.out.println("Invoices and employees processed polymorphically");

        //processa genericamente cada elemento no array payableObjects
        for(Payable currentPayable : payableObjects){

            //gera um saida de currentPayable e sua quantia de pagamento apropriada
            System.out.printf("%n%s %n%s: $%,.2f%n", currentPayable.toString(),
            "payment due", currentPayable.getPaymentAmount());
        }
    }//Fim de main
}//Fim da classe