//Employee.java
//Superclasse abstrata Employee

public abstract class Employee{
    private final String fN;
    private final String lN;
    private final String sSN;

    //Construtor
    public Employee(String fN, String lN, String sSN){
        this.fN=fN;
        this.lN=lN;
        this.sSN=sSN;
    }
    //Retorna o nome
    public String getFN(){
        return fN;
    }
    //Retorna o sobrenome
    public String getLN(){
        return lN;
    }
    //Retorna o numero do seguro social
    public String getSSN(){
        return sSN;
    }
    //Retorna a representacao de String do objeto Employee
    @Override
    public String toString(){
        return String.format("%s %sn social security number: %s",
        getFN(),getLN(),getSSN());
    }
    //O metodo abstract deve ser sobrescrito pelas subclasses concretas
    public abstract double earnings();//Nenhuma implementação aqui
}//Fim da classe abstrata Employee