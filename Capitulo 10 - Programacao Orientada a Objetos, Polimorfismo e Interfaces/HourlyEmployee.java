//HourlyEmployee.java
//Classe HourlyEmployee estende Employee

public class HourlyEmployee extends Employee{
    private double wage; //Salario por hora
    private double hours; //Horas trabalhadas durante a semana

    //Construtor
    public HourlyEmployee(String fN,String lN,String sSN,
     double wage, double hours){
         super(fN,lN,ssN);
         if(wage < 0.0) //valida remuneração
            throw new IllegalArgumentException("Hourly wage must be >= 0.0");

        if((hours < 0.0) || (hours > 168.0)) //Valida horas
            throw new IllegalArgumentException("Hours worked must be >= 0.0 and <= 168.0");
        
        this.wage=wage;
        this.hours=hours;
     }
     //Configura remuneração
     public void setWage(double wage){
        if(wage < 0.0) //valida remuneração
          throw new IllegalArgumentException("Hourly wage must be >= 0.0"); 
        this.wage=wage;
     } 
     //Retorna a remuneração
     public double getWage(){
         return wage;
     }
     //Configura horas
     public void setHours(double hours){
        if((hours < 0.0) || (hours > 168.0)) //Valida horas
            throw new IllegalArgumentException("Hours worked must be >= 0.0 and <= 168.0");
        this.hours=hours;
     }
     //Retorna horas
     public double getHours(){
         return hours;
     }
     //Calcula os rendimentos, sobrescreve o metodo earning em employee
     @Override
     public double earning(){
         if(getHours() <=40)//nenhuma hora extra
            return getWage() * getHours();
        else
            return 40*getWage() + (getHours() - 40) * getWage() * 1.5;
     }
     //Retorna a representação de String do objeto HourlyEmployee
     @Override
     public String toString(){
         return String.format("hourly employee: %s%n%s: $%,.2f;%s:%,.2f",
         super.toString(), "hourly wage", getWage(),"hour worked", getHours());
     }
}//Fim da classe HourlyEmployee