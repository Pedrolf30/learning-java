//CommissionEmployee.java
//Classe CommissionEmployee estende Employee

public class CommissionEmployee extends Employee{
    private double grossSales; //Vendas brutas semanais
    private double commissionRate; //Porcentagem da comissão

    //Construtor
    public CommissionEmployee(String fN,String lN, String sSN,
    double grossSales, double commissionRate){
        super(fN,lN,sSN);

        if(commissionRate <= 0.0 || commissionRate >= 1.0)
            throw new IllegalArgumentException("Commission rate must be > 0.0 and <1.0");
        if(grossSales < 0.0)
            throw new IllegalArgumentException("Gross sales must be >= 0.0");

        this.grossSales=grossSales;
        this.commissionRate=commissionRate;
    }
    //Configura a quantidade de vendas brutas
    public void setGrossSales(double grossSales){
        if(grossSales < 0.0)
            throw new IllegalArgumentException("Gross sales must be >= 0.0");
        this.grossSales=grossSales; 
    }
    //Retorna a quantidade de vendas brutas
    public double getGrossSales(){
        return grossSales;
    }
    //Configura valor comissão
    public void setCommissionRate(double commissionRate){
        if(commissionRate <= 0.0 || commissionRate >= 1.0)
            throw new IllegalArgumentException("Commission rate must be > 0.0 and <1.0");
        this.commissionRate=commissionRate;
    }
    //Retorna valor da comissão
    public double getCommissionRate(){
        return commissionRate;
    }
    //Calcula os rendimentos, sobrescreve o método earning em Employee
    @Override
    public double earning(){
        return getCommissionRate() * getGrossSales();
    }
    //Retorna a representação String do objeto CommissionEmployee
    public String toString(){
        return String.format("%s: %s%n%s: $%,.2f; %s: %.2f",
        "commission employee", super.toString(), "gross sales",
        getGrossSales(), "commission rate", getCommissionRate());
    }
}//Fim da classe CommissionEmployee