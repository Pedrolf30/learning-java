//SalariedEmployee.java
//A classe concreta SalariedEmployee estende a classe abstrata Employee

public class SalariedEmployee extends Employee{
    private double weeklySalary;

    //Constructor
    public SalariedEmployee(String fN, String lN, String sSN, double weeklySalary){
        super(fN,lN,sSN);

        if(weeklySalary < 0.0)
            throw new IllegalArgumentException("Weekly salary must be >= 0.0");
        
        this.weeklySalary=weeklySalary;
    }
    //Configura o salario
    public void setWeeklySalary(double weeklySalary){
        if(weeklySalary < 0.0)
            throw new IllegalArgumentException("Weekly salary must be >= 0.0");
        
        this.weeklySalary=weeklySalary;
    }
    //Retorna o salario
    public double getWeeklySalary(){
        return weeklySalary;
    }
    //Calcula os rendimentos, sobrescreve o metodo earning em Employee
    @Override
    public double earnings(){
        return getWeeklySalary();
    }
    //Retorna a representacao String do objeto SalariedEmployee
    @Override
    public String toString(){
        return String.format("Salaried employee: %s%n%s: $%,.2f", super.toString(),"weekly salary", getWeeklySalary());
    }
}//Fim da classe SalariedEmployee