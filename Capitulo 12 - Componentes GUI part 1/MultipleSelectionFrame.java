//MultipleSelectionFrame.java
//JList que permite multiplas seleções

import java.awt.FlowLayout;
import java.awt.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

public class MultipleSelectionFrame{
    private final JList<String> colorJList; //Lista para exibir cores
    private final JList<String> copyJList; //Lista para armazena nomes copiados
    private JButton copyJButton; //Botão para copiar nomes selecionados
    private static final String[] colorNames = {"Black", "Blue", "Cyan", "Dark Gray",
     "Gray", "Green", "Light Gray", "Magenta", "Orange", "Pink",
      "Red", "White", "Yellow"};
    
    //Construtor MultipleSelectionFrame
    public MultipleSelectionFrame(){
        super("Multiple Selection Frame");
        setLayout(new FlowLayout());

        colorJList = new JList<String>(colorNames);//Lista de colorNames
        colorJList.setVisibleRowCount(5); //Exibe Cinco linha de uma vez
        colorJList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        add(new JScrollPane(colorJList));

        copyJButton = new JButton("Copy >>>");
        copyJButton.addActionListener(
            new ActionListener(){
                //Trata evento de botão
                @Override
                public void actionPerformed(ActionEvent event){
                    //Coloca valores selecionados na copyJList
                    copyJList.setListData(
                        colorJList.getSelectedValuesList().toArray(
                            new String[0]));
                }
            }
        );
        add(copyJButton);
        
        copyJList = new JList<String>(); //Lista para armazena nomes selecionados
        copyJList.setVisibleRowCount(5); //Mostra 5 linhas
        copyJList.setFixesCellWidth(100); //Configura largura
        copyJList.setFixedCellHeight(15); //Configura altura
        copyJList.setSelectionMode(
            ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        add(new JScrollPane(copyJList)); //Adiciona lista com scrollpane
        
    }

}//Fim da classe MultipleSelectionFrame
