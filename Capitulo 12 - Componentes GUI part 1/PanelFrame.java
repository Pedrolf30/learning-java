//PanelFrame
//Utilizando um JPanel para ajudar a fazer o layout dos componentes
import java.awt.GridLayout;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;

public class PanelFrame extends JFrame{
    private final JPanel buttonJPanel; //Painel para armazenar botoes
    private final JButton[] buttons;

    //Construtor sem argumento
    public PanelFrame(){
        super("Panel Demo");
        buttons = new JButton[5];
        buttonJPanel = new JPanel();
        buttonJPanel.setLayout(new GridLayout(1,buttons.length));

        //Cria e adiciona botões
        for(int c = 0;c < buttons.length; c++){
            buttons[c] = new JButton("Button "+(c+1));
            buttonJPanel.add(buttons[c]);//Adiciona botão ao painel
        }
        add(buttonJPanel, BorderLayout.SOUTH); //Adiciona painel ao JFrame
    }
}//FIm da classe PanelFrame