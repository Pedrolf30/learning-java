//LabelFrame.java
//JLabels com texto e icones
import java.awt.FlowLayout; //Especifica como os componentes são organizados
import javax.swing.JFrame; //Fornece recursos basicos de janela
import javax.swing.JLabel; //Exibe texto e imagens
import javax.swing.SwingConstants; //Constantes comuns utilizadas com Swing
import javax.swing.Icon; //Interface utilizada para manipular imagens
import javax.swing.ImageIcon; //Carrega imagens

public class LabelFrame extends JFrame{
    private final JLabel label1; //JLabel apenas com texto
    
    private final JLabel label3; //JLabel com texto e icone adicionados

    //Constutor LabelFrame adiciona JLabels a JFrame
    public LabelFrame(){
        super("Testing label");
        setLayout(new FlowLayout()); //Configura o layout de frame

        //Construtor JLabel com um argumento de string
        label1 = new JLabel("Label with text");
        label1.setToolTipText("This is label1");
        add(label1); //Adiciona label1 ao JFrame

        //Construtor JLabel com string, Icon e argumentos de alinhamento
        
        label3 = new JLabel(); //Construtor JLabel sem argumentos
        label3.setText("Label with icone and text at bottom");
        
        label3.setHorizontalTextPosition(SwingConstants.CENTER);
        label3.setVerticalTextPosition(SwingConstants.BOTTOM);
        label3.setToolTipText("This is label3");
        add(label3); //Adiciona label3 ao JFrame
    }
}//Fim da classe LabelFrame