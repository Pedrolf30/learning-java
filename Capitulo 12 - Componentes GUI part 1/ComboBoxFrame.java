//ComboBoxFrame.java
//JComboBox que exibe uma lista de nomes de imagem
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.event.ItemListener;
import javax.swing.JComboBox;
import java.awt.event.ItemEvent;
import javax.swing.JFrame;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class ComboBoxFrame extends JFrame{
    private final JComboBox<String> imagesJComboBox; //Contem nomes de icone
    private final JLabel label; //Exibe o icone selecionado

    private static final String[] names = {"bug1.gif","bug2.gif","travelbyg.gif",
"buganim.gif"};
    private final Icon[] icons = {new ImageIcon(getClass().getResource(names[0])),
    new ImageIcon(getClass().getResource(names[1])),
    new ImageIcon(getClass().getResource(names[2])),
    new ImageIcon(getClass().getResource(names[3]))};

    //Construtor ComboBoxFrame adiciona JComboBox ao JFrame
    public ComboBoxFrame(){
        super("Testing JComboBox");
        setLayout(new FlowLayout()); //Configura layout de frame

        imagesJComboBox = new JComboBox<String>(names); //Configura JComboBox
        imagesJComboBox.setMaximumRowCount(3); //Exibe tres linhas
        imagesJComboBox.addItemListener(
            new ItemListener(){
                //trata evento JComboBox
                @Override
                public void itemStateChanged(ItemEvent event){
                    //Determina se o item esta selecionado
                    if(event.getStateChange()==ItemEvent.SELECTED)
                        label.setIcon(icons[
                            imagesJComboBox.getSelectedIndex()]);
                }
            }//Fim da classe interna anonima
        );//Fim da chamada para addItemListener
        add(imagesJComboBox);//Adiciona caixa de combinacao ao JFrame
        label = new JLabel(icons[0]);//Exibe primeiro icone
        add(label); //Adiciona rotulo ao JFrame
    }
}//Fim da classe ComboBoxFrame

