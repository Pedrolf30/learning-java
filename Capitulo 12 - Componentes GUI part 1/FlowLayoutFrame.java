//FlowLayoutFrame.java
//FlowLayout permite que os componentes fluam ao longo de multiplas linhas
import java.awt.FlowLayout;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JButton;

public class FlowLayoutFrame extends JFrame{
    private final JButton leftJButton; //Configura alinhamento do botão esquerda
    private final JButton centerJButton; //Configura alinhamento do botao centro
    private final JButton rightJButton; //COnfigura alinhamento do botao direita
    private final FlowLayout layout; //Objeto de layout
    private final Container container; //Conteiner para configurar layout

    //Configura GUI e registra listeners de botão
    public FlowLayoutFrame(){
        super("FlowLayout demo");
        layout = new FlowLayout();
        container = getContentPane(); //Obtem container para layout
        setLayout(layout);

        //Configura leftJButton e registra listener
        leftJButton = new JButton("Left");
        add(leftJButton); //Adiciona o botão left ao frame
        leftJButton.addActionListener(
            new ActionListener(){ //Classe interna anonima
                //Processa o evento leftJButton
                @Override
                public void actionPerformed(ActionEvent event){
                    layout.setAlignment(FlowLayout.LEFT);

                    //Realinha os componentes anexados
                    layout.layoutContainer(container);
                }
            }
        );
        //Configura centerJButton e registra o listener
        centerJButton = new JButton("Center");
        add(centerJButton); //Adiciona o botão center ao frame
        centerJButton.addActionListener(
            new ActionListener(){ //Classe interna anonima
                //Processa o evento centerJButton
                @Override
                public void actionPerformed(ActionEvent event){
                    layout.setAlignment(FlowLayout.CENTER);

                    //Realinha os componentes anexados
                    layout.layoutContainer(container);
                }
            }
        );
        //Configura rightJButton e registra o listener
        rightJButton = new JButton("Right");
        add(rightJButton); //Adiciona o botão right ao frame
        rightJButton.addActionListener(
            new ActionListener(){ //Classe interna anonima
                //Processa o evento rightJButton
                @Override
                public void actionPerformed(ActionEvent event){
                    layout.setAlignment(FlowLayout.RIGHT);

                    //Realinha os componentes anexados
                    layout.layoutContainer(container);
                }
            }
        );

    }//Fim do construtor FlowLayoutFrame
}//Fim da classe FlowLayoutFrame