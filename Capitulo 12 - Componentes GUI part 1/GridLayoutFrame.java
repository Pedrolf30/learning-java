//GridLayoutFrame.java
//GridLayout contendo seus botoes
import java.awt.GridLayout;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JButton;

public class GridLayoutFrame extends JFrame implements ActionListener{
    private final JButton[] buttons; //Array de botoes
    private static final String[] names = {"one","two","three","four","five","six"};
    private boolean toggle = true;//Alterna entre dois layouts
    private final Container container;//Container do frame
    private final GridLayout gridLayout1; //Primeiro layout
    private final GridLayout gridLayout2; //Segundo layout

    //Construtor sem argumento
    public GridLayoutFrame(){
        super("GridLayout Demo");
        gridLayout1 = new GridLayout(2,3); //2 por 3 
        gridLayout2 = new GridLayout(3,2); //3 por 2
        container = getContentPane();
        setLayout(gridLayout1);
        buttons = new JButton[names.length];

        for(int c =0;c<names.length;c++){
            buttons[c] = new JButton(names[c]);
            buttons[c].addActionListener(this); //Ouvinte registrado
            add(buttons[c]); //Adiciona o botao ao JFrame
        }
    }

    //Trata eventos de botao alternando entre layouts
    @Override
    public void actionPerformed(ActionEvent event){
        if(toggle) //Define layout com base nos botoes de alternacao
            container.setLayout(gridLayout2);
        else
            container.setLayour(gridLayout1);

        toggle = !toggle;
        container.validate(); //Refaz o layout do container
    }
}//Fim da classe GridLayoutFrame