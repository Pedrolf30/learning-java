//CheckBoxFrame.java
//JCheckBoxes e eventos de item
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JCheckBox;

public class CheckBoxFrame extends JFrame{
    private final JTextField textField; //Exibe o texto na alteração de fontes
    private final JCheckBox boldJCheckBox; //Para marcar/desmarcar estilo negrito
    private final JCheckBox italicJCheckBox;//Para marcar/desmarcar estilo italico

    //Construtor CheckBoxFrame adiciona JCheckBoxes ao JFrame
    public CheckBoxFrame(){
        super("JCheckBox Test");
        setLayout(new FlowLayout());

        //Configura JTextField e a sua fonte
        textField = new JTextField("Watch the font style change", 20);
        textField.setFont(new Font("Serif",Font.PLAIN, 14));
        add(textField); //Adiciona textField ao JFrame

        boldJCheckBox = new JCheckBox("Bold");
        italicJCheckBox = new JCheckBox("Italic");
        add(boldJCheckBox); //Adiciona caixa de seleção de estilo negrito ao JFrame
        add(italicJCheckBox); //Adiciona caixa de seleção de estilo italico ao JFrame

        //Listeners registradores para JCheckBoxes
        CheckBoxHandler handler = new CheckBoxHandler();
        boldJCheckBox.addItemListener(handler);
        italicJCheckBox.addItemListener(handler);
    }

    //Classe interna private para tratamento de evento ItemListener
    private class CheckBoxHandler implements ItemListener{

        //Responde aos eventos de caixa de seleção
        @Override
        public void itemStateChanged(ItemEvent event){
            Font font = null; //Armazena nova fonte

            //Determina quais CheckBoxes estão marcadas e cria Font
            if(boldJCheckBox.isSelected() && italicJCheckBox.isSelected())
                font = new Font("Serif", Font.BOLD + Font.ITALIC, 14);
            else if(boldJCheckBox.isSelected())
                font = new Font("Serif",Font.BOLD,14);
            else if(italicJCheckBox.isSelected())
                font = new Font("Serif",Font.ITALIC,14);
            else
                font = new Font("Serif",Font.PLAIN,14);
            
            textField.setFont(font);
        }
    }
}//Fim da classe CheckBoxFrame