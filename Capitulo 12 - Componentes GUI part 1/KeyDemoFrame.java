//KeyDemoFrame.java
//Tratamento de evento de teclado
import java.awt.Color;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class KeyDemoFrame extends JFrame implements KeyListener{
    private final String line1 = ""; //Primeira linha da area de texto
    private final String line2 = ""; //Segunda linha da area de texto
    private final String line3 = ""; //Terceira linha da area de texto
    private final JTextArea textArea; //Area de texto para exibir a saida

    //Construto KeyDemoFrame
    public KeyDemoFrame(){
        super("Demonstrating Keystroke Events");

        textArea = new JTextArea(10,15); //Configura JTextArea
        textArea.setText("Press any key on the keyboard...");
        textArea.setEnabled(true);
        textArea.setDisabledTextColor(Color.BLACK);
        add(textArea); //Adiciona area de texto ao JFrame

        addKeyListener(this); //Permite que o frame processe os eventos de teclado
    }
    //Trata pressionamento de qualquer tecla
    @Override
    public void keyPressed(KeyEvent event){
        line1=String.format("Key pressed: %s",KeyEvent.getKeyText(event.getKeyCode()));//Mostra a tecla pressionada
        setLines2and3(event); //Configura a saida das linhas dois e tres
    }
    //Trata liberação de qualquer tecla
    @Override
    public void keyReleased(KeyEvent event){
        line1=String.format("Key released: %s",
        KeyEvent.getKeyText(event.getKeyCode())); //Mosta a tecla liberada
        setLines2and3(event); //Configura a saida das linhas dois e tres
    }
    //Trata pressionamento de uma tecla de ação
    @Override
    public void keyTyped(KeyEvent event){
        line1=String.format("Key typed: %s",event.getKeyChar());
        setLines2and3(event); //Configura a saida das linhas dois e tres
    }
    //Cinfigura a segunda e terceira linhas de saida
    private void setLines2and3(KeyEvent event){
        line2=String.format("This key is %san action key",
        (event.isActionKey() ? "" : "not "));
        String tempo = KeyEvent.getKeyModifiersText(event.getModifiers());

        line3 = String.format("Modifier keys pressed: %s",
        (temp.equals("") ? "none" : temp)); //Modificadores de saida

        textArea.setText(String.format("s\n%s\n%s\n", line1,line2,line3));
    }
}//Fim da classe KeyDemoFrame