//MouseTrackerFrame.java
//Tratamentos de evento de mouse
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MouseTrackerFrame extends JFrame{
    private final JPanel mousePanel; //Painel em que os eventos de mouse ocorrem
    private final JLabel statusBar; //Exibe informações do evento

    //Construtor MouseTrackerFrame configura GUI e 
    //registra rotinas de tratamento de evento de mouse
    public MouseTrackerFrame(){
        super("Demonstrating Mouse Events");

        mousePanel = new JPanel();
        mousePanel.setBackground(Color.WHITE);
        add(mousePanel, BorderLayout.CENTER); //Adiciona painel ao JFrame

        statusBar = new JLabel("Mouse outside JPanel");
        add(statusBar, BorderLayout.SOUTH); //Adiciona rotulo ao JFrame

        //Cria e registra listener para mouse e eventos de movimento de mouse
        MouseHandler handler = new MouseHandler();
        mousePanel.addMouseListener(handler);
        mousePanel.addMouseMotionListener(handler);
    }
    private class MouseHandler implements MouseListener,
         MouseMotionListener{
             //Rotina de tratamentos de evento Mouse Listener
             //Trata evento quando o mouse é liberado imediatamento depois de ter sido pressionado
             @Override
             public void mouseClicked(MouseEvent event){
                 statusBar.setText(String.format("Clicked at[%d, %d]",
                 event.getX(),event.getY()));
             }
             //Trata evento quando mouse é pressionado
             public void mousePressed(MouseEvent event){
                statusBar.setText(String.format("Pressed at[%d, %d]",
                event.getX(),event.getY()));
             }
             //Trata evento quando mouse é liberado
             public void mouseReleased(MouseEvent event){
                statusBar.setText(String.format("Released at[%d, %d]",
                event.getX(),event.getY()));
            }
            //Trata evento quando mouse entra na área
            public void mouseEntered(MouseEvent event){
                statusBar.setText(String.format("Entered at[%d, %d]",
                event.getX(),event.getY()));
                mousePanel.setBackground(Color.GREEN);
            }
            //Trata evento quando mouse sai da área
            public void mouseExited(MouseEvent event){
                statusBar.setText("Mouse outside JPanel");
                mousePanel.setBackground(Color.WHITE);
            }
            //Rotinas de tratamento de evento MouseMotionListener
            //Trata o evento quando o ussuario arrasta o mouse com o botão pressionado
            @Override
            public void mouseDragged(MouseEvent event){
                statusBar.setText(String.format("Dragged at[%d, %d]",
                event.getX(),event.getY()));
            }
            //Trata o evento quando o usuario move o mouse
            public void mouseMoved(MouseEvent event){
                statusBar.setText(String.format("Moved at[%d, %d]",
                event.getX(),event.getY()));
            }
    }//Fo, da classe interna MouseHandler
}//Fim da classe MouseTrackerFrame
