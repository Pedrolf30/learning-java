//PaintPanel.java
//Classe de adaptadores utilizada para implementar rotinas de tratamento de evento
import java.awt.Point;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import javax.swing.JPanel;

public class PaintPanel extends JPanel{
    //Lista de referencias Point
    private final ArrayList<Point> points = new ArrayList<>();

    //Configura GUI e registra rotinas de tratamento de evento de mouse
    public PaintPanel(){
        //Trata evento de movimento de mouse do frame
        addMouseMotionListener(
            new MouseMotionAdapter(){ //Classe interna anonima
                //Armazena coordenadas da acao de arrastar e repintas
                @Override
                public void mouseDragged(MouseEvent event){
                    points.add(event.getPoint());
                    repaint(); //Repinta JFrame
                }
            }
        );
    }
    //Desenha ovais em um quadro delimitador de 4x4 nas localizacoes especificadas
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);//Limpa a area do desenho

        //Desenha todos os pontos
        for(Point point:points)
            g.fillOval(point.x, point.y, 4, 4);
    }
}//Fim da classe PaintPanel