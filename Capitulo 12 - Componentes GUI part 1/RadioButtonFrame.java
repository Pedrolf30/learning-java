//RadioButtonFrame.java
//Criando botões de opção utlizando ButtonGroup e JRadioButton
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ItemListener;
import java.util.concurrent.Flow;
import java.awt.event.ItemEvent;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

public class RadioButtonFrame extends JFrame{
    private final JTextField textField; //Utilizado para exibir alterações de fonte
    private final Font plainFont; //Fonte para texto simples
    private final Font boldFont; //Fonte para texto em negrito
    private final Font italicFont; //Fonte para texto em italico
    private final Font boldItalicFont; //Fonte para texto em negrito e italico
    private final JRadioButton plainJRadioButton; //Seleciona texto simples
    private final JRadioButton boldJRadioButton; //Seleciona texto em negrito
    private final JRadioButton italicJRadioButton; //Seleciona texto em italico
    private final JRadioButton boldItalicJRadioButton; //Seleciona texto em negrito e italico
    private final ButtonGroup radioGroup; //Contem todos os botões de opção

    //Construtor RadioButtonFrame adiciona JRadioButtons ao JFrame
    public RadioButtonFrame(){
        super("RadioButton Test");
        setLayout(new FlowLayout());

        textField = new JTextField("Watch the font style chance",25);
        add(textField); //Adiciona textField ao JFrame

        //Cria botões de opção
        plainJRadioButton = new JRadioButton("Plain", true);
        boldJRadioButton = new JRadioButton("Bold", false);
        italicJRadioButton = new JRadioButton("Italic", false);
        boldItalicJRadioButton = new JRadioButton("Bold/Italic", false);
        add(plainJRadioButton);
        add(boldJRadioButton);
        add(italicJRadioButton);
        add(boldItalicJRadioButton);

        //Cria relacionamento lógico entre JRadioButtons
        radioGroup = new ButtonGroup(); //Cria button group
        radioGroup.add(plainJRadioButton);
        radioGroup.add(boldJRadioButton);
        radioGroup.add(italicJRadioButton);
        radioGroup.add(boldItalicJRadioButton);

        //Cria fonte objetis
        plainFont = new Font("Serif",Font.PLAIN,14);
        boldFont = new Font("Serif", Font.BOLD,14);
        italicFont = new Font("Serif",Font.ITALIC,14);
        boldItalicFont = new Font("Serif",Font.BOLD+Font.ITALIC,14);
        textField.setFont(plainFont);

        //Registra eventos para JRadioButtons
        plainJRadioButton.addItemListener(new RadioButtonHandler(plainFont));
        boldJRadioButton.addItemListener(new RadioButtonHandler(boldFont));
        italicJRadioButton.addItemListener(new RadioButtonHandler(italicFont));
        boldItalicJRadioButton.addItemListener(new RadioButtonHandler(boldItalicFont));
    }
    //Classe Interna private para tratar eventos de botao de opcao
    private class RadioButtonHandler implements ItemListener{
        private Font font;//Fonte associada com esse listener
        public RadioButtonHandler(Font f){
            font = f;
        }
        //Trata eventos de botao de opcao
        @Override
        public void itemStateChanged(ItemEvent event){
            textField.setFont(font);
        }
    }
}//Fim da classe RadioButtonFrame
