//TextAreaFrame.java
//Copiando texto selecionado de uma area de JText para outra
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JScrollPane;

public class TextAreaFrame extends JFrame{
    private final JTextArea textArea1;//Exibe a string demo
    private final JTextArea textArea2;//Texto destacado é copiado aqui
    private final JButton copyJButton;//Começa a copiar o texto

    //Construto sem argumento
    public TextAreaFrame(){
        super("TextArea Demo");
        Box box = Box.createHorizontalBox(); //Cria uma box
        String demo = "This is a demo string to\n"+
        "illustrate copying text\nfrom one text area to \n"+
        "another textarea using an\nexternal event";

        textArea1 = new JTextArea(demo,10,15);
        box.add(new JScrollPane(textArea1)); //Adiciona scrollpane

        copyJButton = new JButton("Copy >>>"); //Cria o botão de copia
        box.add(copyJButton);//Adiciona botão de copia na box
        copyJButton.addActionListener(
            new ActionListener(){ //Classe interna anonima
                //Configura texto em textArea2 como texto selecionado de textArea1
                @Override
                public void actionPerformed(ActionEvent event){
                    textArea2.setText(textArea1.getSelectedText());
                }
            }
        );
        textArea2 = new JTextArea(10,15);
        textArea2.setEditable(false);
        box.add(new JScrollPane());//Adiciona scrollpane

        add(box);//Adiciona box ao frame
    }
}//Fim da classe TextAreaFrame