//BorderLayoutFrame.java
//BorderLayout contendo cinco botões
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JButton;

public class BorderLayoutFrame extends JFrame implements ActionListener{
    private final JButton[] buttons; //Array de botões para ocultar partes
    private static final String[] names = {"Hide North","Hide South",
    "Hide East","Hide West","Hide Center"};
    private final BorderLayout layout;

    //Configura GUI e tratamento de evento
    public BorderLayoutFrame(){
        super("BorderLayout Demo");

        layout = new BorderLayout(5, 5); //Espaço de 5 pixels
        setLayout(layout);
        buttons = new JButton[names.length];

        //Cria JButtons e registra ouvintes para eles
        for(int c =0;c<names.length;c++){
            buttons[c] = new JButton(names[c]);
            buttons[c].addActionListener(this);
        }
        add(buttons[0], BorderLayout.NORTH);
        add(buttons[1], BorderLayout.SOUTH);
        add(buttons[2], BorderLayout.EAST);
        add(buttons[3], BorderLayout.WEST);
        add(buttons[4], BorderLayout.CENTER);
    }

    //Trata os eventos de botão
    @Override
    public void actionPerformed(ActionEvent event){

        //Verifica a origem do eveno e o painel de conteudo de layout de acordo
        for(JButton button:buttons){
            if(event.getSource()==button)
                button.setVisible(false);//Oculta botão que foi clickado
            else
                button.setVisible(true);//Mostra os outros botões
        }
        layout.layoutContainer(getContentPane()); //Define o layout do painel de conteudo
    }
}//Fim da classe BorderLayoutFrame