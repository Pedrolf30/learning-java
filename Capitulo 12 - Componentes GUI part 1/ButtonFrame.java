//ButtonFrame.java
//Botões de comando e eventos de ação
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class ButtonFrame extends JFrame{
    private final JButton plainJButton;//Botão com apenas texto
    private final JButton fancyJButton;//Botão com icones

    //ButtonFrame adiciona JButtons ao JFrame
    public ButtonFrame(){
        super("Testing Buttons");
        setLayout(new FlowLayout());

        plainJButton = new JButton("Plain Button");//Botão com texto
        add(plainJButton); //Adiciona plainJButton ao JFrame

        Icon bug1 = new ImageIcon(getClass().getResource("bug1.gif"));
        Icon bug2 = new ImageIcon(getClass().getResource("bug2.gif"));
        fancyJButton = new JButton("Fancy Button", bug1); //Configura imagem
        fancyJButton.setRolloverIcon(bug2); //Configura imagem de rollover
        add(fancyJButton); //Adiciona fancyJButton ao JFrame

        //Cria novo ButtonHandler de tratamento de evento de botão
        ButtonHandler handler = new ButtonHandler();
        fancyJButton.addActionListener(handler);
        plainJButton.addActionListener(handler);
    }

    //Classe interna para tratamento de eventos de botão
    private class ButtonHandler implements ActionListener{
        //Trata evento de botão
        @Override
        public void actionPerformed(ActionEvent event){
            JOptionPane.showMessageDialog(ButtonFrame.this, String.format("You pressed: %s",
            event.getActionCommand()));
        }
    }
}//Fim da classe ButtonFrame