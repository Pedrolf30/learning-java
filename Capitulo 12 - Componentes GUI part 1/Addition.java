//Addition.java
//Programa de adição que utiliza JOptionPane para entrada e saida
import javax.swing.JOptionPane;

public class Addition{
    public static void main(String[]arg){
        //Obtém a entrada de usuario a partir dos dialogos de entrada JOptionPane
        String firstNumber = JOptionPane.showInputDialog("Enter first integer");
        String secondNumber = JOptionPane.showInputDialog("Enter second integer");

        //Converte String em valores int para utilização em um calculo
        int number1 = Integer.parseInt(firstNumber);
        int number2 = Integer.parseInt(secondNumber);

        int sum = number1 + number2;

        //Exibe o resultado em um dialogo de mensagem JOptionPane
        JOptionPane.showMessageDialog(null, "The sum is: "+ sum, "Sum of two integers", JOptionPane.PLAIN_MESSAGE);
    }
}//Fim da classe Addition
