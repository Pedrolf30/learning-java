//MouseDetailsFrame.java
//Demonstrando cliques de mouse e distinguindo entre botoes do mouse
import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MouseDetailsFrame extends JFrame{
    private String details; //String exibida na statusBar
    private final JLabel statusBar; //JLabel na parte inferior da janela

    //Construtor configura String de barra de titulo e registra o listener de mouse
    public MouseDetailsFrame(){
        super("Mouse clicks and buttons");

        statusBar = new JLabel("Click the mouse");
        add(statusBar, BorderLayout.SOUTH);
        addMouseListener(new MouseClickHandler()); //Adiciona tratamento de evento
    }

    //Classe interna para tratar eventos de mouse
    private class MouseClickHandler extends MouseAdapter{
        //Trata evento de cliqe de mouse e determina qual botao foi pressionado
        @Override
        public void mouseClicked(MouseEvent event){
            int xPos = event.getX(); //Obtem a posicao x do mouse
            int yPos = event.getY(); //Obtem a posicao y do mouse
            details = String.format("Clicked %d time(s)", event.getClickCount());

            if(event.isMetaDown()) //Botao direito do mouse
                details += "with right mouse button";
            else if(event.isAltDown()) //Botao do meio do mouse
                details += "with center mouse button";
            else //Botao esquerdo do mouse
                details += "with left mouse button";

            statusBar.setText(details);//Exibe mensagem em statusBar
        }
    }
}//Fim da classe MouseDetailsFrame