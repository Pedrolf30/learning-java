//TextFieldFrame.java
//JTextoField e JPasswordField
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JOptionPane;

public class TextFieldFrame extends JFrame{
    private final JTextField textField1; //Campo de texto com tamanho configurado
    private final JTextField textField2; //Campo de texto com texto
    private final JTextField textField3; //Campo de texto com texto e tamanho
    private final JPasswordField passwordField; //Campo de senha com texto

    //Construtor TextFieldFrame aficiona JTextFields a JFrame
    public TextFieldFrame(){
        super("Testing JTextField and JPasswordField");
        setLayout(new FlowLayout());

        //Cria campo de texto com 10 colunas
        textField1 = new JTextField(10);
        add(textField1); //Adiciona textField1 ao JFrame

        //Cria campo de texto com texto padrão
        textField2 = new JTextField("Enter text here");
        add(textField2); //Adiciona textField2 ao JFrame

        //Cria campo de texto com texto padrão e 21 colunas
        textField3 = new JTextField("Uniditable text field", 21);
        add(textField3); //Adiciona textField3 ao JFrame

        //Cria campo de senha com texto padrão
        passwordField = new JPasswordField("Hidden text");
        add(passwordField); //Adiciona passwordField ao JFrame

        //Rotina de tratamento de evento registradoras
        TextFieldHandler handler = new TextFieldHandler();
        textField1.addActionListener(handler);
        textField2.addActionListener(handler);
        textField3.addActionListener(handler);
        passwordField.addActionListener(handler);
    }
    
    //Clase interna private para tratamento de evento
    private class TextFieldHandler implements ActionListener{

        //processa eventos de campo de texto
        @Override
        public void actionPerformed(ActionEvent event){
            String string ="";

            //Usuario pressionou Enter no JTextFIeld textField1
            if(event.getSource() ==textField1)
                string = String.format("textField1: %s",
                  event.getActionCommand());

            //Usuario pressionou Enter no JTextField textField2
            else if(event.getSource()==textField2)
                string = String.format("textField2: %s",
                   event.getActionCommand());

            //Usuario pressionou Enter no JTextField textField3
            else if(event.getSource()==textField3)
                string = String.format("textField3: %s",
                   event.getActionCommand());

            //Usuario pressionou Enter no JTextField passwordField
            else if(event.getSource()==passwordField)
                string = String.format("passwordField: %s",
                    event.getActionCommand());

            //Exibe o conteudo de JTextField
            JOptionPane.showMessageDialog(null, string);
        }
    }//Fim da classe TextFieldHandler
}//Fim da classe TextFieldFrame