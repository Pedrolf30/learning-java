//StringMiscellaneous.java
//Esse aplicativo demonsta os métodos da classe String, length,
//charAt e getChars

public class StringMiscellaneous{
    public static void main(String[]args){
        String s1 = "Hello there";
        char[] charArray = new char[5];

        System.out.printf("S1: %s",s1);

        //Testa o método length
        System.out.printf("%nLenght of S1: %d", s1.length());

        //Faz loop pelos caracteres em s1 com charAt e os exibe na ordem
        System.out.printf("%nThe string reversed is: ");

        for(int c = s1.length() -1;c>=0;c--)
            System.out.printf("%c", s1.charAt(c));

        //Copia caracteres a partir de string para charArray
        s1.getChars(0,5,charArray,0);
        System.out.printf("%nThe character array is: ");

        for(char character : charArray)
            System.out.print(character);

        System.out.println();
    }
}//Fim da classe StringMiscellaneous