//ValidateInputTest.java
//Insere e valida os dados do usuario usando a classe ValidadeInput
import java.util.Scanner;

public class ValidateInputTest{
    public static void main(String[]args){
        //Obtem a entrada do usuario
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter first name:");
        String firstName = sc.nextLine();
        System.out.println("Please enter last name:");
        String lastName = sc.nextLine();
        System.out.println("Please enter address:");
        String address = sc.nextLine();
        System.out.println("Please enter city:");
        String city = sc.nextLine();
        System.out.println("Please enter state:");
        String state = sc.nextLine();
        System.out.println("Please enter zip:");
        String zip = sc.nextLine();
        System.out.println("Please enter phone");
        String phone = sc.nextLine();

        //Valida a entrada de usuario e exibe mensagem de erro
        System.out.println("\nValidate Result:");

        if(!ValidateInput.validateFirstName(firstName))
            System.out.println("Invalid first name");
        else if(!ValidateInput.validateLastName(lastName))
            System.out.println("Invalid last name");
        else if (!ValidateInput.validateAddress(address))
            System.out.println("Invalid address");
        else if (!ValidateInput.validateCity(city))
            System.out.println("Invalid city");
        else if (!ValidateInput.validateState(state))
            System.out.println("Invalid state");
        else if (!ValidateInput.validateZip(zip))
            System.out.println("Invalid zip code");
        else if (!ValidateInput.validatePhone(phone))   
            System.out.println("Invalid phone number");
        else
            System.out.println("Valid input. Thank you.");
    }
}//Fim da clasdr ValidateInoutTest