//SubString.java
//Metodos substring de classe string

public class SubString{
    public static void main(String[]args){
        String letters = "abcdefghijklmabcdefghijklm";

        //Testa metodos substring
        System.out.printf("Substracting from index 20 to end is \"%s\"%n",
            letters.substring(20));
        System.out.printf("%s \"%s\"%n","Substring from index 3 up to, but not including 6 is",
            letters.substring(3,6));
    }
}//Fim da classe Substring