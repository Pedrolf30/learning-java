//TokenTest.java
//Objeto StringTokeziner usado para tokezinar strings
import java.util.Scanner;
import java.util.StringTokenizer;

public class TokenTest{
    public static void main(String[]args){
        //Obtem a frase
        Scanner sc =  new Scanner(System.in);
        System.out.println("Enter a sentence and then press Enter");
        String sentence = sc.nextLine();

        //Processa a frase do usuario
        String[] tokens = sentence.split(" ");
        System.out.printf("Number of elements: %d%nThe tokens are:%n", tokens.length);

        for(String token : tokens)
            System.out.println(token);
    }
}//Fim da classe TokenTest