//StaticCharMethods2.java
//Métodos de conversão static da classe Character
import java.util.Scanner;

public class StaticCharMethods2{
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);

        //Obtem radical
        System.out.println("Please enter a radix:");
        int radix = sc.nextInt();

        //Obtem escolha do usuario
        System.out.printf("Please choose one: %n1 -- %s%n2 --%sn",
            "Convert digit to character","Convert character to digit");
        int choice = sc.nextInt();
    
        //Processa a solicitação
        switch(choice){
            case 1: //Converte digito em caractere
                System.out.println("Enter a digit");
                int digit = sc.nextInt();
                System.out.printf("Convert digit to character: %s%n",
                    Character.forDigit(digit, radix));
                break; 
            case 2: //Converte caracter em digito
                System.out.println("Enter a character");
                char character = sc.next().charAt(0);
                System.out.printf("Convert character to digit: %s%n",
                    Character.digit(character, radix));
                break;
        }
    }
}//Fim da classe StaticCharMethods2