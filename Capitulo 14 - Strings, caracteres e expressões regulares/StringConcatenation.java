//StringConcatenation.java
//Metodo string concat.

public class StringConcatenation{
    public static void main(String[]args){
        String s1 = "Happy";
        String s2 = "Birthday";

        System.out.printf("S1 = %s%nS2 = %s%n%n",s1,s2);
        System.out.printf("Result of s1.concat(s2) == %s%n",s1.concat(s2));
        System.out.printf("S1 after concatenation = %s%n",s1);
    }
}//Fim da classe StringConcatenation