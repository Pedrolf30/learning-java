//OtherCharMethods.java
//Métodos de instancia da classe Character
public class OtherCharMethods{
    public static void main(String[]args){
        Character c1 = 'A';
        Character c2 = 'a';

        System.out.printf("c1 = %s%nc2 = %s%n%n", c1.charValue(), c2.toString());

        if(c1.equals(c2))
            System.out.println("C1 and C2 are equal");
        else
            System.out.println("C1 and C2 are not equal");
    }//Fim da classe OtherCharMethods
}