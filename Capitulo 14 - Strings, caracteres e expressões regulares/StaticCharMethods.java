//StaticCharMethods.java
//Métodos estáticos Character para testar caracteres e converter entre Upper e Lower case
import java.util.Scanner;

public class StaticCharMethods{
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a character and press Enter");
        String input = sc.next();
        char c = input.charAt(0); //Obtem caractere de entrada

        //Exibe informações do caractere
        System.out.printf("Is defined: %b%n",Character.isDefined(c));
        System.out.printf("Is digit: %b%n",Character.isDigit(c));
        System.out.printf("Is first character in a Java identifier: %b%n",
            Character.isJavaIdentifierStart(c));
        System.out.printf("Is part of a Java identifier: %b%n",
            Character.isJavaIdentifierPart(c));
        System.out.printf("Is letter: %b%n", Character.isLetter(c));
        System.out.printf("Is letter or digit: %b%n", Character.isLetterOrDigit(c));
        System.out.printf("Is lower case: %b%n",Character.isLowerCase(c));
        System.out.printf("Is upper case: %b%n",Character.isUpperCase(c));
        System.out.printf("To upper case: %s%n", Character.toUpperCase(c));
        System.out.printf("To lower case: %s%n", Character.toLowerCase(c));
    }
}//Fim da classe StaticCharMethods