//StringIndexMethods.java
//Metodos de pesquisa de String indexOf e lastIndexOf

public class StringIndexMethods{
    public static void main(String[]args){
        String letters = "abcdefghijklmabcdefghijklm";

        //Testa indexOf para localizar um caractere em uma string
        System.out.printf("'c' is located at index %d%n", letters.indexOf('c'));
        System.out.printf("'a' is located at index %d%n", letters.indexOf('a', 1));
        System.out.printf("'$' is located at index %d%n%n", letters.indexOf('$'));

        //Testa lastIndexOf para localiza um caractere em uma string
        System.out.printf("'c' is located at index %d%n", letters.lastIndexOf('c'));
        System.out.printf("'a' is located at index %d%n", letters.lastIndexOf('a', 1));
        System.out.printf("'$' is located at index %d%n%n", letters.lastIndexOf('$'));

        //Testa indexOf para localizar uma substring em uma string
        System.out.printf("\"def\" is locate at index %d%n",letters.lastIndexOf("def"));
        System.out.printf("\"def\" is locate at index %d%n",letters.lastIndexOf("def",25));
        System.out.printf("\"hello\" is locate at index %d%n",letters.lastIndexOf("hello"));
    }
}//Fim da classe StringIndexMethods