//StringMiscellaneous2.java
//Metodos String replace, toLowerCase, to UpperCase, trim e toCharArray

public class StringMiscellaneous2{
    public static void main(String[]args){
        String s1 = "hello";
        String s2 = "GOODBYE";
        String s3 = "  spaces  ";

        System.out.printf("S1 = %s%nS2 = %s%nS3 = %s%n%n",s1,s2,s3);

        //testa o metodo replace
        System.out.printf("Replace 'l' with 'L' in S1: %s%n%n",s1.replace('l','L'));

        //Testa o toLowerCase e o toUpperCase
        System.out.printf("s1.toUpperCase() = %s%n",s1.toUpperCase());
        System.out.printf("s2.toLowerCase() = %s%n%n",s2.toLowerCase());

        //Testa o metodo trim
        System.out.printf("S3 after trim = \"%s\"%n%n",s3.trim());

        //Testa o metodo toCharArray
        char[] charArray = s1.toCharArray();
        System.out.printf("S1 as a character array: ");
        for(char character : charArray)
            System.out.print(character);
        System.out.println();

    }
}//Fim da classe StringMiscellaneous2