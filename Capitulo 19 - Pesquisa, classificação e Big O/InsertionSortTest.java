//InsertionSortTest.java
//Classificando um array com a classificacao por insercao
import java.security.SecureRandom;
import java.util.Arrays;

public class InsertionSortTest{
    //Classifica o array utilizando a classificacao por insercao
    public static void insertionSort(int[] data){
        //Faz um loop sobre data.length -1 elementos
        for(int next =1; next<data.length;next++){
            int insert = data[next]; //Valor a inserir
            int moveItem = next;//Local para inserir o elemento

            //Procura o local para colocar o elemento atual
            while(moveItem > 0 && data[moveItem -1] > insert){
                //Descola o elemento direito um slot
                data[moveItem] = data[moveItem -1];
                moveItem--;
            }
            data[moveItem]=insert; //Local do elemento inserido
            printPass(data,next,moveItem); //Passagem de saida do algoritmo
        }
    }
    //Imprime uma passagem do algoritmo
    public static void printPass(int[] data, int pass, int index){
        System.out.printf("After pass %d: ",pass);

        //Gera saida dos elementos ate o item trocado
        for(int i =0;i <index;i++)
            System.out.printf("%d ", data[i]);

        System.out.printf("%d* ", data[index]); //Indica troca

        //Termina de gerar a saida do array
        for(int i = index +1; i < data.length; i++)
            System.out.printf("%d ", data[i]);

        System.out.printf("%n              "); //Para alinhamento

        //Indica quantidade do arrau que e classificado
        for(int i = 0; i <= pass;i++)
            System.out.printf("-- ");
        System.out.println();
    }
    public static void main(String[]args){
        SecureRandom generator = new SecureRandom();
        int[] data = new int[10]; //Cria o array

        for(int i=0;i< data.length;i++) //Preenche o array
            data[i] = 10 + generator.nextInt(90);

        System.out.printf("Unsorted array: %n%s%n%n",
            Arrays.toString(data)); //Exibe o array
        insertionSort(data); //Classifica o array

        System.out.printf("Sorted array: %n%s%n%n",
            Arrays.toString(data)); //Exibe o array
    }
}//Fim da classe InsertionSortTest