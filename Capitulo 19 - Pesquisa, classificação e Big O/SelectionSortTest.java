//SelectionSortTest.java
//Classificando um array com classificacao por selecao
import java.security.SecureRandom;
import java.util.Arrays;

public class SelectionSortTest{
    //Classifica o array utilizando a classificacao por selecao
    public static void selectionSort(int[] data){
        //Faz um loop sobre data.length -1 elementos
        for(int i=0;i<data.length -1;i++){
            int smallest = i; //Primeiro indicie o array remanescente

            //Faz um loop para localizar o indice do menor elemento
            for(int index = i +1;index<data.length;index++)
                if(data[index] < data[smallest])
                    smallest = index;

            swap(data,i,smallest); //Troca o emnor elemento na posicao
            printPass(data,i + 1, smallest); //Passagem de saido do algoritmo
        }
    }//Finaliza o metodo SelectionSort

    //Metodo auxiliar para trocar valores em dois elementos
    private static void swap(int[] data, int first, int second){
        int temporary = data[first]; //Armazena o primeiro em temporario
        data[first] = data[second]; //Substitui o primeiro pelo segundo
        data[second] = temporary; //Coloca o temporario no segundo
    }
    //Imprime uma passagem do algoritmo
    private static void printPass(int[] data, int pass, int index){
        System.out.printf("after pass %2d: ", pass);

        //Saida de elementos ate item selecionado
        for (int i = 0; i < index; i++)
            System.out.printf("%d ", data[i]);
        
        System.out.printf("%d* ", data[index]); //Indica troca

        //Termina de gerar a saida do array
        for (int i = index + 1; i < data.length; i++)
            System.out.printf("%d ", data[i]);

        System.out.printf("%n               "); // para alinhamento

        //Indica quantidade do arrau que e classificado
        for (int j = 0; j < pass; j++)
            System.out.print("-- ");
        System.out.println();
    }
    public static void main(String[]args){
        SecureRandom generator = new SecureRandom();

        int[] data = new int[10]; //Cria o array

        for(int i=0;i<data.length;i++) //Preenche o array
            data[i] = 10 + generator.nextInt(90);

        System.out.printf("Unsorted array: %n%s%n%n",
            Arrays.toString(data)); //Exibe o array
        selectionSort(data); //Classifica o array

        System.out.printf("Sorted array: %n%s%n%n",
            Arrays.toString(data)); //Exibe o array
    }
}//SelectionSortTest