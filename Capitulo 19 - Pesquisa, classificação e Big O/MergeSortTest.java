//MergeSortTest.java
//Classificando um array com a classificacao por intercalacao
import java.security.SecureRandom;
import java.util.Arrays;

public class MergeSortTest{
    //Chama o metodo split recursivo para iniciar a classificacao por intercalacao
    public static void mergeSort(int[] data){
        sortArray(data,0,data.length -1); //Classifica todo o array
    }//Fim do metodo sort

    //Divide o array, classifica subarrays e intefcala subarrays no array classificado
    private static void sortArray(int[] data, int low, int high){
        //Caso basico de teste: tamanho do array e igual a 1
        if((high - low) >= 1){ //Se nao for o caso basico
            int middle1 = (low + high)/2; //Calcula o meio do array
            int middle2 = middle1 +1; //Calcula o proximo elemento

            //Gera uma saida do passo de divisao
            System.out.printf("Split: %s%n",
                subarrayString(data,low,high));
            System.out.printf("        %sn",
                subarrayString(data,low,middle1));
            System.out.printf("        %s%n%n",
                subarrayString(data,middle2,high));

            //Divide o array pela metade
            //Classifica cada metade (chamadas recursivas)
            sortArray(data,low,middle1); //Primeira metade do array
            sortArray(data,middle2,high); //Segunda metade do array

            //Intercala dois arrays classificados depois que as chamadas de divisao retornam
            merge (data, low, middle1, middle2, high);
        }//Fim do if
    }//Fim do metodo sortArray

    //Intercala dois subarrays classificados em um subarray classificado
    private static void merge(int[] data,int left,int middle1,int middle2, int right){
        int leftIndex = left; //Indice no subarray esquerdo
        int rightIndex = middle2; //Indice no subarray direito
        int combinedIndex = left; //Indice no array temporario funcional
        int[] combined = new int[data.length]; //Array de trabalho

        //Gera saida de dois subarrays antes de mesclar
        System.out.printf("Merge: %s%n",
            subarrayString(data,left,middle1));
        System.out.printf("       %s%n",
            subarrayString(data, middle2,right));
        
        //Intercala arrays ate alcancar o final de um deles
        while(leftIndex <= middle1 && rightIndex <= right){
            //Coloca o kenor dos dois elementos atuais no resultado
            //e o movo para o proximo espaco nos arrays
            if(data[leftIndex]<= data[rightIndex])
                combined[combinedIndex++] = data[leftIndex++];
            else 
                combined[combinedIndex++] = data[rightIndex++];
        }

        //Se o array estiver vazio
        if(leftIndex == middle2)
            //Copia o restando do array direito
            while(rightIndex <= right)
                combined[combinedIndex++] = data[rightIndex++];
        else //O array direito esta vazio
            //Copia o restante do array esquerdo
            while(leftIndex <= middle1)
                combined[combinedIndex++] = data[leftIndex++];
            
        //Copia os valores de volta ao array original
        for(int i =left; i <=right;i++)
            data[i] = combined[i];

        //Gera saida do array intercalado
        System.out.printf("      %s%n%n",
            subarrayString(data,left,right));
    }//Fim do metodo merge

    //Metodo para gerar saida de certos valores no array
    private static String subarrayString(int[] data, int low, int high){
        StringBuilder temporary = new StringBuilder();

        //Gera espacos para alinhamento
        for(int i=0; i <low;i++)
            temporary.append("  ");

        //Gera a saida dos elementos que permanecem no array
        for(int i = low; i<=high;i++)
            temporary.append(" "+ data[i]);

        return temporary.toString();
    }

    public static void main(String[]args){
        SecureRandom generator = new SecureRandom();

        int[] data = new int[10]; //Cria o array

        for(int i=0;i <data.length;i++) //Preenche o array
            data[i] = 10 + generator.nextInt(90);

        System.out.printf("Unsorted array: %n%s%n%n",
            Arrays.toString(data)); //Exibe o array
        mergeSort(data); //Classifica o array

        System.out.printf("Sorted array: %n%s%n%n",
            Arrays.toString(data)); //Exibe o array
    }
}//Final da classe MergeSortTest