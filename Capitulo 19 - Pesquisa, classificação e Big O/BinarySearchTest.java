//BinarySearchTest.java
//Utiliza a pesquisa binaria para localizar um item em um array
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Scanner;

public class BinarySearchTest{
    //Realiza uma pesquisa binaria sobre os dados
    public static int binarySearch(int[] data, int key){
        int low = 0; //Extremidade baixa da area de pesquisa
        int high = data.length - 1; //Extemidade alta da area de pesquisa
        int middle = (low+high+1)/2;//Elemento do meio
        int location = -1;//Valor de torno;-q se nao localizado

        do{ //Faz loop para procurar o elemento
            //Imprime os elementos remanescentes do array
            System.out.print(remainingElements(data,low,high));

            //Gera espaços para alinhamente
            for(int i=0;i< middle;i++)
                System.out.print("   ");
            
            System.out.println(" * "); //Indica o meio atual

            //Se o elemento for localizado no meio
            if(key ==data[middle])
                location = middle; //Localização atual é o meio
            else if(key< data[middle]) //Elemento do meio é muito alto
                high = middle +1; //Elimina a metada mais alta
            else //Elemento do meio é muito baixo
                low =middle +1; //Elimina a metade mais alta

            middle = (low+high+1)/2; //Recalcula o meio
        }while((low <= high) && (location == -1));

        return location; //Retorna a localização da chave de pesquisa
    }//Fim do metodo binarySearch

    //Método parar gerar saida de certos valores no array
    private static String remainingElements(int[] data, int low, int high){
        StringBuilder temporary = new StringBuilder();

        //Acrescenta espaços para alinhamento
        for(int i=0;i<low;i++)
            temporary.append("  ");

        //Gera a said dos elementos que permanecem no array
        for(int i = low;i <=high;i++)
            temporary.append(data[i] + " ");

        return String.format("%s%n", temporary);
    }//Fim do método remainingElements

    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        SecureRandom gen = new SecureRandom();

        int[] data = new int[15]; //Cria o array

        for(int i=0;i<data.length;i++) //Preenche o array
            data[i] = 10 + gen.nextInt(90);

        Arrays.sort(data); //binarySearch requer array classificado
        System.out.printf("%s%n%n", Arrays.toString(data)); //Exibe o array

        //Obtem a entrada do usuario
        System.out.print("Please enter and integer value (-1 to quit): ");
        int searchInt = sc.nextInt();

        //Insere repetidamente um inteiro; -1 termina o programa
        while(searchInt != -1){
            //Realiza a pesquisa
            int location = binarySearch(data, searchInt);

            if(location == -1) //Não encontrado
                System.out.printf("%d was not found",searchInt);
            else //Encontrado
                System.out.printf("%d was found in position %d%n%n",
                    searchInt, location);

            //Obtem a entrada do usuario
            System.out.print("Please enter an integer value (-1 to quit): ");
            searchInt = sc.nextInt();
        }
    }//Fim da classe BinarySearchTest
}