//LinearSearchTest.java
//Pesquisando sequencialmente um item em um array
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Scanner;

public class LinearSearchTest{
    //Realiza uma pesquisa linear nos dados
    public static int linearSearch(int data[], int searchKey){
        //Faz um loop pelo array sequencialmente
        for(int i =0;i<data.length;i++)
            if(data[i] == searchKey)
                return i; //Retorna o indice de inteiros
            
        return -1; //Inteiro nao foi localizado
    }//Fim do metodo linearSearch
    public static void main(String[]args){
        Scanner sc = new Scanner(System.in);
        SecureRandom random = new SecureRandom();

        int[] data = new int[10]; //Cria o array

        for(int i = 0; i< data.length;i++)//Preenche o array
            data[i] = 10 + random.nextInt(90);
        
        System.out.printf("%s%n%n", Arrays.toString(data)); //Exibe o array

        //Obtem a entrada do usuario
        System.out.print("Please enter an integer value (-1 to quit): ");
        int searchInt = sc.nextInt();

        //Insere repetidamente um inteiro; -1 termina o programa
        while(searchInt != -1){
            int position = linearSearch(data, searchInt); //Realiza uma pesquisa

            if(position == -1) //Nao encontrado
                System.out.printf("%d was not found%n%n", searchInt);
            else //Encontrado
                System.out.printf("%d was found in position %d%n%n", searchInt, position);

            //Obtem a entrada de usuario
            System.out.print("Please enter an integer value (-1 to quit): ");
            searchInt = sc.nextInt();
        }
    }//Fim de main
}//Fim da classe LinearSearchTest