//ContinueTest.java
//A instrucao continue que termina uma iteracao de um for
public class ContinueTest{
    public static void main(String[]args){
        for(int count=1;count<=10;count++){//Faz o loop ate 10
            if(count==5)
                continue;//Pula o codigo se a contagem chegar a 5
            System.out.printf("%d%n",count);
        }
        System.out.printf("%nUse 'Continue' to skip printing 5");
    }
}