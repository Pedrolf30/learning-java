//AutoPolicyTest.java
//Demonstrando String em um switch
public class AutoPolicyTest{
    public static void main(String[]args){
        AutoPolicy policy1 = new AutoPolicy(777,"Ford Ka","NJ");
        AutoPolicy policy2 = new AutoPolicy(333,"Porshe Panamera","ME");
        policyInNoFaultState(policy1);//exibe se cada policy esta em um estado sem culpa
        policyInNoFaultState(policy2);
    }
    public static void policyInNoFaultState(AutoPolicy policy){
        System.out.println("The auto policy:");
        System.out.printf("Account #: %d, Car: %s, %s %s a no-fault state %n%n", policy.getAccountNumber(), policy.getMakeAndModel(), policy.getState(),(policy.isNoFaultState()?"is":"is not"));
    }
}//Fim da classe AutoPolicyTest