//LetterGrades.java
//Utiliza switch para contar a letra das notas
import java.util.Scanner;
public class LetterGrades{
    public static void main(String[]args){
        int total = 0;
        int gradeCounter=0;
        int aCount=0,bCount=0,cCount=0,dCount=0,fCount=0;

        Scanner sc = new Scanner(System.in);

        System.out.printf("%s%n %s%n",
        "Enter the integer grades in the range 0-100",
        "Type the end-of-file indicator to terminate input");

        while(sc.hasNext()){
            int grade = sc.nextInt(); //le a nota
            total += grade; //adiciona ota ao total
            gradeCounter++; //incrementa o numero de notas

            switch(grade/10){
                case 9: //nota entre 90
                case 10: // e 100, inclusivo
                    aCount++;
                    break; //sai do switch
                case 8: //nota entre 80-89
                    bCount++;
                    break;
                case 7: //nota entre 70-79
                    cCount++;
                    break;
                case 6: //nota entre 60-69
                    dCount++;
                    break;
                default: //nota menor que 60
                    fCount++;
                    break;
            }//Fim do switch
        }//Fim do while
        System.out.printf("%nGrade Resport%n");
        if (gradeCounter!=0){
            double average = (double) total/gradeCounter; //Calcula a media das notas
            System.out.printf("Total of the %d entered is %d%n", gradeCounter, total);
            System.out.printf("Class average  is %.2f%n: ",average);

            System.out.println("Number os students that received each grade");
            System.out.printf("A: %d%n",aCount);
            System.out.printf("B: %d%n",bCount);
            System.out.printf("C: %d%n",cCount);
            System.out.printf("D: %d%n",dCount);
            System.out.printf("F: %d%n",fCount);

        }//Fim do if
        else{
            System.out.println("No grades were entered");
        }
    }//Fim do main
}//Fim da classe LetterGrades