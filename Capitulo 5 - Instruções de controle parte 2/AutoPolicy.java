//AutoPolicy.java
//Clase que representa uma apolice de seguro de automovel
public class AutoPolicy{
    private int accoutNumber; //numero da conta de apolice
    private String makeAndModel; //carro ao qual a apolice e aplicada
    private String state; //Sigla do estado
    public AutoPolicy(int accoutNumber, String makeAndModel, String state){
        this.accoutNumber=accoutNumber;
        this.makeAndModel=makeAndModel;
        this.state=state;
    }
    public void setAccountNumber(int accoutNumber){
        this.accoutNumber=accoutNumber;
    }
    public int getAccountNumber(){
        return accoutNumber;
    }
    public void setMakeAndModel(String makeAndModel){
        this.makeAndModel=makeAndModel;
    }
    public String getMakeAndModel(){
        return makeAndModel;
    }
    public void setState(String state){
        this.state=state;
    }
    public String getState(){
        return state;
    }
    public boolean isNoFaultState(){ //metodo predicado eh retornado se o estado tem seguro
        boolean noFaultState;
        switch(getState()){ //pega sigla do estado
            case "MA": case "NY": case "NJ": case "PA":
                noFaultState = true;
                break;
            default:
            noFaultState = false;
            break;
        }
        return noFaultState;
    }
}//Fim da classe AutoPolicy