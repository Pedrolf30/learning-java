//DoWhile.java
//Instrução de retição do...while
public class DoWhile{
    public static void main(String[]args){
        int counter = 1;
        do{
            System.out.printf("%d   ",counter);
            ++counter;
        }while(counter<=10); //fim da instrução do..while
    }
}//Fim da classe DoWhile