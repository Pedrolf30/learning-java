//ShapesTest.java
//Obtendo a entrada do usuario, cria um JFrame para exibir as formas
import javax.swing.JFrame; import javax.swing.JOptionPane;
public class ShapesTest{
    public static void main(String[]args){
        String input = JOptionPane.showInputDialog("Enter 1 to draw rectangles\n" + "Enter 2 to draw ovals");//Entrada do usuario
        int choice = Integer.parseInt(input); //Converte entrada em int
        Shapes panel = new Shapes(choice);
        JFrame applications = new JFrame(); //Cria novo JFrame
        applications.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        applications.add(panel);
        applications.setSize(300,300);
        applications.setVisible(true);
    }
}//Fim da classe ShapesTest