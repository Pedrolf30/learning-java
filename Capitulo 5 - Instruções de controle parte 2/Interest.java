//Interest.java
//Calculos de juros composto usando for
public class Interest{
    public static void main(String[]args){
        double amount;//quantia de deposito no final de cada ano
        double principal = 1000.0;//quantia inicial de dinheiro
        double rate = 0.05;//taxa de juros

        System.out.printf("%s%20s%n","Year", "Amount on deposit");

        for(int year=1;year<=10;year++){
            amount=principal*Math.pow(1.0+rate, year);
            System.out.printf("%4d%,20.2f%n", year, amount);
        }
    }
}