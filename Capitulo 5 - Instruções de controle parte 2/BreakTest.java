//BreakTest.java
//A instrucao break sai de uma instrucao for
public class BreakTest{
    public static void main(String[]args){
        int count; //Variavel de controle
        for(count=1;count<=10;count++){ //Faz o loop ate 10
            if(count==5)
                break; //Termina o loop se a contagem chegar a 5
            System.out.printf("%d%n", count);
        }
        System.out.printf("%nBroke out of loop at count = %d%n", count);
    }
}//Fim da classe BreakTest