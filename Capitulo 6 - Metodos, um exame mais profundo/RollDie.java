//RollDie.java
/*Serve para mostrar que os numeros gerados tem probabilidade aproximadamente igual, no exemplo e lancado 6 mil vezes o dado e exibido
frequencia de cada numero*/
import java.security.SecureRandom;

public class RollDie{
    public static void main(String[]args){
        SecureRandom randomNumbers = new SecureRandom();

        int f1 = 0; //Contagem de 1s lancados
        int f2 = 0; //Contagem de 2s lancados
        int f3 = 0; //Contagem de 3s lancados
        int f4 = 0; //Contagem de 4s lancados
        int f5 = 0; //Contagem de 5s lancados
        int f6 = 0; //Contagem de 6s lancados

        for(int roll = 1;roll<=6000;roll++){ //Faz o loop 6 mil vezes e contabiliza a frequencia de cada numero
            int face = 1 + randomNumbers.nextInt(6);

            switch(face){ //Determina a frequencia que cada um dos numeros e aleatoriamente gerado
                case 1: ++f1;break;
                case 2: ++f2;break;
                case 3: ++f3;break;
                case 4: ++f4;break;
                case 5: ++f5;break;
                case 6: ++f6;break;
                
            }
        }
        System.out.println("Face\tFrequency"); //Cabecalhos de saida
        System.out.printf("1\t%d%n2\t%d%n3\t%d%n4\t%d%n5\t%d%n6\t%d%n",f1,f2,f3,f4,f5,f6);
    }
}//Fim da classe RollDie