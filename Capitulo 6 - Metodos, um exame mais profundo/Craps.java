//Craps.java
//Simula o jogo de azar craps
import java.security.SecureRandom;
public class Craps{
    private static final SecureRandom randomNumbers = new SecureRandom();

    private enum Status{CONTINUE,WON,LOST,runfa}; //Tipo enum com constantes que representam o estado do jogo

    //Constantes que representam lançamentos comuns dos dados
    private static final int SNAKE_EYES = 2;
    private static final int TREY = 3;
    private static final int SEVEN = 7;
    private static final int YO_LEVEN = 11;
    private static final int BOX_CARS = 12;

    public static void main(String[]args){ //Joga uma partida de craps
        int myPoint = 0; //Pontuação inicial do jogador
        Status gameStatus; //Pode conter CONTINUE,WON e LOST
        int sumOfDice = rollDice(); //Primeira rolagem dos dados

        switch(sumOfDice){ //Detemina o estado do jogo e a pontuação com base no primeiro lançamento
            case SEVEN: //Ganha com 7 pontos no primeiro lançamento
            case YO_LEVEN: //Ganha com 11 pontos no primeiro lançamento
                gameStatus = Status.WON;
                break;
            case SNAKE_EYES: //Perde com 2 pontos no primeiro lançamento
            case TREY: //Perde com 3 pontos no primeiro lançamento
            case BOX_CARS: //Perde com 12 pontos no primeiro lançamento
                gameStatus = Status.LOST; break;
            default: //Não ganhou nem perdeu, portanto é registrada a pontuação
                myPoint = sumOfDice; //Informa a pontuação
                System.out.printf("Player's points is %d%n",myPoint);
                break;
        }        
        
        //Enquanto o jogo não estiver terminado
        
        while(gameStatus == Status.CONTINUE){ //nem WON nem LOST
            sumOfDice = rollDice(); //Lança os dados novamente
            
            if(sumOfDice == myPoint) //Vitória por pontuação
                gameStatus = Status.WON;
            else if(sumOfDice == SEVEN){ //Perde obtendo 7 antes de atingir a pontuação
                gameStatus = Status.LOST;
            }
        }
        //Exibe mensagem se ganhou ou perdeu
        if(gameStatus == Status.WON)
            System.out.println("You WON");
        else
            System.out.println("You LOST");
    }
    //Lança dos dados calcula a soma e exibe os resultados
    public static int rollDice(){
        //Seleciona valores aleatorios do dado
        int die1 = 1 + randomNumbers.nextInt(6);//Primeiro lançamento
        int die2 = 1 + randomNumbers.nextInt(6);//Segundo lançamento
        int sum = die1 + die2; //Soma dos valores dos dados

        //Exibe os resultados desse lançamento
        System.out.printf("Player rolled %d + %d = %d%n",die1,die2,sum);
        return sum;
    }
}//Fim da classe Craps