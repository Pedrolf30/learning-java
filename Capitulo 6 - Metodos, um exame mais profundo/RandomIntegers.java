//RandomIntegers.java
//Lanca um dado de seis lados 20 vezes
import java.security.SecureRandom; //O programa usa essa classe para gerar numeros randomicos seguros
 public class RandomIntegers{
     public static void main(String[]args){
         SecureRandom randomNumbers = new SecureRandom();

         for(int x = 1; x <= 20 ; x++){ //Faz o loop 20 vezes
            int face = 1 + randomNumbers.nextInt(6); //Seleciona um inteiro aleatorio entre 1 e 6
            System.out.printf("%d ", face); //Exibe numero gerado
            if(x %5 == 0) //Se o contador for divisivel por 5, inicia uma nova linha de saida
                System.out.println();
         }
     }
 }//Fim da classe RandomIntegers