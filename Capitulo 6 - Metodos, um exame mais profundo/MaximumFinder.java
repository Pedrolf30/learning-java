//MaximumFinder.java
//Metodo maximum declarado pelo programador com tres parametros double;
import java.util.Scanner;
public class MaximumFinder{
    public static void main(String[]args){//Obtem tres valores de ponto flutuante e localiza o valor maximo
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter three floating-point values separated by spaces");
        double n1 = sc.nextDouble();//Le o primeiro numero
        double n2 = sc.nextDouble();//Le o segundo numero
        double n3 = sc.nextDouble();//Le o terceiro numero

        double result = maximum(n1,n2,n3);//Determina o valor maximo

        System.out.println("Maximum is: "+ result);
    }
    public static double maximum(double x, double y, double z){ //Retorna o maximo dos seus tres parametros de double
        double maximumValue = x;//Super que X e o maior numero

        if(y>maximumValue) //Determina se Y e o maior numero
            maximumValue = y;
        if(z>maximumValue) //Determina se Z e o maior numero
            maximumValue = z;

        return maximumValue;
    }
}//Fim da classe MaximumFinder