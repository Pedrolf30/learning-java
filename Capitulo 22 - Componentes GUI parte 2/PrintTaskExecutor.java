//PrintTaskExecutor.java
//Usando um Executor Service para executar Runnables
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class PrintTaskExecutor{
    public static void main(String[]args){
        //Cria e nomeia cada executavel
        PrintTask task1 = new PrintTask("task1");
        PrintTask task2 = new PrintTask("task2");
        PrintTask task3 = new PrintTask("task3");

        System.out.println("Starting Executor");

        //Cria ExecutorService para gerenciar threads
        ExecutorService executorService = Executors.newCachedThreadPool();

        //Inicia as tres PrintTasks
        executorService.execute(task1); //Inicia task1
        executorService.execute(task2); //Inicia task2
        executorService.execute(task3); //Inicia task3

        //Fecha ExecutorService -- ele decide quando fechar threads
        executorService.shutdown();

        System.out.printf("Tasks started, main ends.%n%n");

    }
}//Fim da classe PrintTaskExecutor