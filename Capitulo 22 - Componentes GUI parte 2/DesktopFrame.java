//DesktopFrame.java
//Demonstrando JDesktopPane
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JDesktopPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import java.security.SecureRandom;

public class DesktopFrame extends JFrame{
    private final JDesktopPane theDesktop;

    //Configura a GUI
    public DesktopFrame(){
        super("Using a JDesktopPane");

        JMenuBar bar = new JMenuBar();
        JMenu addMenu = new JMenu("Add");
        JMenuItem newFrame = new JMenuItem("Internal Frame");

        addMenu.add(newFrame); //Adiciona um novo item de quadro ao menu add
        bar.add(addMenu); //Adiciona o menu Add a barra de menus
        setJMenuBar(bar); //Configura a barra de menus para esse aplicativo

        theDesktop = new JDesktopPane();
        add(theDesktop); //Adiciona painel de area de trabalho ao quadro

        //Configura o ouvinte para i item de menu newFrame
        newFrame.addActionListener(
            new ActionListener(){ //Classe interna anonima
                //Exibe a nova janela interna
                @Override
                public void actionPerformed(ActionEvent event){
                    //Cria o quadro interno
                    JInternalFrame frame = new JInternalFrame(
                        "Internal Frame", true,true,true,true);
                    
                    MyJPanel panel = new MyJPanel();
                    frame.add(panel, BorderLayout.CENTER);
                    frame.pack(); //Configura o quadro interno de acordo com o tamanho do conteudo

                    theDesktop.add(frame); //Anexa o quadro interno
                    frame.setVisible(true); //Mostra o quadro interno
                }
            }
        );
    }//Fim do construtor DesktopFrame
}//Fim da classe DesktopFrame

//Classe para exibir um ImageIcon em um painel
class MyJPanel extends JPanel{
    private static final SecureRandom generator = new SecureRandom();
    private final ImageIcon picture; //Imagem a ser exibida
    private final static String[] images = {"yellowflowers.png","purpleflowers.png",
        "redflowers.png", "redflowers2.png","lavanderflowers.png"};
    
    //Carrega a imagem
    public MyJPanel(){
        int randomNumber = generator.nextInt(images.length);
        picture = new ImageIcon(images[randomNumber]); //Configura o icone
    }

    //Exibe ImageIcon no painel
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        picture.paintIcon(this,g,0,0); //Exibe o icone
    }

    //Retorna as dimensoes da imagem
    public Dimension getPreferredSize(){
        return new Dimension(picture.getIconWidth(),
            picture.getIconHeight());
    }
}//Fim da classe MyJPane;