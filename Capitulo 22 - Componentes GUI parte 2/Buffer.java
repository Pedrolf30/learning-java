//Buffer.java
//Interface Buffer especifica metodos chamados por Producer e Consumer
public interface Buffer{
    //Coloca o valor int no Buffer
    public void blockingPut(int value) throws InterruptedException;

    //Retorna o valor int a partir do Buffer
    public int blockingGet() throws InterruptedException;
}//Fim da interface Buffer