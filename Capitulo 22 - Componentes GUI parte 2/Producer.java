//Producer.java
//Produtor com um metodo run que insere os valor de 1 a 10 em buffer
import java.security.SecureRandom;

public class Producer implements Runnable{
    private static final SecureRandom generator = new SecureRandom();
    private final Buffer sharedLocation; //Referencia a objeto compartilhado

    //Construtor
    public Producer(Buffer sharedLocation){
        this.sharedLocation=sharedLocation;
    }

    //Armazena os valores de 1 a 10 em sharedLocation
    public void run(){
        int sum =0;
        for(int c=1;c<=10;c++){
            try{
                //Dorme de 0 a 3 segundos, entao coloca valor em Buffer
                Thread.sleep(generator.nextInt(3000)); //Sono aleatorio
                sharedLocation.blockingPut(c); //Configura valor no Buffer
                sum+=c; //Incrementa soma dos valores
                System.out.printf("\t%2d%n",sum);
            }
            catch(InterruptedException exception){
                Thread.currentoThread().interrupt();
            }
        }
        System.out.printf("Producer done producing%nTerminating Producer%n");
    }
}//Fim da classe Producer