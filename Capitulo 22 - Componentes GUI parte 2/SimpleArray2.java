//SimpleArray2.java
//Classe que gerencia um array de inteiros a ser compartilhado por multiplas
//threads com sincronizacao
import java.security.SecureRandom;
import java.util.Arrays;

public class SimpleArray2{
    private static final SecureRandom generator = new SecureRandom();
    private final int[] array; //Array de inteiros compartilhados
    private int writeIndex = 0; //Indice do proximo elemento a ser gravado

    //Cria um SimpleArray de um determinado tamanho
    public SimpleArray2(int size){
        array = new int[size];
    }

    //Adiciona um valor ao array compartilhado
    public synchronized void add(int value){
        int position = writeIndex; //Armazena o indice de gravacao

        try{
            //Em aplicativos reais, nao se deve dormir enquanto se mantem um bloqueio
            Thread.sleep(generator.nextInt(500)); //Apenas para demonstracao
        }
        catch(InterruptedException ex){
            Thread.currentThread().interrupt();
        }

        //Coloca o valor no elemento apropriado
        array[position] = value;
        System.out.printf("%s wrote %2d to element %d.%n",
            Thread.currentThread().getName(),value,position);

        ++writeIndex; //Incrementa indice de elemento a ser gravado depois
        System.out.printf("Next write index: %d%n",writeIndex);
    }
    //Usando para gerar o conteudo do array de inteiros compartilhado
    public synchronized String toString(){
        return Arrays.toString(array);
    }
}//Fim da classe SimpleArray2