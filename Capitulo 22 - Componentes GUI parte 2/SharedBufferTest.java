//SharedBufferTest.java
//Aplicativo com duas threads que manipulam um buffer nao sincronizado
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SharedBufferTest{
    public static void main(String[]args) throws InrerruptedException{
        //Cria nova pool de threads com duas threads
        ExecutorService executorService = Executors.newCachedThreadPool();

        //Cria buffer nao sincronizado para armazenar ints
        Buffer sharedLocation = new UnsynchronizedBuffer();

        System.out.println("Action\t\tValue\tSum");
        System.out.printf("------\t\t-----\t----------------\t---------------%n%n");

        //Executar producer e Consumer, dando a cada um acesso a sharedLocation
        executorService.execute(new Producer(sharedLocation));
        executorService.execute(new Consumer(sharedLocation));

        executorService.shutdown(); //Termina o aplicativo quando as tarefas concluem
        executorService.awaitTermination(1, TimeUnit.MINUTES);
    }
}//Fim da classe SharedBufferTest