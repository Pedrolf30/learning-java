//BoxLayoutFrame.java
//Demonstrando BoxLayout
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class BoxLayoutFrame extends JFrame{
    //Configura a GUI
    public BoxLayoutFrame(){
        super("Demonstrating Box com BoxLayout");

        //Cria containers Box com BoxLayout
        Box horizontal1 = Box.createHorizontalBox();
        Box vertical1 = Box.createVerticalBox();
        Box horizontal2 = Box.createHorizontalBox();
        Box vertical2 = Box.createVerticalBox();

        final int SIZE = 3; //Numero de botoes em cada box

        //Adiciona botoes a Box horizontal 1
        for(int c =0;c<SIZE;c++)
            horizontal1.add(new JButton("Button "+c));

        //Cria um sporte e adiciona botoes a Box vertical1
        for(int c=0;c<SIZE;c++){
            vertical1.add(Box.createVerticalStrut(25));
            vertical1.add(new JButton("Button "+c));
        }

        //Cria a cola horizontal e adiciona botoes a Box horizontal 2
        for(int c=0;c<SIZE;c++){
            horizontal2.add(Box.createHorizontalGlue());
            horizontal2.add(new JButton("Button "+c));
        }

        //Cria uma area rigida e adiciona botoes a Box vertical 2
        for(int c=0;c<SIZE;c++){
            vertical2.add(Box.createRigidArea(new Dimension(12,8)));
            vertical2.add(new JButton("Button "+c));
        }

        //Cria um JTabbedPane
        JTabbedPane tabs = new JTabbedPane(
            JTabbedPane.TOP,JTabbedPane.SCROLL_TAB_LAYOUT);

        //Coloca cada conteiner no painel com guias
        tabs.addTab("Horizontal box", horizontal1);
        tabs.addTab("Vertical Box with Struts",vertical1);
        tabs.addTab("Horizontal Box with Glue", horizontal2);
        tabs.addTab("Vertical with Rigid Areas", vertical2);
        

        add(tabs); //Coloca o painel com guias no frame
    }//Fim do construto BoxLayoutFrame
}//Fim da classe BoxLayoutFrame