//PrintTask.java
//Classe PrintTask dorme por um tempo aleatorio de 0 a 5 segundos
import java.security.SecureRandom;

public class PrintTask implements Runnable{
    private static final SecureRandom generator = new SecureRandom();
    private final int sleepTime; //Tempo de adormecimento aleatorio para a thread
    private final String taskName;

    //Construtor
    public PrintTask(String taskName){
        this.taskName=taskName;
        //Seleciona tempo de adormecimento aleatorio entre 0 e 5 segundos
        sleepTime = generator.nextInt(5000); //Milissegundos
    }

    //Metodo run contem o condigo que uma threadh executara
    public void run(){
        try{ //Coloca a thread para dormir pela quantidade de tempo do sleepTime
            System.out.printf("%s going to sleep for %d milliseconds.%n",
                taskName, sleepTime);
            Thread.sleep(sleepTime); //Coloca a thread para dormir
        }
        catch(InterruptedException exception){
            exception.printStackTrace();
            Thread.currentThread().interrupt(); //Reintorrempe a thread
        }

        //Imprime nome da tarefa
        System.out.printf("%s done sleeping%n",taskName);
    }
}//Fim da classe PrintTask