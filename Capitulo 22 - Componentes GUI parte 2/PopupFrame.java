//PopupFrame.java
//Demonstrando JPopupMenus
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.ButtonGroup;

public class PopupFrame extends JFrame{
    private final JRadioButtonMenuItem[] items; //Contem itens para cores
    private final Color[] colorValues = {Color.BLUE,Color.YELLOW,Color.RED}; //Cores a seres utilizadas
    private final JPopupMenu popupMenu; //Permite que o usuario selecione a cor

    //Construtor sem argumento configura a GUI
    public PopupFrame(){
        super("Using JPoupup Menus");
        ItemHandler handler = new ItemHandler(); //Rotina de tratamento para itens de menu
        String[] colors = {"Blue","Yellow","Red"};

        ButtonGroup colorGroup = new ButtonGroup(); //Gerencia itens de cor
        popupMenu = new JPopupMenu(); //Cria menu pop up
        items = new JRadioButtonMenuItem[colors.length];

        //Cria item de menu, adiciona-o ao menu pop up, permite tratamento de eventos
        for(int c=0;c<items.length;c++){
            items[c] = new JRadioButtonMenuItem(colors[c]);
            popupMenu.add(items[c]); //Adiciona o item ao menu pop-up
            colorGroup.add(items[c]); //Adiciona o item ao grupo de botoes
            items[c].addActionListener(handler); //Adiciona handler
        }

        setBackground(Color.WHITE);

        //Declara um mouse listener para a janela a fim de exibir o menu pop-up
        addMouseListener(
            new MouseAdapter(){ //Classe interna anonima
                //Trata eventos de pressionamento de mouse
                @Override
                public void mousePressed(MouseEvent event){
                    checkForTriggerEvent(event);
                }

                //Trata eventos de liberacao de botao de mouse
                @Override
                public void mouseReleased(MouseEvent event){
                    checkForTriggerEvent(event);
                }

                //Determina se o evento deve aciona o menu pop-up
                private void checkForTriggerEvent(MouseEvent event){
                    if(event.isPopupTrigger())
                        popupMenu.show(event.getComponent(),event.getX(),event.getY());
                }
            }
        );
    }//Fim do construtor PopupFrame

    //Classe interna privada para tratar eventos de item de menu
    private class ItemHandler implements ActionListener{
        //Processa selecoes de itens de menu
        @Override
        public void actionPerformed(ActionEvent event){
            //Determina qual item de menu for selecionado
            for(int i=0;i<items.length;i++){
                if(event.getSource() == items[i]){
                    getContentPane().setBackground(colorValues[i]);
                    return;
                }
            }
        }
    }//Fim da classe interna privada ItemHandler
}//Fim da classePopupFrame