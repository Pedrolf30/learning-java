//SliderFrame.java
//Utilizando JSlider para deminsionar um oval
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class SliderFrame extends JFrame{
    private final JSlider diameterJSlider; //Slider para seleciona o diametro
    private final OvalPanel myPanel; //Painel para desenhar um circulo

    //Construtor sem argumento
    public SliderFrame(){
        super("Slider Demo");

        myPanel = new OvalPanel(); //Cria o painel para desenhar um circulo
        myPanel.setBackground(Color.YELLOW);

        //Configura JSlider para controlar o valor do diametro
        diameterJSlider = new JSlider(SwingConstants.HORIZONTAL,0,200,10);
        diameterJSlider.setMajorTickSpacing(10); //Cria uma amrca e medida a cada 10
        diameterJSlider.setPaintTicks(true); //Pinta as marcas de medida no slider

        //Registra o ouvinte de evento do JSlider
        diameterJSlider.addChangeListener(
            new ChangeListener(){ //Classe interna anonima
                //Trata a alteracao de valor do controle deslizante
                @Override
                public void stateChanged(ChangeEvent e){
                    myPanel.setDiameter(diameterJSlider.getValue());
                }
            }
        );

        add(diameterJSlider, BorderLayout.SOUTH);
        add(myPanel, BorderLayout.CENTER);
    }
}//Fim da classe SliderFrame