//LookAndFeelTest.java
//Alterando a aparencia e comportamento
import javax.swing.JFrame;

public class LookAndFeelTest{
    public static void main(String[]args){
        LookAndFeelFrame lookAndFeelFrame = new LookAndFeelFrame();
        lookAndFeelFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        lookAndFeelFrame.setSize(400,220);
        lookAndFeelFrame.setVisible(true);
    }
}//Fim da classe LookAndFeelTest