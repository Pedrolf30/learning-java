//SimpleArray.java
//Classe que gerencia um array de inteiros para ser compartilhado por varias threads
import java.security.SecureRandom;
import java.util.Arrays;

public class SimpleArray{ //NAO SEGURO PARA THREADS
    private static final SecureRandom generator = new SecureRandom();
    private final int[] array; //array de inteiros compartilhado
    private int writeIndex =0; //Indice compartilhado do proximo elemento a gravar

    //Cria um SimpleArray de um determinado tamanho
    public SimpleArray(int size){
        array = new int[size];
    }

    //Adiciona um valor ao array compartilhado
    public void add(int value){
        int position = writeIndex; //Armazena o indice de gravacao

        try{
            //Coloca a thread para dormir durante 0 a 499 milissegundos
            Thread.sleep(generator.nextInt(500));
        }
        catch(InterruptedException ex){
            Thread.currentThread().interrupt();//Reinterrompe a threadh
        }

        //Coloca o valor no elemento apropriado
        array[position] = value;
        System.out.printf("%s wrote %2d to element %d.%n",
            Thread.currentThread().getName(),value,position);

        ++writeIndex; //Indice de incremento de elemento a ser gravado depois
        System.out.printf("Next write index: %d%n",writeIndex);
    }

    //Usado para gerar saida do conteudo do array de inteiros compartilhado
    public String toString(){
        return Arrays.toString(array);
    }
}