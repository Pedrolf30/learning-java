//MenuFrame.java
//Demonstrando menus
import java.awt.Color;
import java.awt.Font;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JFrame;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;

public class MenuFrame extends JFrame{
    private final Color[] colorValues = {Color.BLACK,Color.BLUE,Color.RED,Color.GREEN};
    private final JRadioButtonMenuItem[] colorItems; //Itens do menu color
    private final JRadioButtonMenuItem[] fonts; //Itens do menu Font
    private final JCheckBoxMenuItem[] styleItems; //Itens do menu Font Style
    private final JLabel displayJLabel; //Exibe exemplo de texto
    private final ButtonGroup fontButtonGroup; //Gerencia itens do menu Font
    private final ButtonGroup colorButtonGroup; //Gerencia itens do menu Color
    private int style; //Utilizado para criar estilos de fontes

    //Construtor sem argumento para configurar a GUI
    public MenuFrame(){
        super("Using JMenus");

        JMenu fileMenu = new JMenu("File"); //Cria o menu file
        fileMenu.setMnemonic('F'); //Configura Mnemonico com f

        //Cria item de menu About
        JMenuItem aboutItem = new JMenuItem("About...");
        aboutItem.setMnemonic('A'); //Configura o mnemonico com A
        fileMenu.add(aboutItem); //Adiciona o item about ao menu File
        aboutItem.addActionListener(
            new ActionListener(){ //Classe interna anonima
                //Exibe um dialogo de mensagem quando o usuario seleciona about...
                @Override
                public void actionPerformed(ActionEvent event){
                    JOptionPane.showMessageDialog(MenuFrame.this, 
                        "This is an example\nof using menus",
                        "About",JOptionPane.PLAIN_MESSAGE);
                }
            }
        );

        JMenuItem exitItem = new JMenuItem("Exit"); //Cria o item exit
        exitItem.setMnemonic('x'); //Configura o mnemonico como x
        fileMenu.add(exitItem); //Adiciona o item exit ao menu File
        exitItem.addActionListener(
            new ActionListener(){ //classe interna anonima
                //Termina o aplicativo quando o usuario clicka em exitItem
                @Override
                public void actionPerformed(ActionEvent event){
                    System.exit(0); //Encerra o aplicativo
                }
            }
        );

        JMenuBar bar = new JMenuBar(); //Cria a barra de menus
        setJMenuBar(bar); //Adiciona uma barra de menus ao aplicativo
        bar.add(fileMenu); //Adiciona o menu File a barra de menus

        JMenu formatMenu = new JMenu("Format"); //Cria o menu Format
        formatMenu.setMnemonic('r'); //Configura Mnemonico como r

        //Array listando cores de String
        String[] colors = {"Black","Blue","Red","Green"};

        JMenu colorMenu = new JMenu("Color"); //Cria o menu Color
        colorMenu.setMnemonic('C'); //Configura mnemonico como C

        //Cria os itens de menu de botao de radio pera cores
        colorItems = new JRadioButtonMenuItem[colors.length];
        colorButtonGroup = new ButtonGroup(); //Gerencia cores
        ItemHandler itemHandler = new ItemHandler(); //Rotina de tratamento para cores

        //Cria itens do menu Color com botoes de opcao
        for(int c=0;c<colors.length;c++){
            colorItems[c] = new JRadioButtonMenuItem(colors[c]); //Cria o item
            colorMenu.add(colorItems[c]); //Adiciona item ao menu Color
            colorButtonGroup.add(colorItems[c]); //Adiciona ao grupo
            colorItems[c].addActionListener(itemHandler);
        }

        colorItems[0].setSelected(true); //Seleciona io primeiro item Color

        formatMenu.add(colorMenu); //Adiciona o menu Color ao menu Format
        formatMenu.addSeparator(); //Adiciona um separador no menu

        //Array listando nomes de fonte
        String[] fontNames = {"Serif","Monospaced","SansSerif"};
        JMenu fontMenu = new JMenu("Font"); //Cria a fonto do menu
        fontMenu.setMnemonic('n'); //Configura mnemonico como n

        //Cria itens do menu radio button para nomes de fonte
        fonts = new JRadioButtonMenuItem[fontNames.length];
        fontButtonGroup = new ButtonGroup(); //Gerencia os nomes das fontes

        //Criar itens do menu Font com botoes de opcao
        for(int c=0;c<fonts.length;c++){
            fonts[c] = new JRadioButtonMenuItem(fontNames[c]);
            fontMenu.add(fonts[c]); //Adiciona fonte do menu Font
            fontButtonGroup.add(fonts[c]); //Adiciona ao grupo botoes
            fonts[c].addActionListener(itemHandler); //Adiciona rotina de tratamento
        }

        fonts[0].setSelected(true); //Seleciona o primeiro item do menu Font
        fontMenu.addSeparator(); //Adiciona uma barra separadora ao menu Font

        String[] styleNames = {"Bold","Italic"}; //Nomes dos estilos
        styleItems = new JCheckBoxMenuItem[styleNames.length];
        StyleHandler styleHandler = new StyleHandler(); //Rotina de tratamento de estilo

        //Cria itens do menu Style com caixas de selecao
        for(int c =0;c<styleNames.length;c++){
            styleItems[c] = new JCheckBoxMenuItem(styleNames[c]); //Para estilo
            fontMenu.add(styleItems[c]); //Adiciona ao menu Font
            styleItems[c].addItemListener(styleHandler); //Rotina de tratamento
        }

        formatMenu.add(fontMenu); //Adiciona o menu Font ao menu Format
        bar.add(formatMenu); //Adiciona o menu Format a barra de menus

        //Configura o rotulo para exibir o texto
        displayJLabel = new JLabel("Sample Text", SwingConstants.CENTER);
        displayJLabel.setForeground(colorValues[0]);
        displayJLabel.setFont(new Font("Serif",Font.PLAIN,72));

        getContentPane().setBackground(Color.CYAN); //Configura o fundo
        add(displayJLabel, BorderLayout.CENTER); //Adiciona displayJLabel
    }//Fim do construtor MenuFrame

    //Classe interna para tratar evebtis de acao dos itens de menu
    private class ItemHandler implements ActionListener{
        //Processa selecoes de cor e font
        @Override
        public void actionPerformed(ActionEvent event){
            //Processa a selecao de cor
            for(int c=0;c<colorItems.length;c++){
                if(colorItems[c].isSelected()){
                    displayJLabel.setForeground(colorValues[c]);
                    break;
                }
            }
            //Processa a selecao de fonte
            for(int c=0;c<fonts.length;c++){
                if(event.getSource() == fonts[c]){
                    displayJLabel.setFont(new Font(fonts[c].getText(),style,72));
                }
            }
            repaint(); //Redesenha o aplicativo
        }
    }//Fim da classe ItemHandler

    //Classe interna para tratar eventos de itens dos itens de menu da caixa de verificacao
    private class StyleHandler implements ItemListener{
        //Processa selecoes de estilo da fonte
        @Override
        public void itemStateChanged(ItemEvent e){
            String name = displayJLabel.getFont().getName(); //Fonte atual
            Font font; //Nova fonte com base nas selecoes pelo usuario

            //Determina quais itens estao marcados e cria Font
            if(styleItems[0].isSelected() &&
                styleItems[1].isSelected())
                font =new Font(name,Font.BOLD + Font.ITALIC,72);
            else if(styleItems[0].isSelected())
                font = new Font(name,Font.BOLD,72);
            else if(styleItems[1].isSelected())
                font = new Font(name, Font.ITALIC,72);
            else
                font = new Font(name,Font.PLAIN,72);

            displayJLabel.setFont(font);
            repaint();
        }
    }
}//Fim da classe MenuFrame