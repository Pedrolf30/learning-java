//LookAndFeelFrame.java
//Alterando a aparencia e o comportamento
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.event.ItemListener;
import java.beans.Expression;
import java.awt.event.ItemEvent;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class LookAndFeelFrame extends JFrame{
    private final UIManager.LookAndFeelInfo[] looks;
    private final String[] lookNames; //Nomes da aparencia e do comportamento
    private final JRadioButton[] radio; //Para selecionana aparencia e o comportamento
    private final ButtonGroup group; //Grupo para botoes de radio
    private final JButton button; //Exibe a aparencia do botao
    private final JLabel label; //Exibe a aparencia do rotulo
    private final JComboBox<String> comboBox; //Exibe a aparencia em caixa de combo

    //Configura a GUI
    public LookAndFeelFrame(){
        super("Look and feel demo");

        //Obtem as informacoes sobre a aparencia e o comportamento instaladas
        looks = UIManager.getInstalledLookAndFeels();
        lookNames= new String[looks.length];

        //Obtem os nomes das aparencias e comprotamentos instalados
        for(int i=0;i<looks.length;i++)
            lookNames[i] = looks[i].getName();

        JPanel  northPanel = new JPanel();
        northPanel.setLayout(new GridLayout(3,1,0,5));

        label = new JLabel("This is a "+lookNames[0] + "look-and-feel",
            SwingConstants.CENTER);
        northPanel.add(label);

        button = new JButton("JButton");
        northPanel.add(button);

        comboBox = new JComboBox<String>(lookNames);
        northPanel.add(comboBox);

        //Cria um array para botoes de opcao
        radio = new JRadioButton[looks.length];

        JPanel southPanel = new JPanel();

        //usa um GridLayout com tres botoes em cada linha
        int rows = (int) Math.ceil(radio.length/3.0);
        southPanel.setLayout(new GridLayout(rows,3));

        group = new ButtonGroup(); //Grupo de botoes para aparencia e comportamento
        ItemHandler handler = new ItemHandler(); //Rotina de tratamento de aparencia e comportamento

        for(int c=0;c<radio.length;c++){
            radio[c] = new JRadioButton(lookNames[c]);
            radio[c].addItemListener(handler); //Adiciona rotina de tratamento
            group.add(radio[c]); //Adiciona botao de opcoes ao grupo
            southPanel.add(radio[c]); //Adiciona botoao de opcoes ao painel
        }

        add(northPanel, BorderLayout.NORTH); //Adiciona o painel North
        add(southPanel, BorderLayout.SOUTH); //Adiciona o painel South

        radio[0].setSelected(true); //Configura a selecao padrao
    }//Fim do construtor LookAndFeelFrame

    //Utiliza UIManager para alterar a aparencia e comportamento da GUI
    private void changeTheLookAndFeel(int value){
        try{ //Muda a aparencia e comportamento
            //Configura a aparencia e o comportamento para esse aplicativo
            UIManager.setLookAndFeel(looks[value].getClassName());

            //Atualiza os componentes nesse aplicativo
            SwingUtilities.updateComponentTreeUI(this);
        }
        catch(Exception exception){
            exception.printStackTrace();
        }
    }

    //Classe interna private para tratar de eventos de botao de opcao
    private class ItemHandler implements ItemListener{
        //Processa a selecao de aparencia e comportamento feita pelo usuario
        @Override
        public void itemStateChanged(ItemEvent event){
            for(int c=0;c<radio.length;c++){
                if(radio[c].isSelected()){
                    label.setText(String.format(
                        "This is a %s look-and-feel", lookNames[c]));
                    comboBox.setSelectedIndex(c); //Configura o indice da caixa de combo
                    changeTheLookAndFeel(c); //Muda a aparencia e comportamento
                }
            }
        }
    }//Fim da classe interna privada ItemHandler
}//Fim da classe LookAndFeelFrame