//SimpleArrayWriterTest.java
//Executando dois Runnables para adicionar elementos a um SimpleArray compartilhado
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class SimpleArrayWriterTest{
    public static void main(String[]args){
        //Constroi o objeto compartilhado
        SimpleArray sharedSimpleArray = new SimpleArray(6);

        //Cria duas tarefas a gravas no SimpleArray compartilhado
        SimpleArrayWriter writer1 = new SimpleArrayWriter(1, sharedSimpleArray);
        SimpleArrayWriter writer2 = new SimpleArrayWriter(11, sharedSimpleArray);

        //Executa as tarefas com um ExecutorService
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(writer1);
        executorService.execute(writer2);

        executorService.shutdown();

        try{
            //Espera 1 minuto para que ambos os escritores terminem a execucao
            boolean taskEnded = executorService.awaitTermination(1, TimeUnit.MINUTES);

            if(taskEnded){
                System.out.printf("%nContents of Simple Array:%n");
                System.out.println(sharedSimpleArray); //Imprime o conteudo
            }
            else
                System.out.println("Timed out while waiting for tasks to finish");
        }
        catch(InterruptedException ex){
            ex.printStackTrace();
        }
    }//Fim de main
}//Fim da classe SimpleArrayWriterTest