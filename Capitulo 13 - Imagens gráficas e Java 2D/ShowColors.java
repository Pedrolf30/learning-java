//ShowColors.java
//Escolhendo cores com JColorChooser
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JColorChooser;
import javax.swing.JPanel;

public class ShowColors extends JFrame{
    private final JButton changeColorJButton;
    private Color color = Color.LIGHT_GRAY;
    private final JPanel colorJPanel;

    //Configura a GUI
    public ShowColors(){
        super("Using JColorChooser");

        //Cria JPanel para exibir as cores
        colorJPanel = new JPanel();
        colorJPanel.setBackground(color);

        //Configura changeColorButton e registra sua rotina de tratamento
        changeColorJButton = new JButton("Change color");
        changeColorJButton.addActionListener(
            new ActionListener(){ //Classe interna anonima
                //Exibe JColorChooser quando o usuario clicka no botão
                @Override
                public void actionPerformed(ActionEvent event){
                    color = JColorChooser.showDialog(
                        ShowColors.this,"Choose a color", color);
                    
                    //Configura a cor padrão, se nenhuma cor for retornada
                    if(color==null)
                        color = Color.LIGHT_GRAY;
                    
                    //Muda a cor de fundo do painel de conteudo
                    colorJPanel.setBackground(color);
                }//Fim do método actionPerformed
            }//Fim da classe interna anonima
        );//Fim da chamada para addActionListener
        add(colorJPanel,BorderLayout.CENTER);
        add(changeColorJButton, BorderLayout.SOUTH);

        setSize(400,130);
        setVisible(true);
    }//Fim do construtor ShowColors
}//Fim da classe ShowColors