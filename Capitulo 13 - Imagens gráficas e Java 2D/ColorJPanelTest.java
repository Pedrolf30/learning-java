//ColorJPanelTest
//Demonstrando colors
import javax.swing.JFrame;

public class ColorJPanelTest{
    //Executa o aplicativo
    public static void main(String[]args){
        //Cria o frame para ColorJPanel
        JFrame frame = new JFrame("Using colors");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ColorJPanel cJP = new ColorJPanel();
        frame.add(cJP);
        frame.setSize(400,180);
        frame.setVisible(true);
    }
}//Fim da classe ColorJPanelTest