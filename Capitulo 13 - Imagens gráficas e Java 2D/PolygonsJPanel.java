//PolygonsJPanel.java
//Desenhando poligonos
import java.awt.Graphics;
import java.awt.Polygon;
import javax.swing.JPanel;

public class PolygonsJPanel extends JPanel{
    //Desenha poligonos e polilinhas
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);

        //Desenha o poligono com objeto Polygon
        int [] xValues = {20,40,50,30,20,15};
        int [] yValues = {50,50,60,80,80,60};
        Polygon poly1 = new Polygon(xValues,yValues,6);
        g.drawPolygon(poly1);

        //Desenha polilinhas com dois arrays
        int [] xValues2 = {70,90,100,80,70,65,60};
        int [] yValues2 = {100,100,110,110,130,110,90};
        g.drawPolyline(xValues2,yValues2,7);

        //Preenche o poligono com dois arrays
        int [] xValues3 = {120,140,150,190};
        int [] yValues3 = {40,70,80,60};
        g.fillPolygon(xValues3,yValues3,4);

        //Desenha o poligono preenchido com objeto Polygon
        Polygon poly2 = new Polygon();
        poly2.addPoint(165,135);
        poly2.addPoint(175,150);
        poly2.addPoint(270,200);
        poly2.addPoint(200,220);
        poly2.addPoint(130,180);
        g.fillPolygon(poly2);
    }
}//Fim da classe PolygonsJPanel

