//ArcsJPanelTest
//Desenhando arcos
import javax.swing.JFrame;

public class ArcsJPanelTest{
    //Executa o aplicativo
    public static void main(String[]args){
        //Cria frame para ArcsJPanel
        JFrame frame = new JFrame("Drawing arcs");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ArcsJPanel ajp = new ArcsJPanel();
        frame.add(ajp);
        frame.setSize(300,210);
        frame.setVisible(true);
    }
}//Fim da classe ArcsJPanelTest