//FontJPanelTest
// Utilizando fontes
import javax.swing.JFrame;

public class FontJPanelTest{
    //Executa o aplicativo
    public static void main(String[]args){
        //Cria frame para FontJPanel
        JFrame frame = new JFrame("Using fonts");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        FontJPanel fJP = new FontJPanel();
        frame.add(fJP);
        frame.setSize(420,150);
        frame.setVisible(true);
    }
}//Fim da classe FontJPanelTest