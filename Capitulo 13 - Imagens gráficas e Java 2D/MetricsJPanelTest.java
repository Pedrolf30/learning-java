//MetricsJPanelTest
//Exibindo a metrica da fonte
import javax.swing.JFrame;

public class MetricsJPanelTest{
    //Executa o aplicativo
    public static void main(String[]args){
        //Cria frame para MetricsJPanel
        JFrame frame = new JFrame("Demonstrating FontMetrics");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        MetricsJPanel mJP = new MetricsJPanel();
        frame.add(mJP);
        frame.setSize(510,240);
        frame.setVisible(true);
    }
}//Fim da classe MetricsJPanelTest