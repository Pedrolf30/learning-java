//ShapesJPanelTest.java
//Testando ShapesJPanelTest
import javax.swing.JFrame;

public class ShapesJPanelTest{
    //Executa o aplicativo
    public static void main(String[]args){
        //Cria JFrame para ShapesJPanelTest
        JFrame frame = new JFrame("Drawing 2D shapes");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Cria ShapesJPanel
        ShapesJPanel sjp = new ShapesJPanel();

        frame.add(sjp);
        frame.setSize(425,200);
        frame.setVisible(true);
    }
}//Fim da classe ShapesJPanelTest