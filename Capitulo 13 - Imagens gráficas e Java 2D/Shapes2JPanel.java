//Shapes2JPanel.java
//Demonstrando um caminho geral
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.security.SecureRandom;
import javax.swing.JPanel;

public class Shapes2JPanel extends JPanel{
    //Desenha caminhos gerais
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        SecureRandom random = new SecureRandom();

        int[] xPoints = {55,67,109,73,83,55,27,37,1,43};
        int[] yPoints = {0,36,36,54,96,72,96,54,36,36};

        Graphics2D g2d = (Graphics2D) g;
        GeneralPath star = new GeneralPath();

        //Configura a coordenado inicial do GeneralPath
        star.moveTo(xPoints[0], yPoints[0]);

        //Cria a estrela -- nao desenha
        for(int c = 0; c< xPoints.length;c++)
            star.lineTo(xPoints[c], yPoints[c]);

        star.closePath(); //Fecha a forma

        g2d.translate(150,150); //Traduz a origem para (150,150)

        //Gira em torna da origiem e desenha estrelas em cores aleatorias
        for(int c =1; c<=20;c++){
            g2d.rotate(Math.PI/10.0); //Rotaciona sistema de coordenadas

            //Configura Cores aleatorias
            g2d.setColor(new Color(random.nextInt(256),
                random.nextInt(256), random.nextInt(256)));

            g2d.fill(star);//Desenha estrela preenchida
              
        }
    }
}//Fim da classe Shapes2JPanel
