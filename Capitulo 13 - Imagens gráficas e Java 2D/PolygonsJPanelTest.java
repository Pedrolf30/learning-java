//PolygonsJPanelTest.java
//Desenhando poligonos
import javax.swing.JFrame;

public class PolygonsJPanelTest{
    //Executa o aplicativo
    public static void main(String[]args){
        //Cria frame para PolygonsJPanel
        JFrame frame = new JFrame("Drawin polygons");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        PolygonsJPanel pjp = new PolygonsJPanel();
        frame.add(pjp);
        frame.setSize(280,270);
        frame.setVisible(true);
    }
}//Fim da classe PolygonsJPanelTest