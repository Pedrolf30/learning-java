//Shapes2JPanelTest.java
//Demonstrando um caminho geral
import java.awt.Color;
import javax.swing.JFrame;

public class Shapes2JPanelTest{
    //Executa o aplicativo
    public static void main(String[]args){
        //Cria frame para Shapes2JPanelTest
        JFrame frame = new JFrame("Drawing 2D shapes");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Shapes2JPanel s2jp = new Shapes2JPanel();
        frame.add(s2jp);
        frame.setBackground(Color.WHITE);
        frame.setSize(315,330);
        frame.setVisible(true);
    }
}//Fim da classe Shapes2JPanelTest