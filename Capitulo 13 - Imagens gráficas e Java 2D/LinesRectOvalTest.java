//LinesRectOvalTest.java
//Testando LinesRectOval
import java.awt.Color;
import javax.swing.JFrame;

public class LinesRectOvalTest{
    //Executa o aplicativo
    public static void main(String[]args){
        //Criar frame para LinesRectOval
        JFrame frame = new JFrame("Drawing lines, recs and ovals");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        LinesRectOval lro = new LinesRectOval();
        lro.setBackground(Color.WHITE);
        frame.add(lro);
        frame.setSize(400,210);
        frame.setVisible(true);
    }
}//Fim da classe LinesRectOvalTest