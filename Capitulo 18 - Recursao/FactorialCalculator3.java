//FactorialCalculator.java
//Metodo fatorial iterativo

public class FactorialCalculator{
    //Declaracao recursiva de metodo fatorial
    public long factorial(long number){
        long result = 1;

        //declaracao iterativa de metodo fatorial
        for(long i = number; i>=1;i--)
            result *= i;
        
        return result;
    }
    //Gera saida de fatoriais para valores 0 a 10
    public static void main(String[]args){
        //Calcula o fatorial de 0 a 10
        for(int c = 0; c<=10;c++)
            System.out.printf("%d! = %d%n",c,factorial(c));
    }
}//Fim da classe FactorialCalculator