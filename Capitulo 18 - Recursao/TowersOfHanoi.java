//TowersOfHanoi.java
//Solucao de problema das torres de hanoi com um metodo exclusivo

public class TowersOfHanoi{
    //Move recursivameto os discos entre as torres
    public static void solveTowers(int disks, int sourcePeg, int destinationPeg, int tempPeg){
        //Caso basico -- somente um disco a ser removido
        if(disks ==1){
            System.out.printf("%n%d --> %d", sourcePeg,destinationPeg);
            return;
        }
        //Passo recursivo -- move os discos (disco-1) do sourcePeg
        //Para tempPeg usando destinationPeg
        solveTowers(disks -1, sourcePeg, tempPeg, destinationPeg);

        //Move o ultimo disco de sourcePeg para destinationPeg
        System.out.printf("%n%d --> %d",sourcePeg,destinationPeg);

        //Move (disks -1) discos de tempPeg para destinationPeg
        solveTowers(disks -1, sourcePeg, tempPeg, destinationPeg);
    }
    public static void main(String[]args){
        int startPeg =1; //Valor 1 utilizado para indicar startPeg na saida
        int endPeg =3; //Valor 3 utilizado para indicar endPeg na saida
        int tempPeg =2; //Valor 2 utilizado para indicar tempPeg na saida
        int totalDisks=3; //Numero de discos

        //Chamada nao recursiva inicial: move todos os discos
        solveTowers(totalDisks, startPeg, endPeg, tempPeg);
    }
}//Fim da classe TowersOfHanoi