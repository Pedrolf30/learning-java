//Fractal.java
//Interface com o usuario do fractal
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JColorChooser;

public class Fractal extends JFrame{
    private static final int WIDTH = 400; //Define a largura de GUI
    private static final int HEIGHT = 480; //Defina a altura de GUI
    private static final int MIN_LEVEL = 0;
    private static final int MAX_LEVEL = 15;

    //Configura a GUI
    public Fractal(){
        super("Fractal");

        //Configura levelJLabel para adicionar ao controlJPanel
        final JLabel levelJLabel = new JLabel("Level: 0");

        final FractalJPanel drawSpace = new FractalJPanel(0);

        //Configura o painel de controle
        final JPanel controlJPanel = new JPanel();
        controlJPanel.setLayout(new FlowLayout());

        //Configura o botao de cor e registra o ouvinte
        final JButton changeColorJButton = new JButton("Color");
        controlJPanel.add(changeColorJButton);
        changeColorJButton.addActionListener(
            new ActionListener() //Classe interna anonima
            {
                //Processa o evento changeColorJButton
                @Override
                public void actionPerformed(ActionEvent event){
                    Color color = JColorChooser.showDialog(
                        Fractal.this,"Choose a color", Color.BLUE);

                //Configura a cor padrao, se nenhuma for retornada
                if(color == null)
                    color = Color.BLUE;

                drawSpace.setColor(color);
                }
            }//Fim da classe interna anonima
        );//Fim de addActionListener
        //Configura o botao decrease level para adiciona painel de controle e ouviste registrado
        final JButton decreaseLevelJButton = new JButton("Decrease Level");
        controlJPanel.add(decreaseLevelJButton);
        decreaseLevelJButton.addActionListener(
            new ActionListener(){ //Class interna anonima
                //Processa o evento decreaseLevelJButton
                @Override
                public void actionPerformed(ActionEvent event){
                    int level = drawSpace.getLevel();
                    --level;

                    //Modofica o nivel se possivel
                    if ((level >= MIN_LEVEL) &&
                        (level <= MAX_LEVEL))
                    {
                        levelJLabel.setText("Level: "+ level);
                        drawSpace.setLevel(level);
                        repaint();
                    }
                }    
            }//Fim da classe interna anonima
        ); //Fim de addActionListener

        //Configura o botao increase level para adicionar painel de controle e registra o ouvinte
        final JButton increaseLevelJButton = new JButton("Increase level");
        controlJPanel.add(increaseLevelJButton);
        increaseLevelJButton.addActionListener(
            new ActionListener(){ //Class interna anonima
                //Processa evento increaseLevelJButton
                @Override
                public void actionPerformed(ActionEvent event){
                    int level = drawSpace.getLevel();
                    ++level;

                    //Modifica o nivel se possivel
                    if((level >= MIN_LEVEL) && (level <= MAX_LEVEL)){
                        levelJLabel.setText("Level:" + level);
                        drawSpace.setLevel(level);
                        repaint();
                    }
                }
            }//Fim da classe Interna anonima
        );//Fim de addActionListener

        controlJPanel.add(levelJLabel);

        //Cria um mainJPAnel para conter controlJPanel e drawSpace
        final JPanel mainJPanel = new JPanel();
        mainJPanel.add(controlJPanel);
        mainJPanel.add(drawSpace);

        add(mainJPanel);//Adiciona JPanel ao JFrame

        setSize(WIDTH,HEIGHT); //Configura o tamanho do JFrame
        setVisible(true); //Exibe JFrame
    }//Fim do construtor Fractal
    public static void main(String[]args){
        Fractal demo = new Fractal();
        demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}//Fim da classe Fractal