//FactorialCalculator.java
//Metodo fatorial recursivo

public class FactorialCalculator{
    public static long factorial(long number){
        if(number <= 1) //Tenta caso basico
            return 1; //Casos basicos: 0! = e 1! = 1
        else //Passo de recursao
            return number * factorial(number - 1);
    }

    //Gera saida de fatoriais para valores de 0 a 21
    public static void main(String[]args){
        //Calcula o fatorial de 0 a 21
        for (int counter = 0; counter <= 21; counter++)
            System.out.printf("%d! = %d%n", counter, factorial(counter));
    }
}//Fim da classe FactorialCalculator