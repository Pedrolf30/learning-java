//FractalJPanel.java
//Desenhando o fractal de Lo Feather com a recursao
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;

public class FractalJPanel extends JPanel{
    private Color color; //Armazena cor utilizada para desenhar o fractal
    private int level; //Armazena o nivel atual do fractal

    private static final int WIDTH = 400; //Define largura do JPanel
    private static final int HEIGHT = 400; //Define a largura do JPanel

    //Configura o nivel do fractal inicial com o valor especificado
    //E configura as especificacoes do JPanel
    public FractalJPanel(int currentLevel){
        color = Color.BLUE; //Inicializa a cor do desenho como azul
        level = currentLevel; //Configura o nivel do fractal inicial
        setBackground(Color.WHITE);
        setPreferredSize(new Dimension(WIDTH,HEIGHT));
    }
    //Desenha o fractal recursivamente
    public void drawFractal(int level, int xA, int yA, int xB, int yB, Graphics g){
        //Caso basico: desenha uma linha conectando dois pontos dados
        if(level ==0)
            g.drawLine(xA,yA,xB,yB);
        else //Passo de recursao: determina novos pontos, desenha proxima
        {
            //Calcula o ponto intermediario entre (xA,yA) e (xB, yB)
            int xC = (xA + xB)/2;
            int yC = (yA + yB)/2;

            //Calcula o quarto ponto(xD,yD) que forma um triangulo isoceles entre
            //(xA,yA) e (xC,yC) onde o angulo direito esta a (xD,yD)
            int xD = xA + (xC - xA) / 2 - (yC - yA) / 2;
            int yD = yA + (yC - yA) / 2 + (xC - xA) / 2;

            //Desenha recursivamente o fractal
            drawFractal(level -1, xD,yD,xA,yA,g);
            drawFractal(level -1, xD,yD,xC,yC,g);
            drawFractal(level -1, xD,yD,xB,yB,g);
        }
    }
    //Comeca a desenhar o fractal
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        
        //Desenha o padrao de fractal
        g.setColor(color);
        drawFractal(level,100,90,290,200,g);
    }

    //Configura a cor de desenho como c
    public void setColor(Color c){
        color = c;
    }
    //Configura o novo nivel de recursao
    public void setLevel(int currentLevel){
        level = currentLevel;
    }
    //Retorna o nivel de recursao
    public int getLevel(){
        return level;
    }
    
}//Fim da classe FractalJPanel  