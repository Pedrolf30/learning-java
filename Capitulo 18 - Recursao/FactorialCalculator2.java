//FactorialCalculator2.java
//Metodo fatorial recursivo
import java.math.BigInteger;

public class FactorialCalculator2{
    //Meotod fatorial recursivo
    public static BigInteger factorial(BigInteger number){
        if(number.compareTo(BigInteger.ONE) <= 0) //Caso basico de testes
            return BigInteger.ONE; //Casos basicos: 0! =1 e 1! =1
        else //Passo de recusao
            return number.multiply(factorial(number.subtract(BigInteger.ONE)));
    }
    //Gera saida de fatoriais para valores 0 a 50
    public static void main(String[]args){
        //Calcula o fatorial de 0 a 50
        for(int c =0;c<=50;c++)
            System.out.printf("%d! = %d%n",c,factorial(BigInteger.valueOf(c)));
    }
}//Fim da classe FactorialCalculator2