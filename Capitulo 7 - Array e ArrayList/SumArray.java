//SumArray.java
//Calculando a soma dos elementos de um array
public class SumArray{
    public static void main(String[]args){
        int [] array = {87,68,94,100,83,78,85,91,76,87};
        int total = 0;

        for(int i = 0;i < array.length;i++){ //Adiciona o valor de cada elemento ao total
            total+=array[i];
        }
        System.out.printf("Total of array elements: %d%n", total);
    }
}//Fim da classe SumArray