//GradeBook.java
//Class GradeBook utilizando um array para armazenar notas de teste
 public class GradeBook{
     private String courseName; //Nome do curso que essa GradeBook representa
     private int [] grades; //Array de notas de alunos

     //construtor
     public GradeBook(String courseName, int[] grades){
         this.courseName=courseName;
         this.grades=grades;
     }
     //Metodo para configurar o nome do curso
     public void setCourseName(String courseName){
         this.courseName=courseName;
     }
     //Metodo para recuperar o nome do curso
     public String getCourseName(){
         return courseName;
     }
     //Realiza varias operacoes nos dados
     public void processGrades(){

        //Gera saida de array das notas
        outputGrades();
        //Chama metodo getAverage para calcular a nota media
        System.out.printf("%nClasse average is %.2f%n", getAverage());
        //Chama metodos getMinimum e getMaximum
        System.out.printf("Lowest grade is %d%nHighest grade is %d%n",getMinimum(),getMaximum());
        //Chama outputBarChart para imprimir grafico de distribuicao de nota
        outputBarChart();
     }
     //Localiza nota minima
     public int getMinimum(){
         int lowGrade = grades [0]; //Supoe qye grades[0] e a menor nota

         //faz um loop pelo array de notas
         for(int grade : grades){
             //se nota for mais baixa que lowGrade, atribui essa nota a lowGrade
             if(grade < lowGrade)
                lowGrade = grade; //Nova nota mais baixa
         }
         return lowGrade;
     }
     //Localiza nota maxima
     public int getMaximum(){
         int highGrade = grades[0];//Supoe que grades[0] e a maior nota

         //faz um loop pelo array de notas
         for(int grade:grades){
             //Se a nota for maior que highGrade, atribui essa nota a highGrade
             if(grade > highGrade)
                highGrade = grade;
         }
         return highGrade;
     }
     //Determina a media para o teste
     public double getAverage(){
         int total = 0;

         //soma das notas de um alunos
         for(int grade:grades){
            total+=grade;
         }
         //Retorna a media das notas
         return (double) total / grades.length;
     }
     //Gera a saida do grafico de barras exibindo a distribuicao de notas
     public void outputBarChart(){
         System.out.println("Grades Distribution:");

         //armazena frequencia de notas em cada intervalo de 10 notas
         int [] frequency = new int[11];

         //para cada nota, incremente a frequencia da mesma
         for(int grade : grades){
             ++frequency[grade/10];
         }
         //para cada frequencia de nota, imprime barra no grafico
         for(int i = 0;i<frequency.length;i++){
            //gera saida do rotulo da barra
            if(i==10)
                System.out.printf("%5d: ",100);
            else
                System.out.printf("%02d=%02d: ",i*10,i*10+9);
            //Imprime a barra de asteriscos
            for(int stars = 0;stars<frequency[i];stars++){
            System.out.print("*");
            }
            System.out.println();
         }
     }
     //Gera a saida do conteudo do array de notas
     public void outputGrades(){
         System.out.printf("The grades are: %n%n");

         //Gera a saida da nota de cada aluno
         for(int student =0;student<grades.length;student++)
            System.out.printf("Student %2d: %3d%n", student +1,grades[student]);
     }
 }//Fim da clase GradeBook