//StudentPoll.java
//Programa de analise de enquete
public class StudentPoll{
    public static void main(String[]args){
        int[] responses = {1,2,5,4,3,5,2,1,3,3,1,4,3,3,3,2,3,3,2,14}; //Array das respostas da enquete
        int[] frequency = new int[6]; //Array de contadores de frequencia

        for(int a=0;a<responses.length;a++){
            try{
                ++frequency[responses[a]];
            }
            catch(ArrayIndexOutOfBoundsException e){
                System.out.println(e); //Invoca o metodo toString
                System.out.printf("    responses[%d] = %d%n%n", a,responses[a]);
            }
        }
        System.out.printf("%s%10s%n","Rating","Frequency");

        for(int rating = 1;rating<frequency.length;rating++){
            System.out.printf("%6d%10d%n", rating, frequency[rating]);
        }
    }
}//Fim da classe StudentPoll