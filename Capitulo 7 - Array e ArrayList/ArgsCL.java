//ArgsCL
//Inicializando um array com argumento de linha de comando
public class ArgsCL{
    public static void main(String[]args){
        //Verifica numero de argumentos de linha de comando
        if(args.length !=3)
            System.out.printf("Erro: Please re-enter the entire command, includin%n" + "an array size, initial value and increment");
        else{
            //obtem o tamanho do array a apartir do primeiro argumento de linha de comando
            int arrayLength = Integer.parseInt(args[0]);
            int [] array = new int[arrayLength];

            //obtem o valor inicial e o incrementa a partir dos argumentos de linha de comando
            int initialValue = Integer.parseInt(args[1]);
            int increment = Integer.parseInt(args[2]);

            //calcula valor de cada elemento do array
            for(int i=0;i<array.length;i++)
                array[i] = initialValue + increment * i;
            
            System.out.printf("%s%8s%n", "Index", "Value");
            //Exibe o valor e o indice de array
            for(int i=0;i<array.length;i++)
                System.out.printf("%5d%8d%n", i, array[i]);
        }
    }
}//Fim da classe ArgsCL