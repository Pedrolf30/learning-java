//RollDie.java
//Programa de jogo de dados utilizando arrays em vez de switch
import java.security.SecureRandom;

public class RollDie{
    public static void main(String[]args){
        SecureRandom randomNumbers = new SecureRandom();
        int [] frequency = new int [7]; //Array de contadores de frequencia

        for(int roll = 1; roll<=6000;roll++){ //Lanca o dado 6 mil vezes; usa o valor do dado como indice de frequencia
            ++frequency[1 + randomNumbers.nextInt(6)];
        }
        System.out.printf("%s%10s%n","Face","Frequency");
        
        for(int face=1;face<frequency.length;face++){ //Gera saida do valor de cada elemento do array
            System.out.printf("%4d%10d%n",face,frequency[face]);
        }
    }
}