//InitArray.java
//Inicializando elementos de um array como valores de padrao zero
public class InitArray{
    public static void main(String[]args){
        int [] array = new int [10]; //Cria o objeto array

        System.out.printf("%s%8s%n","Index","Value"); //Titulos de coluna

        for (int i = 0; i <array.length;i++){ //Gera saida do valor de cada elemento do array
            System.out.printf("%5d%8d%n",i,array[i]);
        }
    }
}//Fim da classe InitArray