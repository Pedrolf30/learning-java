//GradeBookBidimensional.java
//Classe GradeBook utilizando um array bidimensional para armazenar notas
public class GradeBookBidimensional{
    private String courseName; //Nome de curso que este livre de nota representa
    private int[][] grades; //Array bidimensional de notas de um aluno

    //Construtor de dois argumentos inicializa courseName e array de notas
    public GradeBookBidimensional(String courseName, int[][] grades){
        this.courseName=courseName;
        this.grades=grades;
    }
    //Metodo para configurar o nome do curso
    public void setCourseName(String courseName){
        this.courseName=courseName;
    }
    //Metodo para recuperar o nome do curso
    public String getCourseName(){
        return courseName;
    }
    //Realiza varias operacoes nos dados
    public void processGrades(){
        //gera saida de array de notas
        outputGrades();
        //chama metodos getMinimum e getMaximum
        System.out.printf("%n%s %d%n%s %d%n%n", "Lowest grade in the grade book is:", getMinimum(),"Highest gradein the grade book is:",getMaximum());
        //gera a saida de grafico de distribuicao de notas de todas as notas em todos os testes
        outputBarChart();
    }
    //Localiza a nota minima
    public int getMinimum(){
        //Supoe que o primeiro elemento de array de notas e o menor
        int lowGrade = grades[0][0];

        //faz um loop pelas linhas do array de notas
        for(int[]studentGrades:grades){
            //faz um loop pelas colunas da linha atual
            for(int grade:studentGrades){
                //se a nota for menor que lowGrade, atribui a nota a lowGrade
                if(grade < lowGrade)
                    lowGrade = grade;
            }
        }
        return lowGrade;
    }
    public int getMaximum(){
        //Supoe que o primeiro elemento de array de notas e o maior
        int highGrade = grades[0][0];

        //faz um loop pelas linhas do array de notas
        for(int[] studentGrades:grades){
            //faz um loop pelas colunas da linha atual
            for(int grade:studentGrades){
                //se a nota for maior do que highGrades, atribui nota a highGrades
                if(grade>highGrade)
                    highGrade = grade;
            }
        }
        return highGrade;
    }
    //Determina a media do conjunto particular de notas
    public double getAverage(int[] setOfGrades){
        int total = 0;

        //soma notas de um aluno
        for(int grade:setOfGrades)
            total+=grade;
        //Retorna media de notas
        return (double) total / setOfGrades.length;
    }
    //Gera a saida do grafico de barras para exibir distribuicao total de notas
    public void outputBarChart(){
        System.out.println("Overall grade distributions");
        //armazena frequencia de notas em cada intervalo de 10 notas
        int[] frequency = new int[11];
        //para cada nota em GradeBook, incrementa a frequencia apropriada
        for(int[]studentGrades:grades){
            for(int grade:studentGrades)
                ++frequency[grade/10];
        }
        //para cada frequencia de nota, imprime barra no grafico
        for(int i=0;i<frequency.length;i++){
            //gera a saida do rotulo e barra
            if(i==10)
                System.out.printf("%5d ",100);
            else
                System.out.printf("%02d-%02d",i*10,i*10+9);
            for(int stars=0;stars<frequency[i];stars++)
                System.out.print("*");
            System.out.println();
        }
    }
    //gera a saida do conteudo do array de notas
    public void outputGrades(){
        System.out.printf("The grades are now:%n%n");
        System.out.print("           ");//Alinha titulos de coluna

        //cria um titulo de coluna para cada um dos testes
        for(int test=0;test<grades[0].length;test++)
            System.out.printf("Test %d ", test+1);
        
        System.out.println("Average");//Titulo de coluna de media do aluno

        //cria colunas e linhas de texto que representam notas de array
        for(int student=0;student<grades.length;student++){
            System.out.printf("Student %2d", student+1);

            for(int test: grades[student])//gera saida de notas do aluno
                System.out.printf("%8d", test);
            
                //chama metodo getAverage para calcular a media do aluno
                //passa linha de notas como argumento para getAverage
                double average = getAverage(grades[student]);
                System.out.printf("%9.2f%n", average);
        }
    }
}//Fim da classe GradeBook