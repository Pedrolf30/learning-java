//Card.java
//Classe card representa uma carta de baralho
public class Card{
    private final String face; //face da carta
    private final String suit; //Naipe da carta

    //Construtor de dois argumentos inicializa face e naipe da carta
    public Card(String cardFace, String cardSuit){
        this.face=cardFace; //Inicializa face da carta
        this.suit=cardSuit; //Inicializa naipe da carta
    }
    //Retorna representação String de Card
    public String toString(){
        return face + " of " + suit;
    }
}//Fim da classe Card
