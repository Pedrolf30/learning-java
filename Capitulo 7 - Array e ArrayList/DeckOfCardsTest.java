//DeckOfCardsTest.java
//Embraralhando e distribuindo cartas

public class DeckOfCardsTest{
    //executa o aplicativo
    public static void main(String[]args){
        DeckOfCards myDeck = new DeckOfCards();
        myDeck.shuffle(); //Coloca Cards em ordem aleatória

        //impreme todas as 52 cartas na ordem em que elas são distribuidas
        for(int i =1;i<=52;i++){
            //Distribui e exibe uma card
            System.out.printf("%-19s", myDeck.dealCard());

            if(i % 4 == 0) //Gera uma nova linha após cada quarta carta
                System.out.println();
        }
    }
}//Fim da classe DeckOfCardsTest