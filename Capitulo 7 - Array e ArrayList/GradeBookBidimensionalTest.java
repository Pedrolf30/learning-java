//GradeBookBidimensionalTest.java
//Testa GradeBookBidimensional
public class GradeBookBidimensionalTest{
    public static void main(String[]args){
        //array bidimensional de nota de aluno
        int[][] gradesArray = {{87,96,70},
    {68,87,90},
    {94,100,90},
    {100,81,82},
    {83,65,85},
    {87,87,65},
    {91,94,100},
    {76,72,84},
    {87,93,73}};

    GradeBookBidimensional myGB = new GradeBookBidimensional("Java Programming", gradesArray);
    System.out.printf("Welcome to the grade book  for%n%s%n%n", myGB.getCourseName());
    myGB.processGrades();
    }
}//Fim da classe GradeBookBidimensionalTest