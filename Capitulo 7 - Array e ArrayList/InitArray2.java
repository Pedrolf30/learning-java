//InitArray2.java
//Inicializando os elementos de um array com um inicializador de array

public class InitArray2{
    public static void main(String[]args){
        int [] array = {32,27,64,18,95,14,90,70,60,37}; //A lista de inicializador especifica o valor inicial de cada elemento

        System.out.printf("%s%8s%n","Index","Value"); //Titulos de coluna

        for(int i=0;i<array.length;i++){ //Gera a saida do valor de cada elemento array
            System.out.printf("%5d%8d%n",i,array[i]);
        }
    }
}//Fim da classe InitArray2