//ArrayListCollection.java
//Demonstracao da colecao ArrayList <t> generica
import java.util.ArrayList;

public class ArrayListCollection{
    public static void main(String[]args){
        //cria um novo ArryaList de string com uma capacidade inicial de 10
        ArrayList<String> items = new ArrayList<String>();

        items.add("red");//Aenxa um item a lista
        items.add(0,"yellow");//Insere Yellow no indice 0

        //cabecalho
        System.out.println("Display list contesnt with counter controlled loop:");

        //exibe as cores na lista
        for(int i=0;i<items.size();i++)
            System.out.printf(" %s", items.get(i));
        
        //exibe as cores usando for aprimorada no metodo display
        display(items,"%nDisplat list contents with enhanced for statement:");
        items.add("green");
        items.add("yellow");
        display(items, "List with two new elements:");

        items.remove("yellow");
        display(items, "List with first Yellow removed:");
        
        items.remove(1);
        display(items, "List with second list element removed:");

        //verifica se um valor esta na lista
        System.out.printf("\"red\" is %sin the list%n", items.contains("red") ? "":"not ");

        //exibe o numero de elementos na List
        System.out.printf("Size: %s%n", items.size());
    }
    //Exibe elementos do ArrayList no console
    public static void display(ArrayList<String> items, String header){
        //Exibe cada elemento em items
        for(String item: items)
            System.out.printf(" %s", item);
        
        System.out.println();
    }
}//Fim da classe ArrayListCollection