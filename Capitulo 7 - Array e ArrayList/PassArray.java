//PassArray.java
//Passando arrays e elementos do array individuais aos metodos
public class PassArray{
    //main cria array e chama modifyArray e modifyElement
    public static void main(String[]args){
        int [] array = {1,2,3,4,5};

        System.out.printf("Effects of passing reference to entire array:%n"+
                          "The values of the original array are:%n");
        
        //Gera saida de elementos do array original
        for(int value:array)
            System.out.printf("   %d",value);
        
            modifyArray(array); //Passa a referencia do array
            System.out.printf("%n%nThe values of the modified array are: %n");

            //Gera saida de elementos do array modificado
            for(int value : array)
                System.out.printf("   %d",value);
            
            System.out.printf("%n%nEffects of passing array element value:%n"+
                              "Array[3] before modifyElement: %d%n", array[3]);
            modifyElement(array[3]); //Tenta Modificar o array[3] 
            System.out.printf("array[3] after modifyElement: %d%n", array[3]); 
    }
    //Mutiplica da elemento de um array por 2
    public static void modifyArray(int [] array2){
        for(int i =0; i <array2.length;i++)
            array2[i] *=2;
    }
    //Multiplica argumento por 2
    public static void modifyElement(int element){
        element*=2;
        System.out.printf("Value of element in modifyElement: %d%n", element);
    }
}//Fim da classe PassArray