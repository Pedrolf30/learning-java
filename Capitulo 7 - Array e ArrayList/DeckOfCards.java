//DeckOfCards.java
//Classe DeckOfCards representa um baralho
import java.security.SecureRandom;

public class DeckOfCards{
    private Card[] deck; //Array de objetos card
    private int currentCard; //Indice da proxima carta a ser distribuida
    private static final int NUMBER_OF_CARDS =52; //Numero constanto de cartas
    //Gerador de numero aleatório
    private static final SecureRandom randomNumbers = new SecureRandom();

    //Construtor preench baralho de cartas
    public DeckOfCards(){
        String[] faces = {"Ace","Deuce","Three","Four","Five","Six","Seven",
        "Eight","Nine","Ten","Jack","Queen","King"};
        String [] suits = {"Hearts","Diamonds","Clubs","Spades"};
        deck = new Card[NUMBER_OF_CARDS]; //Cria array de objetos card
        currentCard = 0; //A primeira carta ditribuida será o deck[0]

        //Preenche o baralho com objetos Card
        for(int i = 0;i < deck.length;i++){
            deck[i] = new Card(faces[i%13], suits[i/13]);
        }
    }
    //Embaralha as cartas com um algoritmo de passagem    
    public void shuffle(){
        //A próxima chamada para o método dealCard deve começar no deck[0] novamente
        currentCard = 0;
        //Para cada Card, seleciona outra Card aleatória (0-51) e as compara
        for(int first = 0;first<deck.length;first++){
            //Seleciona um numero aleatório entre 0 e 51
            int second = randomNumbers.nextInt(NUMBER_OF_CARDS);

            //Compara Card atual com Card aleatóriamente selecionada
            Card temp = deck[first];
            deck[first] = deck[second];
            deck[second] = temp;
        }
    }
    //Distribui uma Card
    public Card dealCard(){
        //Determina se ainda há Cards a serem distribuidas
        if(currentCard < deck.length){
            return deck[currentCard++]; //Retorna Card atual no array
        }
        else{
            return null;//Retorna nulo para indicar que todos as Cards foram distribuidas
        }
    }
    
}//Fim da classe DeckOfCards