//SortedSetTest.java
//Usando SortedSets e TreeSets
import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

public class SortedSetTest{
    public static void main(String[]args){
        //Cria TreeSet a partir do array colors
        String[] colors = {"yellow", "green", "black", "tan", "grey",
            "white", "orange", "red", "green"};
        
        SortedSet<String> tree = new TreeSet<>(Arrays.asList(colors));

        System.out.print("Sorted set: ");
        printSet(tree);

        //Obtem headSet com base em orange
        System.out.print("headSet (\"orange\"): ");
        printSet(tree.tailSet("orange"));

        //Obtem tailSet baseado em Orange
        System.out.print("tailSet (\"orange\"): ");
        printSet(tree.tailSet("orange"));

        //Obtem o primeiro e o ultimo elemento
        System.out.printf("first: %s%n", tree.first());
        System.out.printf("last : %s%n", tree.last());
    }
    // envia SortedSet para a saída usando a instrução for aprimorada
    private static void printSet(SortedSet<String> set){
        for (String s : set)
            System.out.printf("%s ", s);
        
        System.out.println();
    }
}//Fim da classe SortedSetTest