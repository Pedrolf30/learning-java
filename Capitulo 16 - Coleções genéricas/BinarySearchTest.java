//BinarySearchTest.java
//Método binarySearch de Collections
import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.ArrayList;

public class BinarySearchTest{
    public static void main(String[]args){
        //Cria um ArrayList <String> a partir do conteudo do array colors
        String[] colors = {"red","white","blue","black","yellow",
            "purple","tan","pink"};
        List<String> list = new ArrayList<>(Arrays.asList(colors));
        
        Collections.sort(list); //Classifica a ArrayList
        System.out.printf("Sorted ArrayList: %s%n",list);

        //Pesquisa vários valores na lista
        printSearchResults(list, "black"); //primeiro item
        printSearchResults(list, "red"); //Item do meio
        printSearchResults(list, "pink"); //Ultimo item
        printSearchResults(list, "aqua"); //Abaixo do mais baixo
        printSearchResults(list, "gray"); //Não existe
        printSearchResults(list, "teal"); //Não existe
    }

    //Reliza pesquisa e exibe o resultado
    private static void printSearchResults(List<String> list, String key){
        int result =0;
        
        System.out.printf("Searching for: %s%n",key);
        result = Collections.binarySearch(list,key);

        if(result >= 0)
            System.out.printf("Found at index %d%n%n", result);
        else
            System.out.printf("Not found (%d)%n%n", result);
    
    }
}