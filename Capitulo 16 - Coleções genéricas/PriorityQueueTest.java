//PriorityQueueTest.java
//Programa de teste de PriorityQueue
import java.util.PriorityQueue;

public class PriorityQueueTest{
    public static void main(String[]args){
        //Fila de capacidade 11
        PriorityQueue<Double> queue = new PriorityQueue<>();

        //Insere elementos na fila
        queue.offer(3.2);
        queue.offer(9.8);
        queue.offer(5.4);

        System.out.print("Polling from queue: ");

        //Exibe elementos na fila
        while(queue.size()>0){
            System.out.printf("%.1f  ",queue.peek()); //Visualiza o elemento superior
            queue.poll(); //Remove o elemento superior
        }
    }
}//Fim da classe PriorityQueueTest