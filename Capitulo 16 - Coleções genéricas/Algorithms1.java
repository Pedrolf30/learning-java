//Algorithms1.java
//Métodos Collections reverse,fill,copy,max e min
import java.util.List;
import java.util.Arrays;
import java.util.Collections;

public class Algorithms1{
    public static void main(String[]args){
        //Cria e exibe uma List<Character>
        Character[] letters = {'P','C','M'};
        List<Character> list = Arrays.asList(letters); //Obtem List
        System.out.println("List contains: ");
        output(list);

        //Inverte e exibe uma List<Character>
        Collections.reverse(list); //Inverte a ordem dos elementos
        System.out.printf("%nAfter calling reverse, list contains:%n");
        output(list);

        //Cria CopyList de um array de três caracteres
        Character[] lettersCopy = new Character[3];
        List<Character> copyList = Arrays.asList(lettersCopy);

        //Copia o conteudo da list para CopyList
        Collections.copy(copyList, list);
        System.out.printf("%nAfter copying, copyList contains:%n");
        output(copyList);

        //Preenche a lista com Rs
        Collections.fill(list, 'R');
        System.out.printf("After calling fill, list contains:%n");
        output(list);
    }
    //Envia informações de List para saida
    private static void output(List<Character> listRef){
        System.out.println("The list is: ");

        for(Character element : listRef)
            System.out.printf("%s ", element);

        System.out.printf("%nMax: %s", Collections.max(listRef));
        System.out.printf(" Min: %s%n", Collections.min(listRef));
    }
}//Fim da classe Algorithms1