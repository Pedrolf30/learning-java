//PropertiesTest.java
//Demonstra classe Properties do pacte java.util
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

public class PropertiesTest{
    public static void main(String[]args){
        Properties table = new Properties();

        //Configura propriedades
        table.setProperty("color", "blue");
        table.setProperty("width", "200");

        System.out.println("After setting properties");
        listProperties(table);

        //Substitui o valor de propriedade
        table.setProperty("color", "red");

        System.out.println("After replacing properties");
        listProperties(table);

        saveProperties(table);
        table.clear();

        System.out.println("After clearing properties");
        listProperties(table);

        loadProperties(table);

        //Obtem valor de cor da propriedade
        Object value = table.getProperty("color");

        //Verifica se o valor esta na tabela
        if(value != null)
            System.out.printf("Property color's value is %s%n", value);
        else    
            System.out.printf("Property color is not in table");
    }
    //Salva as propriedades em um arquivo
    private static void saveProperties(Properties props){
        //Salva o conteudo da tabela
        try{
            FileOutputStream output = new FileOutputStream("props.dat");
            props.store(output, "Sample Properties"); //Salva as propriedades
            output.close();
            System.out.println("After saving properties");
            listProperties(props);
        }
        catch(IOException ioException){
            ioException.printStackTrace();
        }
    }
    //Carregava as propriedades de um arquivo
    private static void loadProperties(Properties props){
        //Carrega o conteudo de tabela
        try{
            FileInputStream input = new FileInputStream("props.dat");
            props.load(input); //Carrega propriedades
            input.close();
            System.out.println("After loading properties: ");
            listProperties(props);
        }
        catch(IOException ioException){
            ioException.printStackTrace();
        }
    }
    //Gera a saida de valores de propriedade
    private static void listProperties(Properties props){
        Set<Object> keys = props.keySet(); //Obtem nomes de propriedade

        //Gera saida de pares nome/valor
        for(Object key : keys)
            System.out.printf("%s\t%s%n", key, props.getProperty((String) key));
        
        System.out.println();
    }    
}//Fim da classe PropertiesTest
