//SetTest.java
//HashSet utilizada para remover valores duplicados do array de strings
import java.util.List;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Collection;

public class SetTest{
    public static void main(String[]args){
        //Cria e exibe uma List<String>
        String[] colors = {"red","white","blue","green","gray","orange","tan",
            "white","cyan","peach","gray","orange"};
        List<String> list = Arrays.asList(colors);
        System.out.printf("List: %s%n",list);

        //Elimina duplicatas, entao imprime os valores unicos
        printNonDuplicates(list);
    }
    //Cria um Set de uma colecao para eliminar duplicatas
    private static void printNonDuplicates(Collection<String> values){
        //Cria um HashSet
        Set<String> set = new HashSet<>(values);

        System.out.printf("%nNon Duplicates are: ");

        for(String value : set)
            System.out.printf("%s ",value);
        System.out.println();
    }
}//Fim da classe SetTest