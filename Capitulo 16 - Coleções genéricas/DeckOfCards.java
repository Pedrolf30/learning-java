//DeckOfCards.java
//Embaralhamento e distribuição de cartas com método shuffle de Collections
import java.util.List;
import java.util.Arrays;
import java.util.Collections;

//Classe para representar uma Card de um baralho
class Card{
    public static enum Face{Ace,Deuce,Three,Four,Six,
        Seven,Eight,Nine,Ten,Jack,Queen,King};
    public static enum Suit{Clubs,Diamonds,Hearts,Spades};

    private final Face face;
    private final Suit suit;

    //Construtor
    public Card(Face face, Suit suit){
        this.face=face;
        this.suit=suit;
    }
    //Retorna face da carta
    public Face getFace(){
        return face;
    }
    //Retorna naipe de Card
    public Suit getSuit(){
        return suit;
    }
    //Retorna representação string de card
    public String toString(){
        return String.format("%s of %s", face, suit);
    }
}//Fim da classe Card

//Declaração da classe DeckOfCards
public class DeckOfCards{
    private List<Card> list; //Declara list que armazenará Cards

    //COnfigura baralho de Cards e embaralha
    public DeckOfCards(){
        Card[] deck = new Card[52];
        int c = 0; //Numero de Cartas

        //Preenche baralho com objetos Card
        for(Card.Suit suit: Card.Suit.values()){
            for(Card.Face face: Card.Face.values()){
                deck[c] = new Card(face, suit);
                ++c;
            }
        }
        list = Arrays.asList(deck); //Obtem list
        Collections.shuffle(list); //Embaralha as cartas
    }//Fim do construtor DeckOfCards
    //Gera saida de baralho
    public void printCards(){
        //Exibe 52 cartas em duas colunas
        for(int i = 0; i<list.size();i++)
            System.out.printf("%-19s%s",list.get(i),
                ((i+1)%4==0)? "\n ":"");
    }
    public static void main(String[]args){
        DeckOfCards cards = new DeckOfCards();
        cards.printCards();
    }
}//FIm da classe DeckOfCards


