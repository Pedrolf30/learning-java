//ListTest.java
//Lists, LinkedLists e ListIterators
import java.util.List;
import java.util.LinkedList;
import java.util.ListIterator;

public class ListTest{
    public static void main(String[] args){
        //Adiciona elementos colors à list1
        String[] colors = {"black","yellow","green","blue","violet","silver"};
        List<String> list1 = new LinkedList<>();

        for(String color : colors)
            list1.add(color);
        
        //Adiciona elementos colors à list2
        String[] colors2 = {"gold","white","brown","blue","gray","silver"};
        List<String> list2 = new LinkedList<>();

        for(String color : colors2)
            list2.add(color);

        list1.addAll(list2); //Concatena as listas
        list2 = null; //Libera recursos
        printList(list1); //Imprime elementos list1

        convertToUppercaseStrings(list1); //Converte em string de letra maiuscula
        printList(list1);//Imprime elementos list1
        
        System.out.printf("%nDeleting elements 4 to 6");
        removeItem(list1,4,7); //Remove itens 4 a 6 da lista
        printList(list1);//Imprime elementos list1
        printReversedList(list1); //Imprime lista na ordem inversa
    }
    //Gera saida do conteudo de List
    private static void printList(List<String> list){
        System.out.printf("%nList:%n");

        for(String color: list)
            System.out.printf("%s ", color);

        System.out.println();
    }
    //Localiza objetos String e converte em letras maiusculas
    private static void convertToUppercaseStrings(List<String> list){
        ListIterator<String> iterator = list.listIterator();

        while(iterator.hasNext()){
            String color = iterator.next();//Obtem o item
            iterator.set(color.toUpperCase()); //Converte em letras maiusculas
        }
    }
    //Obtem sublista e utiliza método clear para excluir itens da sublista
    private static void removeItem(List<String> list, int start, int end){
        list.subList(start,end).clear(); //Remove os items
    }
    //Imprime lista invertida
    private static void printReversedList(List<String> list){
        ListIterator<String> iterator = list.listIterator(list.size());
        System.out.printf("%nReversed List:%n");

        //Imprime lista na ordem inversa
        while(iterator.hasPrevious())
            System.out.printf("%s ",iterator.previous());
    }
}//Fim da classe ListTest