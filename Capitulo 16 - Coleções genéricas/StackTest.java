//StackTest.java
//Classe Stack do pacote java.util
import java.util.Stack;
import java.util.EmptyStackException;

public class StackTest{
    public static void main(String[]args){
        Stack<Number> stack = new Stack<>(); //Cria uma Stack

        //Utiliza metodo push
        stack.push(12L); //Insere o valor long 12L
        System.out.println("Pushed 12L");
        printStack(stack);
        stack.push(34567); //Insere o valor int 34567
        System.out.println("Pushed 34567");
        printStack(stack);
        stack.push(1.0F); //Insere o valor float 1.0F
        System.out.println("Pushed 1.0F");
        printStack(stack);
        stack.push(1234.5678); //Insere o valor double 1234.5678
        System.out.println("Pushed 1234.5678");
        printStack(stack);

        //Remove itens de pilha
        try{
            Number removedObject = null;

            //Remove elementos de pilha
            while(true){
                removedObject = stack.pop(); //Utiliza metodo pop
                System.out.printf("Popped %s%n", removedObject);
                printStack(stack);
            }
        }
        catch(EmptyStackException emptyStackException){
            emptyStackException.printStackTrace();
        }
    }
    //Exibe o conteudo de stack
    private static void printStack(Stack<Number> stack){
        if(stack.isEmpty())
            System.out.printf("Stack is empty%n%n"); //Plha esta vazia
        else //A pilha nao esta vazia
            System.out.printf("stack contains: %s (top) %n", stack);
    }
}//Fim da classe StackTest