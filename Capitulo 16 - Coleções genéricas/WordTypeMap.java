//WordTypeMap.java
//O programa conta o numero de ocorrencias de cada palavra em uma String
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.Scanner;

public class WordTypeMap{
    public static void main(String[]args){
        //Cria HashMap para armazenar chevaes de String e valores Integer
        Map<String, Integer> myMap = new HashMap<>();
        
        createMap(myMap); //Cria mapa com base na entrada do usuario
        displayMap(myMap); //Exibe o conteudo do mapa
    }

    //Cria mapa de entrada de usuario
    private static void createMap(Map<String, Integer> map){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a string: "); //Solicita a entrada do usuario com uma string
        String input = sc.nextLine();

        //Tokeniza a entrada
        String[] tokens = input.split(" ");

        //Processamento de texto de entrada
        for(String token : tokens){
            String word = token.toLowerCase(); //Obtem a palavra em letras

            //Se o mapa conter a palavra
            if(map.containsKey(word)){ //A palavra esta no mapa
                int count = map.get(word); //Obtem a contagem atual
                map.put(word,count +1); //Incrementa a contagem
            }
            else
                map.put(word,1); //Adiciona nova palavra com uma contagem de 1 para mapa
        }
    }
    //Exibe conteudo do mapa
    private static void displayMap(Map<String,Integer> map){
        Set<String> keys = map.keySet(); //Obtem as chaves

        //Classifica as chaves
        TreeSet<String> sortedKeys = new TreeSet<>(keys);

        System.out.printf("%nMap contains: %nKey\t\tValue%n");

        //Gera saida de cada chave no mapa
        for(String key : sortedKeys)
            System.out.printf("%-10s%10s%n",key,map.get(key));
        
       System.out.printf("%nsize: %d%nisEmpty: %b%n", map.size(),map.isEmpty());
    }
}//Fim da classe WordTypeMap