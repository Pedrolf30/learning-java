//UsingToArray.java
//Visualizando arrays como Lists e convertendo Lists em arrays
import java.util.LinkedList;
import java.util.Arrays;

public class UsingToArray{
    //Cria uma linkedList, adiciona elementos e converte em array
    public static void main(String[]args){
        String[]colors = {"black","blue","yellow"};
        LinkedList<String> links = new LinkedList<>(Arrays.asList(colors));

        links.addLast("red"); //Adiciona como o ultimo item
        links.add("pink"); //Adiciona ao final
        links.add(3,"green"); //Adiciona no terceiro indice
        links.addFirst("cyan"); //Adiciona como primeiro item

        //Obtem elementos LinkedList como um array
        colors = links.toArray(new String[links.size()]);

        System.out.println("colors: ");

        for(String color : colors)
            System.out.println(color);
    }
}//Fim da classe UsingToArray