//Time1Test.java
//Objeto Time1 utilizado em aplicativo

public class Time1Test{
    public static void main(String[]args){
        //Cria e inicializa um objeto Time1
        Time1 time = new Time1(); //Invoca o construtor Time1

        //Gera saida de representacoes de string da data/hora
        displayTime("After time object is created", time);
        System.out.println();

        //Altera a data/hora e gera saida de data/hora atualizada
        time.setTime(13,27,6);
        displayTime("After calling setTime", time);
        System.out.println();

        //Tenta definir data/hora com valores invalidos
        try{
            time.setTime(99,99,99); //Todos os valores fora do intervalo
        }
        catch(IllegalArgumentException e){
            System.out.printf("Exception: %s%n%n", e.getMessage());
        }

        //Exibe a data/hora apos uma tentativa de definir valores invalidos
        displayTime("After calling setTime with invalid values", time);
    }

    //Exibe um objeto Time1 nos formados de 24 ghoras e 12 horas
    private static void displayTime(String header, Time1 t){
        System.out.printf("%s%nUniversal time: %s%nStandart time: %s%n", header, t.toUniversalString(),t.toString());
    }
}//Fim da classe Time1Test