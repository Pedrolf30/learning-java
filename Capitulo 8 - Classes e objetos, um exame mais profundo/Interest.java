//Interest.java
//Calculo de juros compostos com BigDecimal
import java.math.BigDecimal;
import java.text.NumberFormat;

public class Interest{
    public static void main(String[]args){
        //quantidade principal inicial antes dos juros
        BigDecimal principal = BigDecimal.valueOf(1000.0);
        BigDecimal rate = BigDecimal.valueOf(0.05);//Taxa de juros

        //Exibe os cabecalhos
        System.out.printf("%s%20s%n", "Year","Amount on deposit");

        //Calcula quantidade de deposito para cada um dos dez anos
        for(int y = 1;y<=10;y++){
            //Calcula a nova quantidade durante ano especificado
            BigDecimal amount = principal.multiply(rate.add(BigDecimal.ONE).pow(y));

            //Exibe o ano e a quantidade
            System.out.printf("%4d%20s%n", y, NumberFormat.getCurrencyInstance().format(amount));
        }
    }
}//Fim da classe Interest