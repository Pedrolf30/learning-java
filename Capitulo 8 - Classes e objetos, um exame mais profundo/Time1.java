//Time1.java
//Declaracao de classe Time1 mantem a data e hora no formato de 24 horas

public class Time1{
    private int hour; //0-23
    private int minute;//0-59
    private int second;//0-59

    //Configura um novo valor de tempo usando hora universal;
    //Lanca uma excecao se a hora,minuto,ousegundo for invalido
    public void setTime(int hour, int minute, int second){

        //valida hora,minuto e segundo
        if(hour<0 || hour>=24 || minute <0||minute>=60||second<0 || second >=60){
            throw new IllegalArgumentException("hour, minute and/or second was out of range");
        }

        this.hour=hour;
        this.minute=minute;
        this.second=second;
    }
    //Converte em String no formato de data/hora universal (HH:MM:SS)
    public String toUniversalString(){
        return String.format("%02d:%02d:%02d",hour,minute,second);
    }
    //Converte em String no formato padrao de data/hora (H:MM:SS AM ou PM)
    public String toString(){
        return String.format("%d:%02d:%02d %s",((hour==0 || hour ==12) ? 12 : hour % 12),minute,second,(hour < 12 ? "AM" : "PM"));
    }
}//Fim da classe Time1