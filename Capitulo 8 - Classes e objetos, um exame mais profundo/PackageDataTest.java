//PackageDataTest.java
//Membros de acesso de pacote de uma classe permanecem acessiveis a outra classes no mesmo pacote

public class PackageDataTest{
    public static void main(String[]args){
        PackageData  packageData = new PackageData();

        //Gera saida da representacao String de packageData
        System.out.printf("After instatiation: %n%s%n", packageData);

        //muda os dados de acesso de pacote no objeto packageData
        packageData.number= 77;
        packageData.string= "Goodbye";

        //Gera saida de representacao String de packageData
        System.out.printf("%nAfter changing values: %n%s%n", packageData);
    }
}//Fim da classe PackageDataTest

//Clase com variaveis de instancia de acesso de pacote
class PackageData{
    int number; //variavel de instancia de acesso de pacote
    String string; //variavel de instancia de acesso de pacote

    //construtor
    public PackageData(){
        number = 0;
        string = "Hello";
    }
    //retorna a representacao String do objeto PackageData
    public String toString(){
        return String.format("number: %d; string: %s", number, string);
    }
}//Fim da classe PackageData