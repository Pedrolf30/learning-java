//Book.java
//Declarando um tipo de enum com um construtor e campos de instancia explicitos
//e metodos de acesso para esses campos

public enum Book{
    //declara constantes do tipo enum
    JHTP("Java How to Program","2015"),
    CHTP("C How to Program","2013"),
    IW3HTP("Internet & World Wide Web How to Program","2012"),
    CPPHTP("C++ How to Program","2014"),
    VBHTP("Visual C# How to Program","2014");

    //Campos de instancia
    private final String title; //Titulo do livro
    private final String copyrightYear;//Ano dos direitos autorais

    //Construtor enum
    Book(String title, String copyrightYear){
        this.title=title;
        this.copyrightYear=copyrightYear;
    }

    //acessor para titulo de campo
    public String getTitle(){
        return title;
    }
    //acessor para o campo copyrightYear
    public String getCopyRightYear(){
        return copyrightYear;
    }

}//Fim do enum Book