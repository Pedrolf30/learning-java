//ThisTest.java
//This utilizado implicita e explicitamente para referencia de membros a um objeto

public class ThisTest{
    public static void main(String[]args){
        SimpleTime time = new SimpleTime(15,30,19);
        System.out.println(time.buildString());
    }
}//Fim da classe ThisTest

//classe SimpleTime demonstra a referencia "this"
class SimpleTime{
    private int hour;//0-23
    private int minute;//0-59
    private int second;//0-59

    /*se o construtor utilizar nome de parametro identicos a nomes
    de variaveis de instancia a referencia "this" sera exigida para
    distinguir entre os nome*/
    public SimpleTime(int hour, int minute, int second){
        this.hour=hour; //Configura a hora do objeto "this"
        this.minute=minute;//Configura o minuto do objeto "this"
        this.second=second;//Configura o segundo do objeto "this"
    }

    //utilizam "this" explicito e implicito para chama toUniversalString
    public String buildString(){
        return String.format("%24s: %s%n%24s: %s","this.toUniversalString()",this.toUniversalString(),"toUniversalString",toUniversalString());
    }
    //Converte em String no formato de data/hora universal (HH:MM:SS)
    public String toUniversalString(){
        /*"this" nao e requerido aqui para acessa as variaveis de instancia,
        porque o metodo nao tem variaveis locais com os mesmos nomes das 
        variaveis de instancia*/
        return String.format("%02d:%02d:%02d", this.hour,this.minute,this.second);
    }
}//Fim da classe SimpleTime
