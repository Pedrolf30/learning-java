//Employee2Test.java
//Demonstracao do membro static

public class Employee2Test{
    public static void main(String[]args){
        //Mostra que a contagem eh 0 antes de criar Employees
        System.out.printf("Employees before instatiation: %d%n", Employee2.getC());

        //Cria dois Employees; a contagem deve resultar em 2
        Employee2 e1 = new Employee2("Susan","Baker");
        Employee2 e2 = new Employee2("Bob", "Blue");

        //Mostra que a contage tem valor de 2, depois de criar dois employees
        System.out.printf("%nEmployees after instatiation:%n");
        System.out.printf("via e1.getC(): %d%n", e1.getC());
        System.out.printf("via e2.getC(): %d%n", e2.getC());
        System.out.printf("via Employee2.getC(): %d%n", Employee2.getC());

        //Obtem nomes de employees
        System.out.printf("%nEmployee 1: %s %s%nEmployee 2: %s %s%n", e1.getFS(), e1.getLS(),e2.getFS(),e2.getLS());
    }
}//Fim da classe Employee2Test