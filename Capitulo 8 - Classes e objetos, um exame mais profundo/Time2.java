//Time2.java
//Declaracao da classe Time2 com construtores sobrecarregados

public class Time2{
    private int hour;
    private int minute;
    private int second;

    //construtor sem argumento Time2:
    //Inicializa cada variavel de instancia para zero

    public Time2(){
        this(0,0,0); //Invoce o construtor com 3 argumentos
    }
    //Construtor Time2: hora fornecida,minuto e segundo padronizados para zero
    public Time2(int hour){
        this(hour,0,0);
    }
    //Construtor Time2: hora e minuto fornecidos, segundos padronizados para zero
    public Time2(int hour, int minute){
        this(hour,minute,0);
    }
    //Construtor Time2: hour, minute e second fornecidos
    public Time2(int hour, int minute, int second){
        if(hour < 0 || hour >=24)
            throw new IllegalArgumentException("Hour must be 0-23");
        if(minute < 0 || minute >=60)
            throw new IllegalArgumentException("Minute must be 0-59");
        if(second < 0 || second >=60)
            throw new IllegalArgumentException("Second must be 0-59");
        
        this.hour=hour;
        this.minute=minute;
        this.second=second;
    }
    //Construtor Time2: outro objeto Time2 forncedo
    public Time2(Time2 time){
        //Invoca o construtor com tres argumentos
        this(time.getHour(), time.getMinute(), time.getSecond());
    }
    //Metodos set
    //Configura um novo valor de tempo usando hora universal
    //Valida os dados
    public void setTime(int hour, int minute, int second){
        if(hour < 0 || hour >=24)
            throw new IllegalArgumentException("Hour must be 0-23");
        if(minute < 0 || minute >=60)
            throw new IllegalArgumentException("Minute must be 0-59");
        if(second < 0 || second >=60)
            throw new IllegalArgumentException("Second must be 0-59");
        
        this.hour=hour;
        this.minute=minute;
        this.second=second;
    }
    //Valida e configura a hora
    public setHour(int hour){
        if(hour < 0 || hour >=24)
            throw new IllegalArgumentException("Hour must be 0-23");
        
        this.hour=hour;
    }
    //Valida e configura os minutos
    public setMinute(int minute){
        if(minute < 0 || minute >=60)
            throw new IllegalArgumentException("Minute must be 0-59");
        
        this.minute=minute;
    }
    
    //Valida e configura os segundos
    public setSecond(int second){
        if(second < 0 || second >=60)
            throw new IllegalArgumentException("Second must be 0-59");

       this.second=second;
    }
    //Metodos get
    //Obtem valor da hora
    public int getHour(){
        return hour;
    }
    //Obtem valor dos minutos
    public int getMinute(){
        return minute;
    }
    //Obtem valor dos segundos
    public int getSecond(){
        return second;
    }
    //Converte em String no formato de data/hora universal (HH:MM:SS)
    public String toUniversalString(){
        return String.format("%02d:%02d:%02d", getHour(),getMinute(),getSecond());
    }
    //Converte em String no formato padrao de data/hora (H:MM:SS AM ou PM)
    public String toString(){
        return String.format("%d:%02d:%02d %s",((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12),getMinute(),getSecond(),(getHour() < 12 ? "AM":"PM"));
    }

}//Fim da classe Time2