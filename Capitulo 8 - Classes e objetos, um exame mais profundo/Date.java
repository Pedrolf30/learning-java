//Date.java
//Declaracao da classe Date

public class Date{
    private int month;
    private int day;
    private int year;

    private static final int[] daysPerMonth =
        { 0, 31, 28, 31, 30, 31, 31, 31, 31,30, 31, 30, 31};
    
    //Construtor: confirma o valor adequado para o mes e dia dado o ano
    public Date(int month, int day, int year){
        //Verifica se o mes esta no intervalo
        if(month<=0 || month >12)
            throw new IllegalArgumentException("Month (" + month +") must be 1-12");
        //Verifica se o day esta no intervalo para month
        if(day <= 0 || (day>daysPerMonth[month] && !(month == 2 && day == 29)))
            throw new IllegalArgumentException("Day (" + day + ") out of range for the specified month and year");
        //Verifica no ano bissexto se o mes e 2 e o dia e 29
        if(month == 2 && day == 29 && !(year %400 == 0 || (year %4 == 0 && year % 100 != 0)))
            throw new IllegalArgumentException("Day (" + day + ") out of range for the specified month and year");
        
        this.month=month;
        this.day=day;
        this.year=year;

        System.out.printf("Date object constructor for date %s%n", this);
    }
    //Retorna um String no formato mes/dia/ano
    public String toString(){
        return String.format("%d/%d/%d", month, day, year);
    }
}//Fim da classe Date