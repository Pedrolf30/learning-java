//Employee2.java
//Variavel static utilizada para manter uma contagem de numero de objetos employee na memoria

public class Employee2{
    private static int count = 0;//Numero de Employess criados
    private String firstName;
    private String lastName;

    //Inicializa Employee, adiciona 1 a static count e
    //Gera a saida de String inidicando que o construtor foi chamado
    public Employee2(String firstName, String lastName){
        this.firstName=firstName;
        this.lastName=lastName;

        ++count; //Incrementa contagem estatica de empregados
        System.out.printf("Employee constructor: %s %s; count = %d%n", firstName,lastName,count);
    }
    //Obtem o primeiro nome
    public String getFS(){
        return firstName;
    }
    //Obtem o ultimo nome
    public String getLS(){
        return lastName;
    }
    //Metodo estatico para obter valor de contagem estatica
    public static int getC(){
        return count;
    }
}//Fim da classe Employee