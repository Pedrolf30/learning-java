//EnumTest.java
//Testando o tipo enum book

import java.util.EnumSet;

public class EnumTest{
    public static void main(String[]args){
        System.out.println("All books:");

        //imprime todos os livreos em um enum Book
        for(Book book : Book.values())
            System.out.printf("%-10s%-45s%s%n", book,book.getTitle(),book.getCopyRightYear());

        System.out.printf("%nDisplay a range of enum constants:%n");

        //Imprime os primeiros quatro livros
        for(Book book : EnumSet.range(Book.JHTP,Book.CPPHTP))
            System.out.printf("%-10s%-45s%s%n", book,book.getTitle(),book.getCopyRightYear());
    }
}//Fim da classe EnumTest