//Employee.java
//Clase Employee com referencia a outros objetos

public class Employee{
    private String firstName;
    private String lastName;
    private Date birthDate;
    private Date hireDate;

    //Construtor para inicializar noma, data de nascimento e data de contratacao
    public Employee(String firstName, String lastName, Date birthDay, Date hireDate){
        this.firstName=firstName;
        this.lastName=lastName;
        this.birthDate=birthDate;
        this.hireDate=hireDate;
    }
    //Converte Employee em formato de String
    public String toString(){
        return String.format("%s, %s Hire: %s Birthday: %s", lastName, firstName, hireDate, birthDate);
    }
}//Fim da classe Employee