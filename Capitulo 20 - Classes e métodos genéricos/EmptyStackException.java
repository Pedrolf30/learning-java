//EmptyStackException.java
//Declaracao da clase EmptyStackException
public class EmptyStackException extends RuntimeException{
    //Construtor sem argumento
    public EmptyStackException(){
        this("Stack is empty");
    }

    //Construtor de um argumento
    public EmptyStackException(String message){
        super(message);
    }
}//Fim da classe EmptyStackException