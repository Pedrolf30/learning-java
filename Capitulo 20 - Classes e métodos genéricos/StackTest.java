//StackTest.java
//Programa de teste da class generica Stack

public class StackTest{
    public static void main(String[]args){
        double[] doubleElements = {1.1, 2.2, 3.3, 4.4, 5.5};
        int[] integerElements = {1,2,3,4,5,6,7,8,9,10};

        //Criando um Stack<Double> e uma Stack<Integer>
        Stack<Double> doubleStack = new Stack<>(5);
        Stack<Integer> integerStack = new Stack<>();

        //Coloca os elementos de doubleElements em doubleStack
        testPushDouble(doubleStack, doubleElements);
        testPopDouble(doubleStack); //Remove de double Stack

        //Coloca os elementos de integerElements em integerStack
        testPushInteger(integerStack, integerElements);
        testPopInteger(integerStack); //Remove de integerStack
    }
    //Testa o metodo push com a pilha de double
    private static void testPushDouble(Stack<Double> stack, double[] values){
        System.out.printf("%nPushing elements onto doubleStack%n");

        //Insere elementos na Stack
        for(double value : values){
            System.out.printf("%.1f",value);
            stack.push(value); //Insere em doubleStack
        }
    }
    //Testa o metodo pop com a pilha de double
    private static void testPopDouble(Stack<Double> stack){
        //Remove elementos da pilha
        try{
            System.out.printf("%nPopping elements from doubleStack%n");
            double popValue; //Armazena o elementos removido da pilha

            //remove todos os elementos da stack
            while(true){
                popValue = stack.pop(); //Remove de doubleStack
                System.out.printf("%.1f", popValue);
            }
        }
        catch(EmptyStackException emptyStackException){
            System.err.println();
            emptyStackException.printStackTrace();
        }
    }
    //Testa o metodo push com a pilha de integer
    private static void testPushInteger(Stack<Integer> stack, int[] values){
        System.out.printf("%nPushing elements onto integerStack");
        //Insere elementos na Stack
        for(int value : values){
            System.out.printf("%d ",value);
            stack.push(value); //Insere em integerStack
        }
    }
    //Testa o metodo pop com a pilha de integer
    private static void testPopInteger(Stack<Integer> stack){
        //Remove elementos da pilha
        try{
            System.out.printf("%nPopping elements from integerStack%n");
            int popValue; //Armazena o elemento removido da pilha

            //Remove todos os elementos da stack
            while(true){
                popValue = stack.pop(); //Remove de intStack
                System.out.printf("%d ",popValue);
            }
        }
        catch(EmptyStackException emptyStackException){
            System.err.println();
            emptyStackException.printStackTrace();
        }
    }
}//Fim da classe StackTest
