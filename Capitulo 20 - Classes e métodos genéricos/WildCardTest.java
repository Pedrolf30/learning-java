//WildCardTest.java
//Programa de teste de coringa
import java.util.ArrayList;

public class WildCardTest{
    public static void main(String[]args){
        //Cria, inicializa e gera saida de ArrayList de Integers, entao
        //exibe o total dos elementos
        
        Integer[] integers = {1,2,3,4,5};
        ArrayList<Integer> integerList = new ArrayList<>();

        //Insre elementos na integerList
        for(Integer element : integers)
            integerList.add(element);

        System.out.printf("integerList contains: %s%n", integerList);
        System.out.printf("Total of the elements in integerList: %.0f%n%n",
            sum(integerList));
        
        //Cria, inicializa e gera saida do ArrayList de Double, entao
        //exibe o total dos elementos
        Double[] doubles = {1.1, 3.3, 5.5};
        ArrayList<Double> doubleList = new ArrayList<>();

        //Insere elementos na doubleList
        for(Double element : doubles)
            doubleList.add(element);

        System.out.printf("doubleList contains: %s%n", doubleList);
        System.out.printf("Total of the elements in doubleList: %.1f%n%n",
            sum(doubleList));

        //Cria, inicializa e gera saida de ArrayList de numeros contendo
        //Integers e Double, e entao exibe o total dos elementos
        Number[] numbers = {1, 2.4, 3, 4.1};
        ArrayList<Number> numberList = new ArrayList<>();

        for(Number element : numbers)
            numberList.add(element); //Insere cada numero na numberList

        System.out.printf("numberList contains: %s%n",numberList);
        System.out.printf("Total of the elements in numberList: %.1f%n",
            sum(numberList));
    }
    //Totaliza os elementos; usando um coringa no parametro ArrayList
    public static double sum(ArrayList<? extends Number> list){
        double total =0; //Inicia o total

        //Calcula a soma
        for(Number element : list)
            total+= element.doubleValue();

        return total;
    }
}//Fim da classe WildCardTest