//GenericMethodTest.java
//Imprimindo elementos do array com o método genérico printArray

public class GenericMethodTest{
    public static void main(String[] args){
        //Cria arrays de Integer, Double e Character
        Integer[] integerArray = {1,2,3,4,5};
        Double[] doubleArray = {1.1, 2.2, 3.3, 4.4, 5.5};
        Character[] characterArray = {'H','E','L','L','O'};

        System.out.printf("Array integerArray contains:%n");
        printArray(integerArray); //Passa um array de Integers
        System.out.printf("%nArray doubleArray contains:%n");
        printArray(doubleArray); //Passa um array de Doubles
        System.out.printf("%nArray characterArray contains:%n");
        printArray(characterArray); //Passa um array de Characters
    }

    //Métodos genéricos printArray
    public static <T> void printArray(T[] inputArray){
        //Exibe elementos do array
        for(T element : inputArray)
            System.out.printf("%s ", element);

        System.out.println();
    }
}//Fim da classe GenericMethodTest