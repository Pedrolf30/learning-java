//StackTest2.java
//Passando objetos Stack genericos para metodos genericos
public class StackTest2{
    public static void main(String[]args){
        Double[] doubleElements = {1.1, 2.2, 3.3, 4.4, 5.5};
        Integer[] integerElements = {1,2,3,4,5,6,7,8,9,10};

        //Criando um Stack<Double> e um Stack <Integer>
        Stack<Double> doubleStack = new Stack<>(5);
        Stack<Integer> integerStack = new Stack<>();

        //Coloca os elementos de doubleElements em doubleStack
        testPush("doubleStack", doubleStack, doubleElements);
        testPop("doubleStack", doubleStack); //Remove de doubleStack

        //Coloca os elements de integerElemends em integerStack
        testPush("integerStack", integerStack, integerElements);
        testPop("integerStack", integerStack); //Remove de integerStack
    }
    //Metodo generico testPush insere elementos em uma Stack
    public static <T> void testPush(String name, Stack<T> stack, T[] elements){
        System.out.printf("%nPushing elements onto %s%n", name);

        //Insere elementos na Stack
        for(T element : elements){
            System.out.printf("%s ",element);
            stack.push(element); //Insere o elemento na pilha
        }
    }

    //Metodo generico testPop remove elementos de uma Stack
    public static <T> void testPop(String name, Stack<T> stack){
        //Remove elementos da pilha
        try{
            System.out.printf("%nPopping elements from %s%n",name);
            T popValue; //Armazena o elemento removido da pilha

            //Remove todos os elementos da Stack
            while(true){
                popValue = stack.pop();
                System.out.printf("%s ",popValue);
            }
        }
        catch(EmptyStackException emptyStackException){
            System.err.println();
            emptyStackException.printStackTrace();
        }
    }
}//Fim da classe StackTest2