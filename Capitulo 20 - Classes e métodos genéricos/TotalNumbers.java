//TotalNumbers.java
//Somando os numeros em uma ArrayList<Number>
import java.util.ArrayList;

public class TotalNumbers{
    public static void main(String[]args){
        //Cria, inicializa e gera saida de ArrayList de numeros contendo
        //Integers e Double, e entao exibe o total dos elementos
        Number[] numbers = {1, 2.4, 3, 4.1};
        ArrayList<Number> numberList = new ArrayList<>();

        for(Number element : numbers)
            numberList.add(element); //Insere cada numero na numberList

        System.out.printf("numberList contains: %s%n",numberList);
        System.out.printf("Total of the elements in numberList: %.1f%n",
            sum(numberList));
    }
    //Calcula o total de elementos em ArrayList
    public static double sum(ArrayList<Number> list){
        double total = 0; //Inicializa o total

        //Calcula a soma
        for(Number element : list)
            total+=element.doubleValue();

        return total;
    }
}//Fim da classe TotalNumbers