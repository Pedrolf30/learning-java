//Stack.java
//Declaracao da classe generica Stack
import java.util.ArrayList;


public class Stack<T>{
    private final ArrayList<T> elements; //ArrayList armazena elementos da pilha

    //Construtor sem argumento cria uma pilha do tamanho padrao
    public Stack(){
        this(10);//Tamanho padrao da pilha
    }

    //Construtor cria uma pilha com o numero especificado de elementos
    public Stack(int capacity){
        int initCapacity = capacity > 0 ? capacity : 10; //Valida
        elements = new ArrayList<T>(initCapacity); //Cria a ArrayList
    }

    //Insere o elemento na pilha
    public void push(T pushValue){
        elements.add(pushValue); //Insere pushValue na Stack
    }

    //Retorna o elemento superior se nao estiver vazia; do contrario lanca uma EmptyStack
    public T pop(){
        if (elements.isEmpty()) //Se a pilha estiver vazia
            throw new EmptyStackException("Stack is empty, cannot pop");

            //Remove e torna o elemento superior da Stack
            return elements.remove(elements.size() - 1);
    }
}//Fim da classe Stack