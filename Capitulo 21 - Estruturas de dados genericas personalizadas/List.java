//List.java
//Declaracoes de class ListNode e List


import com.deitel.datastructures.EmptyListException;

//Classe pare representar um no em uma lista
class ListNode<T>{
    //Membros de acesso de pacote;List pode acessa-los diretamente
    T data; //Dados para esse no
    ListNode<T> nextNode; //Referencia para o proximo no na lista

    //Construtor cria um ListNode que referencia o objeto
    ListNode(T object){
        this(object, null);
    }

    //Construtor cria ListNode que referencia o objeto
    //especificado e o proximo ListNode
    ListNode(T object, ListNode<T> node){
        data = object;
        nextNode = node;
    }

    //Retorna a referencia aos dados no no
    T getData(){
        return data;
    }
    //Retorna referencia ao proximo no na list
    ListNode<T> getNext(){
        return nextNode;
    }
}//Fim da classe ListNode<T>

//Definicao da classe list
public class List<T>{
    private ListNode<T> firstNode;
    private ListNode<T> lastNode;
    private String name; //String como list usada na impressao

    //Construtor cria List vazia com list como o nome
    public List(){
        this("list");
    }
    //Construtor cria uma List vazia com um nome
    public List(String listName){
        name = listName;
        firstNode = lastNode = null;
    }
    //Insere o item na frente de List
    public void insertAtFront(T insertItem){
        if(isEmpty()) //firstNode e lastNode referenciam o mesmo objeto
            firstNode = lastNode = new ListNode<T>(insertItem);
        else //firstNode referencia o novo no
            firstNode = new ListNode<T>(insertItem, firstNode);
    }
    //Insere o item no fim de List
    public void insertAtBack(T insertItem){
        if(isEmpty()) //firstNode e lastNode referenciam o mesmo objeto
            firstNode = lastNode = new ListNode<T>(insertItem);
        else //nextNode do lastNode referencia o novo no 
        lastNode = lastNode.nextNode = new ListNode<T>(insertItem);
    }
    //Remove o primeiro no de List
    public T removeFromFront() throws EmptyListException{
        if(isEmpty()) //Lanca excecao se list estiver vazia
            throw new EmptyListException(name);
        
        T removedItem = firstNode.data; //Recupera dados sendo removidos

        //atualiza referencias firstNode e lastNode
        if(firstNode == lastNode)
            firstNode = lastNode = null;
        else
            firstNode = firstNode.nextNode;

        return removedItem; //Retorna dados de no removidos
    }

    //Remove o ultimo no de List
    public T removeFromBack() throws EmptyListException{
        if(isEmpty()) //Lanca excecao se List estiver vazia
            throw new EmptyListException();

        T removedItem = lastNode.data; //Recupera dados sendo removidos

        //Atualiza referencias firstNode e lastNode
        if(firstNode == lastNode)
            firstNode = lastNode = null;
        else{ //Localiza o novo ultimo no 
            ListNode<T> current = firstNode;

            //Faz loop enquanto o no atual nao referencia lastNode
            while(current.nextNode != lastNode)
                current = current.nextNode;

            lastNode = current; //Atual novo lastNode
            current.nextNode = null;
        }
        return removedItem; //Retorna dados de no removidos
    }

    //Determina se a lista estiver vazia
    public boolean isEmpty(){
        return firstNode == null; //Retorna true se a list estiver vazia
    }
    //Gera saida do conteudo da lista
    public void print(){
        if(isEmpty()){
            System.out.printf("Empty %s%n",name);
            return;
        }
        System.out.printf("The %s is: ", name);
        ListNode<T> current = firstNode;

        //enquanto nao estiver no fim da lista, gera saida dos dados do no atual
        while(current != null){
            System.out.printf("%s ",current.data);
            current = current.nextNode;
        }
        System.out.println();
    }
}//Fim da classe List<T>