//Queue.java
//Queue usa a classe List

public class Queue{
    private List<T> queueList;

    //Construtor 
    public Queue(){
        queueList = new List<T>("queue");
    }
    //Adiciona o objeto a fila
    public void enqueue(T object){
        queueList.insertAtBack(object);
    }
    //Remove o objeto da fila
    public T dequeue() throws EmptyListException{
    return queueList.removeFromFront();
    }
    //Determina se a fila está vazia
    public boolean isEmpty(){
        return queueList.isEmpty();
    }
    //Gera o conteuda da fila
    public void print(){
        queueList.print();
    }

}//Fim da classe Queue

