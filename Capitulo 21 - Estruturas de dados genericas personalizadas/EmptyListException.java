//EmptyListException.java
//Declaracao da classe EmptyListException


public class EmptyListException extends RuntimeException{
    //Contrutor
    public EmptyListException(){
        this("List"); //Chama outro construtor de EmptyListException
    }
    //Construtor
    public EmptyListException(String name){
        super(name + " is empty"); //Chama construtor de superclasse
    }
}//Fim da classe EmptyListException