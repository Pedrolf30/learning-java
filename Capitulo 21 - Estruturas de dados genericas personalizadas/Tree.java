//Tree.java
//Declaracoes de classe TreeNode e Tree por uma arvore de pesquisa binaria

//Definidacao da classe TreeNode
class TreeNode<T extends Comparable<T>>{
    //Membros de acessi de pacote
    TreeNode<T> leftNode;
    T data; //Valor do no
    TreeNode<T> rightNode;

    //Construtor inicializa os dados e os torna um no de folha
    public TreeNode(T nodeData){
        data = nodeData;
        leftNode = rightNode = null; //O no nao tem nenhum filho
    }
    //Localiza ponto de insercao e insere novo no;ignora os valores duplicados
    public void insert(T insertValue){
        //Insere na subarvore esquerda
        if(insertValue.compareTo(data) < 0){
            //Insere novo TreeNode
            if(leftNode == null)
                leftNode = new TreeNode<T>(insertValue);
            else //Continua percorrendo a subarvore esquerda recursivamente
                leftNode.insert(insertValue);
        }
        //Insere na subarvore direita
        else if(insertValue.compareTo(data) >0){
            //Insere novo TreeNode
            if(rightNode == null)
                rightNode = new TreeNode<T>(insertValue);
        }
    }
}//Fim da classe TreeNode

//Definicao da classe Tree
public class Tree<T extends Comparable<T>>{
    private TreeNode<T> root;

    //Construtor inicializa uma Tree de inteiros vazia
    public Tree(){
        root = null;
    }
    //Insere um novo no na arvore de pesquisa biaria
    public void insertNode(T insertValue){
        if(root == null)
            root = new TreeNode<T>(insertValue); //Cria o no raiz
        else
            root.insert(insertValue); //Chama o metodo insert
    }
    //Inicia o percurso na pre ordem
    public void preorderTransversal(){
        preorderHelper(root);
    }

    //Metodo recursivo para realizar percurso na pre ordem
    private void preorderHelper(TreeNode<T> node){
        if(node == null)
            return;

        System.out.printf("%s ",node.data); //Gera saida de dados do no
        preorderHelper(node.leftNode); //Percorre subarvore esquerda
        preorderHelper(node.rightNode); //Percorre subarvore direita
    }

    //Inicia percurso na ordem
    public void inorderTransversal(){
        inorderHelper(root);
    }
    //Metodo recursivo para realizar percurso na ordem
    private void inorderHelper(TreeNode<T> node){
        if (node == null)
            return;

        inorderHelper(node.leftNode); //Percorre subarvore esquerda
        System.out.printf("%s ",node.data);
        inorderHelper(node.rightNode); //Percorre subarvore direita
    }

    //Inicia percurso na pos ordem
    public void postorderTransversal(){
        postorderHelper(root);
    }
    //Metodo recursivo para realizar percurso na pos ordem
    private void postorderHelper(TreeNode<T> node){
        if(node == null)
            return;

        postorderHelper(node.leftNode);
        postorderHelper(node.rightNode);
        System.out.printf("%s ",node.data);
    }
}//Fim da classe Tree