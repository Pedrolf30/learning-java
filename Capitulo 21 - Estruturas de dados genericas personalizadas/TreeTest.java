//TreeTest.java
//Programa de teste da arvore binaria
import java.security.SecureRandom;
import com.deitel.datastructures.Tree;

public class TreeTest{
    public static void main(String[]args){
        Tree<Integer> tree = new Tree<Integer>();
        SecureRandom random = new SecureRandom();
        
        System.out.println("Inserting the following values: ");
        
        //Insere 10 inteiros aleatorios de 0 a 99 na arvore
        for(int i = 0; i <=10; i++){
            int value = random.nextInt(100);
            System.out.printf("%d ",value);
            tree.insertNode(value);
        }
        System.out.printf("%n%nPreorder transversal%n");
        tree.preorderTransversal();

        System.out.printf("%n%nInorder transversal%n");
        tree.inorderTransversal();

        System.out.printf("%n%nPostorder transversal%n");
        tree.postorderTransversal();
        System.out.println();
    }
}//Fim da classe TreeTest