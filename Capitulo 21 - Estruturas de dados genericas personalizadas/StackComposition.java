//StackComposition.java
//StackComposition usa um objeto List composto

public class StackComposition<T>{
    private List<T> stackList;

    //Construtor
    public StackComposition(){
        stackList = new List<T>("stack");
    }
    //Adiciona objeto a pilha
    public void push(T object){
        stackList.insertAtFront(object);
    }
    //Remove objeto da pilha
    public T pop() throws EmptyListException{
        return stackList.removeFromFront();
    }
    //Determina se a pilha está vazia
    public boolean isEmpty(){
        return stackList.isEmpty();
    }

    //Gera saida do conteudo de pilha
    public void print(){
        stackList.print();
    }
}//Fim da classe StackComposition