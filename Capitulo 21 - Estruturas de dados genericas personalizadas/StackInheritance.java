//StackInheritance.java
//StackInheritance estende a classe List

public class StackInheritance<T> extends List<T>{
    //Construtor
    public StackInheritance(){
        super("stack");
    }

    //Adicionar objeto a pilha
    public void push(T object){
        insertAtFront(object);
    }

    //Remove objeto da pilha
    public T pop() throws EmptyListException{
        return removeFromFront();
    }
}//Fim da classe StackInheritance